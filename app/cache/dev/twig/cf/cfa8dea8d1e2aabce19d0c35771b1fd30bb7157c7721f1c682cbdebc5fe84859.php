<?php

/* FrontendBundle:Frontend:contact.html.twig */
class __TwigTemplate_3b353eaf6303a1ce15f1deaf4d975c128b3764081f3c4af00544ceb66c45fcab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle:Frontend:base.html.twig", "FrontendBundle:Frontend:contact.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle:Frontend:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f104858b70b785e8fbcbb4516ac8b91527e308dbb2ef7bbec300865bb92d091d = $this->env->getExtension("native_profiler");
        $__internal_f104858b70b785e8fbcbb4516ac8b91527e308dbb2ef7bbec300865bb92d091d->enter($__internal_f104858b70b785e8fbcbb4516ac8b91527e308dbb2ef7bbec300865bb92d091d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontendBundle:Frontend:contact.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f104858b70b785e8fbcbb4516ac8b91527e308dbb2ef7bbec300865bb92d091d->leave($__internal_f104858b70b785e8fbcbb4516ac8b91527e308dbb2ef7bbec300865bb92d091d_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_52fbecac273f621335f8cced28ef2b6d15416944deeed6613dab3f0978227609 = $this->env->getExtension("native_profiler");
        $__internal_52fbecac273f621335f8cced28ef2b6d15416944deeed6613dab3f0978227609->enter($__internal_52fbecac273f621335f8cced28ef2b6d15416944deeed6613dab3f0978227609_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "NOMON CLOCKS | ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.title.contact"), "html", null, true);
        
        $__internal_52fbecac273f621335f8cced28ef2b6d15416944deeed6613dab3f0978227609->leave($__internal_52fbecac273f621335f8cced28ef2b6d15416944deeed6613dab3f0978227609_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_389a38212ad15f4aa02c3375c535d1bf109bcc2e98c3263f76c6c3616459a902 = $this->env->getExtension("native_profiler");
        $__internal_389a38212ad15f4aa02c3375c535d1bf109bcc2e98c3263f76c6c3616459a902->enter($__internal_389a38212ad15f4aa02c3375c535d1bf109bcc2e98c3263f76c6c3616459a902_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "<!-- HERO -->
<section style=\"background-image: url(";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/contacto_nomon_clocks.jpg"), "html", null, true);
        echo ");\" class=\"module module-parallax bg-dark-30\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/contacto_nomon_clocks.jpg"), "html", null, true);
        echo "\">

    <!-- HERO TEXT -->
    <div class=\"container\">

        <div class=\"row\">
            <div class=\"col-sm-12 text-center\">
                <h1 class=\"mh-line-size-3 font-alt m-b-20\">";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.contactpage.title"), "html", null, true);
        echo "</h1>
            </div>
        </div>

    </div>
    <!-- /HERO TEXT -->

</section>
<!-- /HERO -->

<!-- CONTACT -->
<section class=\"module\">

    <div class=\"container\">

        <div class=\"row\">
            <div class=\"col-sm-6 col-sm-offset-3\">
                <h2 class=\"module-title font-alt\">";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.contactpage.titletext"), "html", null, true);
        echo "</h2>
                <div class=\"module-subtitle font-serif\">";
        // line 30
        echo $this->env->getExtension('translator')->trans("nomon.contactpage.text");
        echo "</div>
            </div>
        </div>

        <div class=\"row\">

            <div class=\"col-sm-6 col-sm-offset-1\">
                
                <form id=\"contact-form\" role=\"form\" action=\"";
        // line 38
        echo $this->env->getExtension('routing')->getPath("frontend_frontend_send_email");
        echo "\" method=\"post\" novalidate=\"\">

                    <div class=\"form-group\">
                        <label class=\"sr-only\" for=\"cname\">Nombre</label>
                        <input id=\"cname\" class=\"form-control\" name=\"cname\" placeholder=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.contactpage.name"), "html", null, true);
        echo "*\" required=\"\" data-validation-required-message=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.contactpage.nameerror"), "html", null, true);
        echo "\" type=\"text\">
                        <p class=\"help-block text-danger\"></p>
                    </div>

                    <div class=\"form-group\">
                        <label class=\"sr-only\" for=\"cemail\">Email</label>
                        <input id=\"cemail\" name=\"cemail\" class=\"form-control\" placeholder=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.contactpage.email"), "html", null, true);
        echo "*\" required=\"\" data-validation-required-message=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.contactpage.emailerror"), "html", null, true);
        echo "\" type=\"email\">
                        <p class=\"help-block text-danger\"></p>
                    </div>

                    <div class=\"form-group\">
                        <textarea class=\"form-control\" id=\"cmessage\" name=\"cmessage\" rows=\"7\" placeholder=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.contactpage.message"), "html", null, true);
        echo "*\" required=\"\" data-validation-required-message=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.contactpage.messageerror"), "html", null, true);
        echo "\"></textarea>
                        <p class=\"help-block text-danger\"></p>
                    </div>

                    <div class=\"text-center\">
                        <button type=\"submit\" class=\"btn btn-block btn-round btn-d\">";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.contactpage.send"), "html", null, true);
        echo "</button>
                    </div>

                </form>

                <!-- Ajax response -->
                <div id=\"contact-response\" class=\"ajax-response font-alt text-center\"></div>

            </div>

            <div class=\"col-sm-4\">

                <!-- ALT CONTENT BOX -->
                <div class=\"alt-content-box m-t-0 m-t-sm-30\">
                    <h5 class=\"alt-content-box-title font-alt\">
                        NOMON CLOCKS
                    </h5>
                    info@nomon.es
                    <br>
                    Tel: +34 933 186 585
                    <br>
                    Passatge Maiol, 4
                    <br>
                    08013 – Barcelona – Spain
                </div>
                <!-- /ALT CONTENT BOX -->
            </div>

        </div>

    </div>
</section>
<!-- /CONTACT -->

<hr class=\"divider\"><!-- DIVIDER -->
";
        
        $__internal_389a38212ad15f4aa02c3375c535d1bf109bcc2e98c3263f76c6c3616459a902->leave($__internal_389a38212ad15f4aa02c3375c535d1bf109bcc2e98c3263f76c6c3616459a902_prof);

    }

    // line 94
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_fc397e4ac5b975c4ab97dcf3be64f3a186cb6ada875ecbcdfad22a6d808253eb = $this->env->getExtension("native_profiler");
        $__internal_fc397e4ac5b975c4ab97dcf3be64f3a186cb6ada875ecbcdfad22a6d808253eb->enter($__internal_fc397e4ac5b975c4ab97dcf3be64f3a186cb6ada875ecbcdfad22a6d808253eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 95
        echo "    <script>
        (function(\$){

            \$(document).ready(function() {

                /* ---------------------------------------------- /*
                 * Contact form ajax
                 /* ---------------------------------------------- */

                \$('#contact-form').find('input,textarea').jqBootstrapValidation({
                    preventSubmit: true,
                    submitError: function(\$form, event, errors) {
                        // additional error messages or events
                    },
                    submitSuccess: function(\$form, event) {
                        event.preventDefault();

                        var submit          = \$('#contact-form submit');
                        var ajaxResponse    = \$('#contact-response');

                        var name            = \$(\"input#cname\").val();
                        var email           = \$(\"input#cemail\").val();
                        var message         = \$(\"textarea#cmessage\").val();

                        \$.ajax({
                            type: 'POST',
                            url: '";
        // line 121
        echo $this->env->getExtension('routing')->getPath("frontend_frontend_send_email");
        echo "',
                            dataType: 'json',
                            data: {
                                name: name,
                                email: email,
                                message: message,
                            },
                            cache: false,
                            beforeSend: function(result) {
                                submit.empty();
                                submit.append('<i class=\"fa fa-cog fa-spin\"></i> Espere...');
                            },
                            success: function(result) {
                                if(result.sendstatus == 1) {
                                    ajaxResponse.html(result.message);
                                    \$form.fadeOut(500);
                                } else {
                                    ajaxResponse.html(result.message);
                                }
                            }
                        });
                    }
                });

            });

        })(jQuery);
    </script>
";
        
        $__internal_fc397e4ac5b975c4ab97dcf3be64f3a186cb6ada875ecbcdfad22a6d808253eb->leave($__internal_fc397e4ac5b975c4ab97dcf3be64f3a186cb6ada875ecbcdfad22a6d808253eb_prof);

    }

    public function getTemplateName()
    {
        return "FrontendBundle:Frontend:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  220 => 121,  192 => 95,  186 => 94,  143 => 58,  133 => 53,  123 => 48,  112 => 42,  105 => 38,  94 => 30,  90 => 29,  70 => 12,  58 => 5,  55 => 4,  49 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle:Frontend:base.html.twig' %}*/
/* {% block title %}NOMON CLOCKS | {{ 'nomon.title.contact' | trans }}{% endblock %}*/
/* {% block content %}*/
/* <!-- HERO -->*/
/* <section style="background-image: url({{ asset('bundles/frontend/images/contacto_nomon_clocks.jpg') }});" class="module module-parallax bg-dark-30" data-background="{{ asset('bundles/frontend/images/contacto_nomon_clocks.jpg') }}">*/
/* */
/*     <!-- HERO TEXT -->*/
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/*             <div class="col-sm-12 text-center">*/
/*                 <h1 class="mh-line-size-3 font-alt m-b-20">{{ 'nomon.contactpage.title' | trans }}</h1>*/
/*             </div>*/
/*         </div>*/
/* */
/*     </div>*/
/*     <!-- /HERO TEXT -->*/
/* */
/* </section>*/
/* <!-- /HERO -->*/
/* */
/* <!-- CONTACT -->*/
/* <section class="module">*/
/* */
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/*             <div class="col-sm-6 col-sm-offset-3">*/
/*                 <h2 class="module-title font-alt">{{ 'nomon.contactpage.titletext' | trans }}</h2>*/
/*                 <div class="module-subtitle font-serif">{{ 'nomon.contactpage.text' | trans | raw }}</div>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="row">*/
/* */
/*             <div class="col-sm-6 col-sm-offset-1">*/
/*                 */
/*                 <form id="contact-form" role="form" action="{{ path('frontend_frontend_send_email') }}" method="post" novalidate="">*/
/* */
/*                     <div class="form-group">*/
/*                         <label class="sr-only" for="cname">Nombre</label>*/
/*                         <input id="cname" class="form-control" name="cname" placeholder="{{ 'nomon.contactpage.name' | trans }}*" required="" data-validation-required-message="{{ 'nomon.contactpage.nameerror' | trans }}" type="text">*/
/*                         <p class="help-block text-danger"></p>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <label class="sr-only" for="cemail">Email</label>*/
/*                         <input id="cemail" name="cemail" class="form-control" placeholder="{{ 'nomon.contactpage.email' | trans }}*" required="" data-validation-required-message="{{ 'nomon.contactpage.emailerror' | trans }}" type="email">*/
/*                         <p class="help-block text-danger"></p>*/
/*                     </div>*/
/* */
/*                     <div class="form-group">*/
/*                         <textarea class="form-control" id="cmessage" name="cmessage" rows="7" placeholder="{{ 'nomon.contactpage.message' | trans }}*" required="" data-validation-required-message="{{ 'nomon.contactpage.messageerror' | trans }}"></textarea>*/
/*                         <p class="help-block text-danger"></p>*/
/*                     </div>*/
/* */
/*                     <div class="text-center">*/
/*                         <button type="submit" class="btn btn-block btn-round btn-d">{{ 'nomon.contactpage.send' | trans }}</button>*/
/*                     </div>*/
/* */
/*                 </form>*/
/* */
/*                 <!-- Ajax response -->*/
/*                 <div id="contact-response" class="ajax-response font-alt text-center"></div>*/
/* */
/*             </div>*/
/* */
/*             <div class="col-sm-4">*/
/* */
/*                 <!-- ALT CONTENT BOX -->*/
/*                 <div class="alt-content-box m-t-0 m-t-sm-30">*/
/*                     <h5 class="alt-content-box-title font-alt">*/
/*                         NOMON CLOCKS*/
/*                     </h5>*/
/*                     info@nomon.es*/
/*                     <br>*/
/*                     Tel: +34 933 186 585*/
/*                     <br>*/
/*                     Passatge Maiol, 4*/
/*                     <br>*/
/*                     08013 – Barcelona – Spain*/
/*                 </div>*/
/*                 <!-- /ALT CONTENT BOX -->*/
/*             </div>*/
/* */
/*         </div>*/
/* */
/*     </div>*/
/* </section>*/
/* <!-- /CONTACT -->*/
/* */
/* <hr class="divider"><!-- DIVIDER -->*/
/* {% endblock %}*/
/* {% block javascripts %}*/
/*     <script>*/
/*         (function($){*/
/* */
/*             $(document).ready(function() {*/
/* */
/*                 /* ---------------------------------------------- /**/
/*                  * Contact form ajax*/
/*                  /* ---------------------------------------------- *//* */
/* */
/*                 $('#contact-form').find('input,textarea').jqBootstrapValidation({*/
/*                     preventSubmit: true,*/
/*                     submitError: function($form, event, errors) {*/
/*                         // additional error messages or events*/
/*                     },*/
/*                     submitSuccess: function($form, event) {*/
/*                         event.preventDefault();*/
/* */
/*                         var submit          = $('#contact-form submit');*/
/*                         var ajaxResponse    = $('#contact-response');*/
/* */
/*                         var name            = $("input#cname").val();*/
/*                         var email           = $("input#cemail").val();*/
/*                         var message         = $("textarea#cmessage").val();*/
/* */
/*                         $.ajax({*/
/*                             type: 'POST',*/
/*                             url: '{{ path('frontend_frontend_send_email') }}',*/
/*                             dataType: 'json',*/
/*                             data: {*/
/*                                 name: name,*/
/*                                 email: email,*/
/*                                 message: message,*/
/*                             },*/
/*                             cache: false,*/
/*                             beforeSend: function(result) {*/
/*                                 submit.empty();*/
/*                                 submit.append('<i class="fa fa-cog fa-spin"></i> Espere...');*/
/*                             },*/
/*                             success: function(result) {*/
/*                                 if(result.sendstatus == 1) {*/
/*                                     ajaxResponse.html(result.message);*/
/*                                     $form.fadeOut(500);*/
/*                                 } else {*/
/*                                     ajaxResponse.html(result.message);*/
/*                                 }*/
/*                             }*/
/*                         });*/
/*                     }*/
/*                 });*/
/* */
/*             });*/
/* */
/*         })(jQuery);*/
/*     </script>*/
/* {% endblock %}*/
/* */
/* */
