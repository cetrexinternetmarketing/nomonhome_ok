<?php

/* FrontendBundle:Frontend:footer.html.twig */
class __TwigTemplate_841061b60c7ac30f87ce96c1c807e5d7ac6413825f78476d09be60755ec0b4f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- FOOTER -->
<section class=\"module-small bg-light\">

    <div class=\"container\">

        <!-- div class=\"row\">
            <div class=\"col-sm-3\">
                <!-- IMAGE OR SIMPLE TEXT >
                <img src=\"content/site-logo.png\" alt=\"\">
            </div>
        </div -->

        <div class=\"row m-t-40\">

            <div class=\"col-sm-3\">
                <h5 class=\"font-alt m-t-0 m-b-20\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.footer.wedo"), "html", null, true);
        echo "...</h5>
                <p>";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.footer.wedotext"), "html", null, true);
        echo ".</p>
            </div>

            <div class=\"col-sm-foot\">
                <div style=\"margin-top: -25px\" >
                    <img width=\"20px\" src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/iconoreloj.png"), "html", null, true);
        echo "\">
                </div>
                <h5 class=\"font-alt m-t-0 m-b-20\">";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.footer.clocks"), "html", null, true);
        echo "</h5>
                <ul class=\"list-unstyled\">
                    ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aCollection"]) ? $context["aCollection"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oCollection"]) {
            // line 27
            echo "                        <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("frontend_collection_list");
            echo "#";
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCollection"], "name", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCollection"], "name", array()), "html", null, true);
            echo "</a></li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oCollection'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "                </ul>
            </div>

            <div class=\"col-sm-foot\">
                <div style=\"margin-top: -25px\" >
                    <img width=\"20px\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/iconohome.png"), "html", null, true);
        echo "\">
                </div>
                <h5 class=\"font-alt m-t-0 m-b-20\">";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.footer.furniture"), "html", null, true);
        echo "</h5>
                <ul class=\"list-unstyled\">
                    ";
        // line 39
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aCollectionHome"]) ? $context["aCollectionHome"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oCollectionHome"]) {
            // line 40
            echo "                        <li>
                            <a href=\"";
            // line 41
            echo $this->env->getExtension('routing')->getPath("frontend_homecollection_list");
            echo "#";
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCollectionHome"], "name", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCollectionHome"], "name", array()), "html", null, true);
            echo "</a>
                        </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oCollectionHome'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "                </ul>
            </div>

            <div class=\"col-sm-foot\">
                <h5 class=\"font-alt m-t-0 m-b-20\">";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.footer.followus"), "html", null, true);
        echo "</h5>
                <ul class=\"list-unstyled\">
                \t<li><a href=\"http://bit.ly/1P9MWOv\" target=\"_blank\">CLOCKS CATALOGUE</a></li>
                \t<li><a href=\"http://bit.ly/1KPJCwL\" target=\"_blank\">HOME CATALOGUE</a></li>
                    <li><a href=\"http://presskit.nomon.es/\" target=\"_blank\">PRESS KIT</a></li>
                    ";
        // line 56
        echo "                </ul>
            </div>
            
              <div class=\"col-sm-foot\">
                <h5 class=\"font-alt m-t-0 m-b-20\">";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.footer.contactus"), "html", null, true);
        echo "</h5>
                info@nomon.es
                <br>
                Tel: +34 933 186 585
                <br>
                Passatge Maiol, 4
                <br>
                08013 – Barcelona – Spain
                <br>-
                <ul class=\"list-unstyled\">
                <li><a href=\"";
        // line 70
        echo $this->env->getExtension('routing')->getPath("frontend_page_view", array("sSlug" => "legal"));
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.footer.aviso"), "html", null, true);
        echo "</a></li>
                <li><a href=\"";
        // line 71
        echo $this->env->getExtension('routing')->getPath("frontend_page_view", array("sSlug" => "cookies"));
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.footer.cookies"), "html", null, true);
        echo "</a></li>
                </ul>
            </div>
\t\t\t

        </div>

    </div>

</section>

<hr class=\"divider\"><!-- DIVIDER -->

<!-- /FOOTER -->";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Frontend:footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 71,  146 => 70,  133 => 60,  127 => 56,  119 => 48,  113 => 44,  100 => 41,  97 => 40,  92 => 39,  87 => 36,  82 => 34,  75 => 29,  62 => 27,  58 => 26,  53 => 24,  48 => 22,  40 => 17,  36 => 16,  19 => 1,);
    }
}
/* <!-- FOOTER -->*/
/* <section class="module-small bg-light">*/
/* */
/*     <div class="container">*/
/* */
/*         <!-- div class="row">*/
/*             <div class="col-sm-3">*/
/*                 <!-- IMAGE OR SIMPLE TEXT >*/
/*                 <img src="content/site-logo.png" alt="">*/
/*             </div>*/
/*         </div -->*/
/* */
/*         <div class="row m-t-40">*/
/* */
/*             <div class="col-sm-3">*/
/*                 <h5 class="font-alt m-t-0 m-b-20">{{ 'nomon.footer.wedo' | trans }}...</h5>*/
/*                 <p>{{ 'nomon.footer.wedotext' | trans }}.</p>*/
/*             </div>*/
/* */
/*             <div class="col-sm-foot">*/
/*                 <div style="margin-top: -25px" >*/
/*                     <img width="20px" src="{{ asset('bundles/frontend/content/iconoreloj.png') }}">*/
/*                 </div>*/
/*                 <h5 class="font-alt m-t-0 m-b-20">{{ 'nomon.footer.clocks' | trans }}</h5>*/
/*                 <ul class="list-unstyled">*/
/*                     {% for oCollection in aCollection %}*/
/*                         <li><a href="{{ path( 'frontend_collection_list' ) }}#{{ oCollection.name }}">{{ oCollection.name }}</a></li>*/
/*                     {% endfor %}*/
/*                 </ul>*/
/*             </div>*/
/* */
/*             <div class="col-sm-foot">*/
/*                 <div style="margin-top: -25px" >*/
/*                     <img width="20px" src="{{ asset('bundles/frontend/content/iconohome.png') }}">*/
/*                 </div>*/
/*                 <h5 class="font-alt m-t-0 m-b-20">{{ 'nomon.footer.furniture' | trans }}</h5>*/
/*                 <ul class="list-unstyled">*/
/*                     {#<li><a href="{{ path('frontend_home_index') }}">NOMON HOME</a></li>#}*/
/*                     {% for oCollectionHome in aCollectionHome %}*/
/*                         <li>*/
/*                             <a href="{{ path("frontend_homecollection_list") }}#{{ oCollectionHome.name }}">{{ oCollectionHome.name }}</a>*/
/*                         </li>*/
/*                     {% endfor %}*/
/*                 </ul>*/
/*             </div>*/
/* */
/*             <div class="col-sm-foot">*/
/*                 <h5 class="font-alt m-t-0 m-b-20">{{ 'nomon.footer.followus' | trans }}</h5>*/
/*                 <ul class="list-unstyled">*/
/*                 	<li><a href="http://bit.ly/1P9MWOv" target="_blank">CLOCKS CATALOGUE</a></li>*/
/*                 	<li><a href="http://bit.ly/1KPJCwL" target="_blank">HOME CATALOGUE</a></li>*/
/*                     <li><a href="http://presskit.nomon.es/" target="_blank">PRESS KIT</a></li>*/
/*                     {#<li><a href="https://twitter.com/">Twitter</a></li>*/
/*                     <li><a href="https://www.facebook.com/">Facebook</a></li>*/
/*                     <li>-</li>#}*/
/*                 </ul>*/
/*             </div>*/
/*             */
/*               <div class="col-sm-foot">*/
/*                 <h5 class="font-alt m-t-0 m-b-20">{{ 'nomon.footer.contactus' | trans }}</h5>*/
/*                 info@nomon.es*/
/*                 <br>*/
/*                 Tel: +34 933 186 585*/
/*                 <br>*/
/*                 Passatge Maiol, 4*/
/*                 <br>*/
/*                 08013 – Barcelona – Spain*/
/*                 <br>-*/
/*                 <ul class="list-unstyled">*/
/*                 <li><a href="{{ path('frontend_page_view', { 'sSlug' : 'legal' }) }}">{{ 'nomon.footer.aviso' | trans }}</a></li>*/
/*                 <li><a href="{{ path('frontend_page_view', { 'sSlug' : 'cookies' }) }}">{{ 'nomon.footer.cookies' | trans }}</a></li>*/
/*                 </ul>*/
/*             </div>*/
/* 			*/
/* */
/*         </div>*/
/* */
/*     </div>*/
/* */
/* </section>*/
/* */
/* <hr class="divider"><!-- DIVIDER -->*/
/* */
/* <!-- /FOOTER -->*/
