<?php

/* FrontendBundle:IndexSection:collaborators_index.html.twig */
class __TwigTemplate_b878dfc2d5b7df0b53b1c3294738aa9dc42fd75226f31899de03dbba515f40d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- COLABORACIONES -->
<section id=\"portfolio\" class=\"module\">

    <div class=\"container\">

        <!-- MODULE TITLE -->
        <div class=\"row\">
            <div class=\"col-sm-6 col-sm-offset-3\">
                <h2 class=\"module-title font-alt\">";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.menu.collaborations"), "html", null, true);
        echo "</h2>
            </div>
        </div>
        <!-- /MODULE TITLE -->

        <!-- WORKS GRID -->
        <div class=\"works-grid-wrapper\">

            <div style=\"position: relative; height: 210px;\" id=\"works-grid2\" class=\"works-grid works-hover-w\">

                <!-- DO NOT DELETE THIS DIV -->
                <div class=\"grid-sizer\"></div>

                ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aCollaborators"]) ? $context["aCollaborators"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["oCollaborator"]) {
            // line 23
            echo "                    <!-- PORTFOLIO ITEM -->
                    <div style=\"height: 229px; position: absolute; left: ";
            // line 24
            echo twig_escape_filter($this->env, ($this->getAttribute($context["loop"], "index0", array()) * 315), "html", null, true);
            echo "px; top: 0px;\" class=\"work-item\">
                        <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_collaborator_view", array("sSlug" => $this->getAttribute($context["oCollaborator"], "slug", array()))), "html", null, true);
            echo "\">
                            <img src=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute($context["oCollaborator"], "logo", array()), "webpath", array())), "html", null, true);
            echo "\" alt=\"\">

                        </a>
                    </div>
                    <!-- /PORTFOLIO ITEM -->
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oCollaborator'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "            </div>
        </div>
    </div>
</section>
<!-- COLABORACIONES -->";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:IndexSection:collaborators_index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 32,  73 => 26,  69 => 25,  65 => 24,  62 => 23,  45 => 22,  29 => 9,  19 => 1,);
    }
}
/* <!-- COLABORACIONES -->*/
/* <section id="portfolio" class="module">*/
/* */
/*     <div class="container">*/
/* */
/*         <!-- MODULE TITLE -->*/
/*         <div class="row">*/
/*             <div class="col-sm-6 col-sm-offset-3">*/
/*                 <h2 class="module-title font-alt">{{ 'nomon.menu.collaborations' | trans }}</h2>*/
/*             </div>*/
/*         </div>*/
/*         <!-- /MODULE TITLE -->*/
/* */
/*         <!-- WORKS GRID -->*/
/*         <div class="works-grid-wrapper">*/
/* */
/*             <div style="position: relative; height: 210px;" id="works-grid2" class="works-grid works-hover-w">*/
/* */
/*                 <!-- DO NOT DELETE THIS DIV -->*/
/*                 <div class="grid-sizer"></div>*/
/* */
/*                 {% for oCollaborator in aCollaborators %}*/
/*                     <!-- PORTFOLIO ITEM -->*/
/*                     <div style="height: 229px; position: absolute; left: {{ loop.index0*315 }}px; top: 0px;" class="work-item">*/
/*                         <a href="{{ path('frontend_collaborator_view', { 'sSlug' : oCollaborator.slug }) }}">*/
/*                             <img src="{{ asset( oCollaborator.logo.webpath ) }}" alt="">*/
/* */
/*                         </a>*/
/*                     </div>*/
/*                     <!-- /PORTFOLIO ITEM -->*/
/*                 {% endfor %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </section>*/
/* <!-- COLABORACIONES -->*/
