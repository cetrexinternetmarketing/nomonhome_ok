<?php

/* FrontendBundle:Collaborator:view.html.twig */
class __TwigTemplate_5771c7ee957f38bdcdee80a447b75bd06177f9effa05edcad94cc3a1d5b54ffe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle:Frontend:base.html.twig", "FrontendBundle:Collaborator:view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle:Frontend:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oCollaborator"]) ? $context["oCollaborator"] : null), "name", array()), "html", null, true);
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<!-- HERO -->
<section style=\"background-image: url('";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oCollaborator"]) ? $context["oCollaborator"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "');\" class=\"module module-parallax bg-dark-30\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oCollaborator"]) ? $context["oCollaborator"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

    <!-- HERO TEXT -->
    <div class=\"container\">

        <div class=\"row\">
            <div class=\"col-sm-12 text-center\">
                <h1 class=\"mh-line-size-3 font-alt m-b-20\">";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oCollaborator"]) ? $context["oCollaborator"] : null), "name", array()), "html", null, true);
        echo "</h1>
            </div>
        </div>

    </div>
    <!-- /HERO TEXT -->

</section>
<!-- /HERO -->

<!-- PRODUCT -->
<section class=\"module\">

    <div class=\"container\">

        <div class=\"row\">

            <!-- PRODUCT IMAGES -->
            <div class=\"col-sm-6 m-b-sm-40 text-center\">

                ";
        // line 32
        if ($this->getAttribute((isset($context["oImagePrincipal"]) ? $context["oImagePrincipal"] : null), "webpath", array(), "any", true, true)) {
            // line 33
            echo "                    ";
            // line 34
            echo "                    <a class=\"fancybox\" id=\"fancy-image-principal\" rel=\"gallery1\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["oImagePrincipal"]) ? $context["oImagePrincipal"] : null), "webpath", array())), "html", null, true);
            echo "\">
                        <img src=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["oImagePrincipal"]) ? $context["oImagePrincipal"] : null), "webpath", array())), "html", null, true);
            echo "\" id=\"image-principal\" alt=\"\">
                    </a>
                    ";
            // line 38
            echo "                ";
        }
        // line 39
        echo "
                <ul class=\"product-gallery\">
                    ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aImages"]) ? $context["aImages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oImage"]) {
            // line 42
            echo "                    <li>
                        <div class=\"col-sm-12 image-gallery\" style=\"height: 68px; background-size: 110px; background-position: center;  background-repeat: no-repeat; background-image: url('";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["oImage"], "webpath", array())), "html", null, true);
            echo "');\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["oImage"], "webpath", array())), "html", null, true);
            echo "\" class=\"fancybox\" rel=\"gallery1\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["oImage"], "webpath", array())), "html", null, true);
            echo "\">
                        </div>
                        ";
            // line 47
            echo "                        ";
            // line 50
            echo "                    </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oImage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "                </ul>

            </div>
            <!-- PRODUCT IMAGES -->

            <!-- PRODUCT DESC -->
            <div class=\"col-sm-6 m-b-sm-40\">

                <!-- TITLE -->
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                        <h1 class=\"product-title font-alt\">";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oCollaborator"]) ? $context["oCollaborator"] : null), "name", array()), "html", null, true);
        echo "</h1>
                    </div>
                </div>

                <!-- AMOUNT >
                <div class=\"row m-b-20\">
                    <div class=\"col-sm-12\">
                        <div class=\"price font-alt\">
                            <span class=\"amount\">£20.00</span>
                        </div>
                    </div>
                </div -->

                <!-- DESCRIPTION -->
                <div class=\"row m-b-20\">
                    <div class=\"col-sm-12\">
                        <div class=\"description\">
                            <p>
                                ";
        // line 81
        echo $this->getAttribute((isset($context["oCollaborator"]) ? $context["oCollaborator"] : null), "description", array());
        echo "
                            </p>
                        </div>
                    </div>
                </div>

            </div>
            <!-- PRODUCT DESC -->

        </div>

    </div>

</section>
<!-- /PRODUCT -->

<hr class=\"divider\"><!-- DIVIDER -->
";
        // line 103
        echo "
";
    }

    // line 106
    public function block_javascripts($context, array $blocks = array())
    {
        // line 107
        echo "    <script>
        \$( document ).ready(function() {
            \$( \".image-gallery\" ).click(function() {
                \$(\"#image-principal\").attr('src', \$(this).attr('src') );
                \$(\"#fancy-image-principal\").attr('href', \$(this).attr('src') );
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Collaborator:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 107,  183 => 106,  178 => 103,  158 => 81,  137 => 63,  124 => 52,  117 => 50,  115 => 47,  106 => 43,  103 => 42,  99 => 41,  95 => 39,  92 => 38,  87 => 35,  82 => 34,  80 => 33,  78 => 32,  55 => 12,  43 => 5,  40 => 4,  37 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle:Frontend:base.html.twig' %}*/
/* {% block title %} {{ oCollaborator.name }}{% endblock %}*/
/* {% block content %}*/
/* <!-- HERO -->*/
/* <section style="background-image: url('{{ asset( oCollaborator.banner.webpath ) }}');" class="module module-parallax bg-dark-30" data-background="{{ asset( oCollaborator.banner.webpath ) }}">*/
/* */
/*     <!-- HERO TEXT -->*/
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/*             <div class="col-sm-12 text-center">*/
/*                 <h1 class="mh-line-size-3 font-alt m-b-20">{{ oCollaborator.name }}</h1>*/
/*             </div>*/
/*         </div>*/
/* */
/*     </div>*/
/*     <!-- /HERO TEXT -->*/
/* */
/* </section>*/
/* <!-- /HERO -->*/
/* */
/* <!-- PRODUCT -->*/
/* <section class="module">*/
/* */
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/* */
/*             <!-- PRODUCT IMAGES -->*/
/*             <div class="col-sm-6 m-b-sm-40 text-center">*/
/* */
/*                 {% if oImagePrincipal.webpath is defined  %}*/
/*                     {#<a href="{{ asset(oImagePrincipal.webpath) }}" class="gallery">#}*/
/*                     <a class="fancybox" id="fancy-image-principal" rel="gallery1" href="{{ asset(oImagePrincipal.webpath) }}">*/
/*                         <img src="{{ asset(oImagePrincipal.webpath) }}" id="image-principal" alt="">*/
/*                     </a>*/
/*                     {#</a>#}*/
/*                 {% endif %}*/
/* */
/*                 <ul class="product-gallery">*/
/*                     {% for oImage in aImages %}*/
/*                     <li>*/
/*                         <div class="col-sm-12 image-gallery" style="height: 68px; background-size: 110px; background-position: center;  background-repeat: no-repeat; background-image: url('{{ asset( oImage.webpath ) }}');" src="{{ asset( oImage.webpath ) }}" class="fancybox" rel="gallery1" href="{{ asset(oImage.webpath) }}">*/
/*                         </div>*/
/*                         {#<div class="col-sm-12 image-gallery" style="height: 68px; background-size: 80px; background-image: url('{{ asset( oImage.webpath ) }}');" src="{{ asset( oImage.webpath ) }}" class="fancybox" rel="gallery1" href="{{ asset(oImage.webpath) }}">*/
/*                         </div>#}*/
/*                         {#<a href="{{  asset(oImage.webpath )  }}" class="gallery">*/
/*                             <img class="image-gallery" src="{{ asset( oImage.webpath ) }}" alt="">*/
/*                         </a>#}*/
/*                     </li>*/
/*                     {% endfor %}*/
/*                 </ul>*/
/* */
/*             </div>*/
/*             <!-- PRODUCT IMAGES -->*/
/* */
/*             <!-- PRODUCT DESC -->*/
/*             <div class="col-sm-6 m-b-sm-40">*/
/* */
/*                 <!-- TITLE -->*/
/*                 <div class="row">*/
/*                     <div class="col-sm-12">*/
/*                         <h1 class="product-title font-alt">{{ oCollaborator.name }}</h1>*/
/*                     </div>*/
/*                 </div>*/
/* */
/*                 <!-- AMOUNT >*/
/*                 <div class="row m-b-20">*/
/*                     <div class="col-sm-12">*/
/*                         <div class="price font-alt">*/
/*                             <span class="amount">£20.00</span>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div -->*/
/* */
/*                 <!-- DESCRIPTION -->*/
/*                 <div class="row m-b-20">*/
/*                     <div class="col-sm-12">*/
/*                         <div class="description">*/
/*                             <p>*/
/*                                 {{ oCollaborator.description | raw }}*/
/*                             </p>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/* */
/*             </div>*/
/*             <!-- PRODUCT DESC -->*/
/* */
/*         </div>*/
/* */
/*     </div>*/
/* */
/* </section>*/
/* <!-- /PRODUCT -->*/
/* */
/* <hr class="divider"><!-- DIVIDER -->*/
/* {#*/
/* {{ render(controller(*/
/* 'FrontendBundle:Project:other'*/
/* )) }}*/
/* #}*/
/* */
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     <script>*/
/*         $( document ).ready(function() {*/
/*             $( ".image-gallery" ).click(function() {*/
/*                 $("#image-principal").attr('src', $(this).attr('src') );*/
/*                 $("#fancy-image-principal").attr('href', $(this).attr('src') );*/
/*             });*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
