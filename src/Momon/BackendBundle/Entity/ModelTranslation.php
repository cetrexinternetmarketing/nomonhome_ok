<?php


namespace Momon\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class ModelTranslation {

    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="box", type="string", length=255, nullable=true)
     */
    protected $box;

    /**
     * @var string
     *
     * @ORM\Column(name="points", type="string", length=255, nullable=true)
     */
    protected $points;

    /**
     * @var string
     *
     * @ORM\Column(name="hoop", type="string", length=255, nullable=true)
     */
    protected $hoop;

    /**
     * @var string
     *
     * @ORM\Column(name="time_signals", type="string", length=255, nullable=true)
     */
    protected $timeSignals;

    /**
     * @var string
     *
     * @ORM\Column(name="pendulum", type="string", length=255, nullable=true)
     */
    protected $pendulum;

    /**
     * @var string
     *
     * @ORM\Column(name="mechanism", type="string", length=255, nullable=true)
     */
    protected $mechanism;

    /**
     * @var string
     *
     * @ORM\Column(name="leg", type="string", length=255, nullable=true)
     */
    protected $leg;

    /**
     * @var string
     *
     * @ORM\Column(name="foot", type="string", length=255, nullable=true)
     */
    protected $foot;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Model
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set box
     *
     * @param string $box
     *
     * @return Model
     */
    public function setBox($box)
    {
        $this->box = $box;

        return $this;
    }

    /**
     * Get box
     *
     * @return string
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * Set points
     *
     * @param string $points
     *
     * @return Model
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return string
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set hoop
     *
     * @param string $hoop
     *
     * @return Model
     */
    public function setHoop($hoop)
    {
        $this->hoop = $hoop;

        return $this;
    }

    /**
     * Get hoop
     *
     * @return string
     */
    public function getHoop()
    {
        return $this->hoop;
    }

    /**
     * Set mechanism
     *
     * @param string $mechanism
     *
     * @return Model
     */
    public function setMechanism($mechanism)
    {
        $this->mechanism = $mechanism;

        return $this;
    }

    /**
     * Get mechanism
     *
     * @return string
     */
    public function getMechanism()
    {
        return $this->mechanism;
    }

    /**
     * Set leg
     *
     * @param string $leg
     *
     * @return Model
     */
    public function setLeg($leg)
    {
        $this->leg = $leg;

        return $this;
    }

    /**
     * Get leg
     *
     * @return string
     */
    public function getLeg()
    {
        return $this->leg;
    }

    /**
     * Set foot
     *
     * @param string $foot
     *
     * @return Model
     */
    public function setFoot($foot)
    {
        $this->foot = $foot;

        return $this;
    }

    /**
     * Get foot
     *
     * @return string
     */
    public function getFoot()
    {
        return $this->foot;
    }

    /**
     * @return string
     */
    public function getTimeSignals()
    {
        return $this->timeSignals;
    }

    /**
     * @param string $timeSignals
     */
    public function setTimeSignals($timeSignals)
    {
        $this->timeSignals = $timeSignals;
    }

    /**
     * @return string
     */
    public function getPendulum()
    {
        return $this->pendulum;
    }

    /**
     * @param string $pendulum
     */
    public function setPendulum($pendulum)
    {
        $this->pendulum = $pendulum;
    }
}
