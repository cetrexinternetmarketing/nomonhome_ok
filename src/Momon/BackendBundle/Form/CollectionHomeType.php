<?php

namespace Momon\BackendBundle\Form;

use Netik\MediaBundle\Form\SimpleFileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class CollectionHomeType extends AbstractType


{


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('translations', 'a2lix_translations')
            ->add('numOrder')
            ->add( 'image', new SimpleFileType() )
        ;
    }

    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Momon\BackendBundle\Entity\CollectionHome'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'momon_backendbundle_collectionhome';
    }


}
