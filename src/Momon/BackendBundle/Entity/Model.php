<?php

namespace Momon\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Netik\MediaBundle\Model\FolderInterface;
use Netik\MediaBundle\Model\GalleryInterface;
use Netik\MediaBundle\Model\GalleryModel;
use Netik\MediaBundle\Model\Nueva;
use Netik\MediaBundle\Model\UploadFileModel;
use Symfony\Component\HttpFoundation\File\File;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


/**
 * Campo
 *
 * @ORM\Table(name="model")
 * @ORM\Entity(repositoryClass="Momon\BackendBundle\Repository\ModelRepository")
 */
class Model implements  GalleryInterface
{
    use ORMBehaviors\Translatable\Translatable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;

    /**
     * @var string
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @ORM\OneToOne(targetEntity="Netik\MediaBundle\Entity\SimpleFile", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="banner_id", referencedColumnName="id",onDelete="SET NULL")
     **/
    private $banner;

	/**
	 * @ORM\ManyToOne(targetEntity="Collection", inversedBy="models")
	 * @ORM\JoinColumn(name="collection_id", referencedColumnName="id")
	 **/
	private $collection;

	/**
	 * @ORM\OneToOne(targetEntity="Netik\MediaBundle\Entity\SimpleFile", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(name="model3d_id", referencedColumnName="id",onDelete="SET NULL")
	 **/
	private $model3d;

	/**
	 * @ORM\OneToOne(targetEntity="Netik\MediaBundle\Entity\SimpleFile", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(name="datasheet_id", referencedColumnName="id",onDelete="SET NULL")
	 **/
	private $datasheet;

	/**
	 * @ORM\OneToOne(targetEntity="Netik\MediaBundle\Entity\Gallery", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id")
	 **/
	private $gallery;

    /**
     * @var string
     *
     * @ORM\Column(name="colors", type="text", nullable=true)
     */
    private $colors;

    /**
     * @var string
     *
     * @ORM\Column(name="numorder", type="integer", nullable=true)
     */
    private $numOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="isnew", type="boolean", nullable=false)
     */
    private $isnew;

	/**
	 * Set gallery
	 *
	 * @param \Netik\MediaBundle\Entity\Gallery $gallery
	 */
	public function setGallery( \Netik\MediaBundle\Entity\Gallery $oGallery = null ) {
		$this->gallery = $oGallery;
	}

	/**
	 * Get gallery
	 *
	 * @return \Netik\MediaBundle\Entity\Gallery
	 */
	public function getGallery() {
		return $this->gallery;
	}



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Model
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set banner
     *
     * @param \Netik\MediaBundle\Entity\SimpleFile $banner
     *
     * @return Model
     */
    public function setBanner(\Netik\MediaBundle\Entity\SimpleFile $banner = null)
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * Get banner
     *
     * @return \Netik\MediaBundle\Entity\SimpleFile
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * Set collection
     *
     * @param \Momon\BackendBundle\Entity\Collection $collection
     *
     * @return Model
     */
    public function setCollection(\Momon\BackendBundle\Entity\Collection $collection = null)
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * Get collection
     *
     * @return \Momon\BackendBundle\Entity\Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Set model2d
     *
     * @param \Netik\MediaBundle\Entity\SimpleFile $model2d
     *
     * @return Model
     */
    public function setModel2d(\Netik\MediaBundle\Entity\SimpleFile $model2d = null)
    {
        $this->model2d = $model2d;

        return $this;
    }

    /**
     * Get model2d
     *
     * @return \Netik\MediaBundle\Entity\SimpleFile
     */
    public function getModel2d()
    {
        return $this->model2d;
    }

    /**
     * Set model3d
     *
     * @param \Netik\MediaBundle\Entity\SimpleFile $model3d
     *
     * @return Model
     */
    public function setModel3d(\Netik\MediaBundle\Entity\SimpleFile $model3d = null)
    {
        $this->model3d = $model3d;

        return $this;
    }

    /**
     * Get model3d
     *
     * @return \Netik\MediaBundle\Entity\SimpleFile
     */
    public function getModel3d()
    {
        return $this->model3d;
    }

    /**
     * Set datasheet
     *
     * @param \Netik\MediaBundle\Entity\SimpleFile $datasheet
     *
     * @return Model
     */
    public function setDatasheet(\Netik\MediaBundle\Entity\SimpleFile $datasheet = null)
    {
        $this->datasheet = $datasheet;

        return $this;
    }

    /**
     * Get datasheet
     *
     * @return \Netik\MediaBundle\Entity\SimpleFile
     */
    public function getDatasheet()
    {
        return $this->datasheet;
    }

    /**
     * Set numOrder
     *
     * @param integer $numOrder
     *
     * @return Model
     */
    public function setNumOrder($numOrder)
    {
        $this->numOrder = $numOrder;

        return $this;
    }

    /**
     * Get numOrder
     *
     * @return integer
     */
    public function getNumOrder()
    {
        return $this->numOrder;
    }


    /**
     * Set colors
     *
     * @param string $colors
     *
     * @return Model
     */
    public function setColors($colors)
    {

        $this->colors = $colors;

        return $this;
    }

    /**
     * Get colors
     *
     * @return string
     */
    public function getColors()
    {
        return $this->colors;
    }

    public function getColorsHtml(){
        $sColors = explode(";", strip_tags($this->getColors()));

        $aColors = array();

        foreach($sColors as $sColor){
            $oColor = explode(":", $sColor);
            $aColors[] = $oColor;
        }

        return $aColors;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Model
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function __call($method, $arguments)
    {
        if($method != 'folder') {
            return $this->proxyCurrentLocaleTranslation('get' . $method, $arguments);
        }
    }

    /**
     * Set isnew
     *
     * @param boolean $isnew
     *
     * @return Model
     */
    public function setIsnew($isnew)
    {
        $this->isnew = $isnew;

        return $this;
    }

    /**
     * Get isnew
     *
     * @return boolean
     */
    public function getIsnew()
    {
        return $this->isnew;
    }

    public function __toString(){
        return $this->name;
    }
}
