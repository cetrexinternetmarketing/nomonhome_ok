<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\CollectionHome;
use Momon\BackendBundle\Form\CollectionHomeType;

/**
 * CollectionHome controller.
 *
 * @Route("/collectionhome")
 */
class CollectionHomeController extends Controller
{

    /**
     * INDEX ACTION - CollectionHome.
     *
     * @Route("/", name="backend_collectionhome_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aCollectionHomes = $em->getRepository( 'BackendBundle:CollectionHome' )->findAll();

        return array(
            'aCollectionHomes' => $aCollectionHomes,
        );
    }
    /**
     * CREATE ACTION - CollectionHome.
     *
     * @Route("/create", name="backend_collectionhome_create")
     * @Template()
     */
    public function createAction( Request $oRequest )
    {
        $oCollectionHome = new CollectionHome();
        $oForm = $this->createForm( new CollectionHomeType(), $oCollectionHome );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );

            if( $oForm->isValid() ){
                $em = $this->getDoctrine()->getManager();
                $em->persist( $oCollectionHome );
                $em->flush();

                $this->addFlash(
                    'success',
                    'Creado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_collectionhome_view', array( 'nCollectionHomeId' => $oCollectionHome->getId() ) ) );            }
        }

        return array(
            'oCollectionHome' => $oCollectionHome,
            'oForm'   => $oForm->createView(),
        );
    }
    /**
     * VIEW ACTION - CollectionHome.
     *
     * @Route("/view/{nCollectionHomeId}", name="backend_collectionhome_view")
     * @Template()
     */
    public function viewAction( $nCollectionHomeId )
    {
        $em = $this->getDoctrine()->getManager();

        $oCollectionHome = $em->getRepository( 'BackendBundle:CollectionHome' )->find( $nCollectionHomeId );

        if ( !$oCollectionHome ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la CollectionHome' );
        }

        $oForm = $this->createForm( new CollectionHomeType(), $oCollectionHome );

        return array(
            'oCollectionHome' => $oCollectionHome,
            'oForm' => $oForm->createView()
        );
    }

    /**
     * EDIT ACTION - CollectionHome.
     *
     * @Route("/edit/{nCollectionHomeId}", name="backend_collectionhome_edit")
     * @Template()
     */
    public function editAction( $nCollectionHomeId, Request $oRequest )
    {
        $em = $this->getDoctrine()->getManager();

        $oCollectionHome = $em->getRepository( 'BackendBundle:CollectionHome' )->find( $nCollectionHomeId );

        if ( !$oCollectionHome ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la CollectionHome' );
        }

        $oForm = $this->createForm( new CollectionHomeType(), $oCollectionHome );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );
            if( $oForm->isValid() ){
                $em->flush();

                $this->addFlash(
                'success',
                'Editado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_collectionhome_view', array( 'nCollectionHomeId' => $nCollectionHomeId ) ) );
            }
        }

        return array(
            'oCollectionHome' => $oCollectionHome,
            'oForm'         => $oForm->createView(),
        );
    }
    /**
     * DELETE ACTION - CollectionHome.
     *
     * @Route("/delete/{nCollectionHomeId}", name="backend_collectionhome_delete")
     */
    public function deleteAction( $nCollectionHomeId )
    {
        $em = $this->getDoctrine()->getManager();

        $oCollectionHome = $em->getRepository( 'BackendBundle:CollectionHome' )->find( $nCollectionHomeId );

        if ( !$oCollectionHome ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la CollectionHome' );
        }

        $em->remove( $oCollectionHome );
        $em->flush();

        $this->addFlash(
        'success',
        'Eliminado correctamente!'
        );

        return $this->redirect( $this->generateUrl( 'backend_collectionhome_index' ) );
    }}
