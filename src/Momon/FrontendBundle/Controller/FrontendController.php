<?php

namespace Momon\FrontendBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FrontendController extends Controller
{
    /**
     * @Route("/", name="frontend_frontend_index")
     * @Template()
     */
    public function indexAction()
    {
        $oManager = $this->getDoctrine()->getManager();

        $aCollections = $oManager->getRepository('BackendBundle:Collection')->findAll();

        $aCategoryProjects = $oManager->getRepository('BackendBundle:CategoryProject')->findByOrder();

        $aProjects = $oManager->getRepository('BackendBundle:Project')->findByOrderIndex();

        $aCollaborators = $oManager->getRepository('BackendBundle:Collaborator')->findAll();

        $aPages = $oManager->getRepository('BackendBundle:Page')->findByWorldOrder();

        $oHomeParallax = $oManager->getRepository('BackendBundle:Parallax')->findByName('nomon_home');

        $oNovedadesCollection = $oManager->getRepository('BackendBundle:Collection')->find(7);
        $oNovedadesCollectionModels = $oManager->getRepository('BackendBundle:Model')->findByIsNew();

        $aCollections = new ArrayCollection($aCollections);
        //$oNovedadesCollection = $oNovedadesCollection ? $oNovedadesCollection[0] : null;
        $aCollections->removeElement($oNovedadesCollection);
        
        $oBanner = $oManager->getRepository('BackendBundle:Banner')->findOneByName('inicio');

        return array(
            'oHomeParallax' => $oHomeParallax[0],
            'aCollections' => $aCollections,
            'aCategoryProjects' => $aCategoryProjects,
            'aProjects' => $aProjects,
            'aCollaborators' => $aCollaborators,
            'aPages' => $aPages,
            'oNovedadesCollection' => $oNovedadesCollection,
            'oNovedadesCollectionModels' => $oNovedadesCollectionModels,
            'oBanner' => $oBanner
        );
    }

    /**
     * @Template()
     */
    public function menuAction(){

        $oManager = $this->getDoctrine()->getManager();
        $aCollections = $oManager->getRepository('BackendBundle:Collection')->findByOrder();
        
        $oNovedadesCollection = $oManager->getRepository('BackendBundle:Collection')->find(7);
        $oNovedadesCollectionModels = $oManager->getRepository('BackendBundle:Model')->findByIsNew();

        $aCollections = new ArrayCollection($aCollections);        
        $aCollections->removeElement($oNovedadesCollection);
        
        return array(
            'aCollection' => $aCollections,
            'oMenuNovedadesCollection' => $oNovedadesCollection,
            'aMenuNovedadesModels' => $oNovedadesCollectionModels
        );

    }

    /**
     * @Route("/contact", name="frontend_frontend_contact")
     * @Template()
     */
    public function contactAction(){
        return array();
    }

    /**
     * @Route("/contact/send-email", name="frontend_frontend_send_email")
     **/
    public function sendEmailAction( Request $oRequest){

        $sName = $oRequest->get('name');
        $sEmail = $oRequest->get('email');
        $sText = $oRequest->get('message');

        $message = \Swift_Message::newInstance()
            ->setSubject('Formulario de Contacto')
            ->setFrom('info@nomon.es')
            ->setTo('info@nomon.es')
            ->setBody(
                $this->renderView(
                    'FrontendBundle:Emails:contact.html.twig',
                    array(
                        'sName' => $sName,
                        'sEmail' => $sEmail,
                        'sText' => $sText
                    )
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);
        $sMessage = $this->get('translator')->trans('nomon.contactpage.success');
        return new JsonResponse(array('message' => $sMessage));
    }

    /**
     * @Template()
     */
    public function footerAction(){
        $oManager = $this->getDoctrine()->getManager();

        $aCollection = $oManager->getRepository('BackendBundle:Collection')->findAll();
        $aCollectionHome = $oManager->getRepository('BackendBundle:CollectionHome')->findAll();

        return array(
            'aCollectionHome' => $aCollectionHome,
            'aCollection' => $aCollection
        );
    }
}
