<?php

namespace Momon\FrontendBundle\Controller;

use Netik\MediaBundle\Collection\ImageCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DistributorController extends Controller {

	/**
	 * @Route("/distributors", name="frontend_distributor_list")
	 * @Template()
	 */
	public function listAction() {
		$oManager = $this->getDoctrine()->getManager();

		$aDistributors = $oManager->getRepository( 'BackendBundle:Distributor' )->findByOrder( );
		$oSection = $oManager->getRepository( 'BackendBundle:Section' )->findOneByName( 'distributors' );
		return array(
			'oSection' => $oSection,
			'aDistributors' => $aDistributors
		);
	}
}