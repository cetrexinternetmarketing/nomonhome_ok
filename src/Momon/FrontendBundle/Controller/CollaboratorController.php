<?php

namespace Momon\FrontendBundle\Controller;

use Netik\MediaBundle\Collection\ImageCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CollaboratorController extends Controller {

	/**
	 * @Route("/collaborators", name="frontend_collaborator_list")
	 * @Template()
	 */
	public function listAction() {
		$oManager = $this->getDoctrine()->getManager();

		$aCollaborators = $oManager->getRepository( 'BackendBundle:Collaborator' )->findByOrder( );

		$oSection = $oManager->getRepository( 'BackendBundle:Section' )->findOneByName( 'collaborations' );

		return array(
			'oSection' => $oSection,
			'aCollaborators' => $aCollaborators
		);
	}

	/**
	 * @Route("/collaborator/{sSlug}", name="frontend_collaborator_view")
	 * @Template()
	 */
	public function viewAction( $sSlug ){
		$oManager = $this->getDoctrine()->getManager();
		$oCollaborator = $oManager->getRepository( 'BackendBundle:Collaborator' )->findOneBySlug( $sSlug );

		$oGallery = $oCollaborator->getGallery();

		$aImages = new ImageCollection( $oManager->getRepository( 'MediaBundle:Image' )->findByGalleryOrdered( $oGallery->getId() ) );

		return array(
			'oImagePrincipal' => $aImages->first(),
			'aImages' => $aImages,
			'oCollaborator' => $oCollaborator
		);
	}
}