<?php

namespace Momon\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Netik\MediaBundle\Form\SimpleFileType;


class ModelType extends AbstractType


{


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('slug')
            ->add('colors')
            ->add('numOrder')
            ->add('translations', 'a2lix_translations')
            ->add('isnew')
            ->add('banner',new SimpleFileType() )
            ->add('collection')
            ->add('model3d',new SimpleFileType() )
            ->add('datasheet',new SimpleFileType() )
        ;
    }

    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Momon\BackendBundle\Entity\Model'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'momon_backendbundle_model';
    }


}
