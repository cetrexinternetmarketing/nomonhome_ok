<?php

/* FrontendBundle:IndexSection:culture_index.html.twig */
class __TwigTemplate_6ce4096c37967685030125abf42511d26b7bbe9b89ba620badd806bff02c65cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_499ea348708edb7ad25f634e7816506abbce21bf18bcab2437efc118d508f767 = $this->env->getExtension("native_profiler");
        $__internal_499ea348708edb7ad25f634e7816506abbce21bf18bcab2437efc118d508f767->enter($__internal_499ea348708edb7ad25f634e7816506abbce21bf18bcab2437efc118d508f767_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontendBundle:IndexSection:culture_index.html.twig"));

        // line 1
        echo "<!-- NOMON CULTURE -->
<section id=\"nomonworld\" class=\"module\">

    <div class=\"container\">

        <!-- MODULE TITLE -->
        <div class=\"row\">
            <div class=\"col-sm-6 col-sm-offset-3\">
                <h2 class=\"module-title font-alt\">NOMON CULTURE</h2>
            </div>
        </div>
        <!-- /MODULE TITLE -->

        <div class=\"row multi-columns-row\">

            ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aPages"]) ? $context["aPages"] : $this->getContext($context, "aPages")));
        foreach ($context['_seq'] as $context["_key"] => $context["oPage"]) {
            // line 17
            echo "                <!-- POST -->
                <div class=\"col-sm-6 col-md-4 col-lg-4 m-b-60\">
                    <div class=\"post\">
                        <div class=\"post-media\">
                            ";
            // line 21
            if (($this->getAttribute($context["oPage"], "slug", array()) == "distributors")) {
                // line 22
                echo "                                <a href=\"";
                echo $this->env->getExtension('routing')->getPath("frontend_distributor_list");
                echo "\">
                            ";
            } else {
                // line 24
                echo "                                <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_page_view", array("sSlug" => $this->getAttribute($context["oPage"], "slug", array()))), "html", null, true);
                echo "\">
                            ";
            }
            // line 26
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute($context["oPage"], "banner", array()), "webpath", array())), "html", null, true);
            echo "\" alt=\"\">
                            </a>
                        </div>
                        <div class=\"post-header\">
                            <h5 class=\"post-title font-alt\">
                                ";
            // line 31
            if (($this->getAttribute($context["oPage"], "slug", array()) == "distributors")) {
                // line 32
                echo "                                <a href=\"";
                echo $this->env->getExtension('routing')->getPath("frontend_distributor_list");
                echo "\">
                                ";
            } else {
                // line 34
                echo "                                <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_page_view", array("sSlug" => $this->getAttribute($context["oPage"], "slug", array()))), "html", null, true);
                echo "\">
                                ";
            }
            // line 36
            echo "                                    ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["oPage"], "title", array()), "html", null, true);
            echo "
                                </a>
                            </h5>
                        </div>
                    </div>
                </div>
                <!-- /POST -->
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oPage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "        </div>
    </div>
</section>
<!-- /NOMON CULTURE -->";
        
        $__internal_499ea348708edb7ad25f634e7816506abbce21bf18bcab2437efc118d508f767->leave($__internal_499ea348708edb7ad25f634e7816506abbce21bf18bcab2437efc118d508f767_prof);

    }

    public function getTemplateName()
    {
        return "FrontendBundle:IndexSection:culture_index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 44,  86 => 36,  80 => 34,  74 => 32,  72 => 31,  63 => 26,  57 => 24,  51 => 22,  49 => 21,  43 => 17,  39 => 16,  22 => 1,);
    }
}
/* <!-- NOMON CULTURE -->*/
/* <section id="nomonworld" class="module">*/
/* */
/*     <div class="container">*/
/* */
/*         <!-- MODULE TITLE -->*/
/*         <div class="row">*/
/*             <div class="col-sm-6 col-sm-offset-3">*/
/*                 <h2 class="module-title font-alt">NOMON CULTURE</h2>*/
/*             </div>*/
/*         </div>*/
/*         <!-- /MODULE TITLE -->*/
/* */
/*         <div class="row multi-columns-row">*/
/* */
/*             {% for oPage in aPages %}*/
/*                 <!-- POST -->*/
/*                 <div class="col-sm-6 col-md-4 col-lg-4 m-b-60">*/
/*                     <div class="post">*/
/*                         <div class="post-media">*/
/*                             {% if oPage.slug == 'distributors' %}*/
/*                                 <a href="{{ path('frontend_distributor_list') }}">*/
/*                             {% else %}*/
/*                                 <a href="{{ path('frontend_page_view', { 'sSlug' : oPage.slug }) }}">*/
/*                             {% endif %}*/
/*                                 <img src="{{ asset( oPage.banner.webpath) }}" alt="">*/
/*                             </a>*/
/*                         </div>*/
/*                         <div class="post-header">*/
/*                             <h5 class="post-title font-alt">*/
/*                                 {% if oPage.slug == 'distributors' %}*/
/*                                 <a href="{{ path('frontend_distributor_list') }}">*/
/*                                 {% else %}*/
/*                                 <a href="{{ path('frontend_page_view', { 'sSlug' : oPage.slug }) }}">*/
/*                                 {% endif %}*/
/*                                     {{ oPage.title }}*/
/*                                 </a>*/
/*                             </h5>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <!-- /POST -->*/
/*             {% endfor %}*/
/*         </div>*/
/*     </div>*/
/* </section>*/
/* <!-- /NOMON CULTURE -->*/
