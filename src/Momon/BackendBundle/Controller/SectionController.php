<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\Section;
use Momon\BackendBundle\Form\SectionType;

/**
 * Section controller.
 *
 * @Route("/section")
 */
class SectionController extends Controller
{

    /**
     * INDEX ACTION - Section.
     *
     * @Route("/", name="backend_section_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aSections = $em->getRepository( 'BackendBundle:Section' )->findAll();

        return array(
            'aSections' => $aSections,
        );
    }
    /**
     * CREATE ACTION - Section.
     *
     * @Route("/create", name="backend_section_create")
     * @Template()
     */
    public function createAction( Request $oRequest )
    {
        $oSection = new Section();
        $oForm = $this->createForm( new SectionType(), $oSection );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );

            if( $oForm->isValid() ){
                $em = $this->getDoctrine()->getManager();
                $em->persist( $oSection );
                $em->flush();

                $this->addFlash(
                    'success',
                    'Creado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_section_view', array( 'nSectionId' => $oSection->getId() ) ) );            }
        }

        return array(
            'oSection' => $oSection,
            'oForm'   => $oForm->createView(),
        );
    }
    /**
     * VIEW ACTION - Section.
     *
     * @Route("/view/{nSectionId}", name="backend_section_view")
     * @Template()
     */
    public function viewAction( $nSectionId )
    {
        $em = $this->getDoctrine()->getManager();

        $oSection = $em->getRepository( 'BackendBundle:Section' )->find( $nSectionId );

        if ( !$oSection ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Section' );
        }

        $oForm = $this->createForm( new SectionType(), $oSection );

        return array(
            'oSection' => $oSection,
            'oForm' => $oForm->createView()
        );
    }

    /**
     * EDIT ACTION - Section.
     *
     * @Route("/edit/{nSectionId}", name="backend_section_edit")
     * @Template()
     */
    public function editAction( $nSectionId, Request $oRequest )
    {
        $em = $this->getDoctrine()->getManager();

        $oSection = $em->getRepository( 'BackendBundle:Section' )->find( $nSectionId );

        if ( !$oSection ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Section' );
        }

        $oForm = $this->createForm( new SectionType(), $oSection );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );
            if( $oForm->isValid() ){
                $em->flush();

                $this->addFlash(
                'success',
                'Editado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_section_view', array( 'nSectionId' => $nSectionId ) ) );
            }
        }

        return array(
            'oSection' => $oSection,
            'oForm'         => $oForm->createView(),
        );
    }
    /**
     * DELETE ACTION - Section.
     *
     * @Route("/delete/{nSectionId}", name="backend_section_delete")
     */
    public function deleteAction( $nSectionId )
    {
        $em = $this->getDoctrine()->getManager();

        $oSection = $em->getRepository( 'BackendBundle:Section' )->find( $nSectionId );

        if ( !$oSection ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Section' );
        }

        $em->remove( $oSection );
        $em->flush();

        $this->addFlash(
        'success',
        'Eliminado correctamente!'
        );

        return $this->redirect( $this->generateUrl( 'backend_section_index' ) );
    }}
