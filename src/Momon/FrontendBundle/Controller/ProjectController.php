<?php

namespace Momon\FrontendBundle\Controller;

use Netik\MediaBundle\Collection\ImageCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ProjectController extends Controller {
	/**
	 * @Route("/project/{nProjectId}", name="frontend_project_view")
	 * @Template()
	 */
	public function viewAction( $nProjectId ) {
		$oManager = $this->getDoctrine()->getManager();
		$oProject = $oManager->getRepository( 'BackendBundle:Project' )->find( $nProjectId );

		$oGallery = $oProject->getGallery();

		$aImages = new ImageCollection( $oManager->getRepository( 'MediaBundle:Image' )->findByGalleryOrdered( $oGallery->getId() ) );

		return array(
			'oImagePrincipal' => $aImages->first(),
			'aImages' => $aImages,
			'oProject' => $oProject
		);
	}

	/**
	 * @Route("/list-projects", name="frontend_project_list")
	 * @Template()
	 */
	public function listAction() {
		$oManager = $this->getDoctrine()->getManager();

		$aProjects = $oManager->getRepository( 'BackendBundle:Project' )->findByOrder( );

		$aCategoryProject = $oManager->getRepository( 'BackendBundle:CategoryProject' )->findByOrder( );

		$oSection = $oManager->getRepository( 'BackendBundle:Section' )->findOneByName( 'projects' );

		return array(
			'aCategoryProject' => $aCategoryProject,
			'oSection' => $oSection,
			'aProjects' => $aProjects
		);
	}

	/**
	 * @Template()
	 */
	public function otherAction(){

		$oManager = $this->getDoctrine()->getManager();


		$aProjects = $oManager->getRepository('BackendBundle:Project')->findAll();



		$max = count( $aProjects ) >= 3 ? 4 : count( $aProjects );
		$aKeys = array_rand($aProjects, $max);



		if (is_array( $aKeys ) ) {
			array_intersect_key( $aProjects, array_flip( $aKeys ) );
		}
		else{
			$aProjects = array( $aProjects[$aKeys] );
		}


		return array(
			'aProjects' => $aProjects
		);
	}
}