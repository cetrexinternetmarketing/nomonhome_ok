<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\Project;
use Momon\BackendBundle\Form\ProjectType;

/**
 * Project controller.
 *
 * @Route("/project")
 */
class ProjectController extends Controller
{

    /**
     * INDEX ACTION - Project.
     *
     * @Route("/", name="backend_project_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aProjects = $em->getRepository( 'BackendBundle:Project' )->findAll();

        return array(
            'aProjects' => $aProjects,
        );
    }
    /**
     * CREATE ACTION - Project.
     *
     * @Route("/create", name="backend_project_create")
     * @Template()
     */
    public function createAction( Request $oRequest )
    {
        $oProject = new Project();
        $oForm = $this->createForm( new ProjectType(), $oProject );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );

            if( $oForm->isValid() ){
                $em = $this->getDoctrine()->getManager();
                $em->persist( $oProject );
                $em->flush();

                $this->addFlash(
                    'success',
                    'Creado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_project_view', array( 'nProjectId' => $oProject->getId() ) ) );            }
        }

        return array(
            'oProject' => $oProject,
            'oForm'   => $oForm->createView(),
        );
    }
    /**
     * VIEW ACTION - Project.
     *
     * @Route("/view/{nProjectId}", name="backend_project_view")
     * @Template()
     */
    public function viewAction( $nProjectId )
    {
        $em = $this->getDoctrine()->getManager();

        $oProject = $em->getRepository( 'BackendBundle:Project' )->find( $nProjectId );

        if ( !$oProject ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Project' );
        }

        $oForm = $this->createForm( new ProjectType(), $oProject );

        return array(
            'oProject' => $oProject,
            'oForm' => $oForm->createView()
        );
    }

    /**
     * EDIT ACTION - Project.
     *
     * @Route("/edit/{nProjectId}", name="backend_project_edit")
     * @Template()
     */
    public function editAction( $nProjectId, Request $oRequest )
    {
        $em = $this->getDoctrine()->getManager();

        $oProject = $em->getRepository( 'BackendBundle:Project' )->find( $nProjectId );

        if ( !$oProject ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Project' );
        }

        $oForm = $this->createForm( new ProjectType(), $oProject );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );
            if( $oForm->isValid() ){
                $em->flush();

                $this->addFlash(
                'success',
                'Editado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_project_view', array( 'nProjectId' => $nProjectId ) ) );
            }
        }

        return array(
            'oProject' => $oProject,
            'oForm'         => $oForm->createView(),
        );
    }
    /**
     * DELETE ACTION - Project.
     *
     * @Route("/delete/{nProjectId}", name="backend_project_delete")
     */
    public function deleteAction( $nProjectId )
    {
        $em = $this->getDoctrine()->getManager();

        $oProject = $em->getRepository( 'BackendBundle:Project' )->find( $nProjectId );

        if ( !$oProject ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Project' );
        }

        $em->remove( $oProject );
        $em->flush();

        $this->addFlash(
        'success',
        'Eliminado correctamente!'
        );

        return $this->redirect( $this->generateUrl( 'backend_project_index' ) );
    }}
