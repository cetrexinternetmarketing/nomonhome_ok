<?php

namespace Momon\FrontendBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CollectionHomeController extends Controller
{
	/**
	 * @Route("/homecollections", name="frontend_homecollection_list")
	 * @Template()
	 */
	public function listAction()
	{
		$oManager = $this->getDoctrine()->getManager();
		$aCollections = $oManager->getRepository('BackendBundle:CollectionHome')->findByOrder();
		$oSection = $oManager->getRepository( 'BackendBundle:Section' )->findOneByName( 'homecollections' );


		$oNovedadesCollection = $oManager->getRepository('BackendBundle:CollectionHome')->find(2);
		$oNovedadesCollectionModels = $oManager->getRepository('BackendBundle:HomeModel')->findByIsNew();

		$aCollections = new ArrayCollection($aCollections);
		$aCollections->removeElement($oNovedadesCollection);

		return array(
			'oSection' => $oSection,
			'aCollection' => $aCollections,
			'oNovedadesCollectionModels' => $oNovedadesCollectionModels,
			'oNovedadesCollection' => $oNovedadesCollection
		);
	}
}
