<?php

namespace Momon\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Netik\MediaBundle\Model\FolderInterface;
use Netik\MediaBundle\Model\GalleryInterface;
use Netik\MediaBundle\Model\GalleryModel;
use Netik\MediaBundle\Model\Nueva;
use Netik\MediaBundle\Model\UploadFileModel;
use Symfony\Component\HttpFoundation\File\File;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="Momon\BackendBundle\Repository\PageRepository")
 */
class Page {

    use ORMBehaviors\Translatable\Translatable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;

    /**
     * @var string
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    private $slug;


	/**
	 * @ORM\OneToOne(targetEntity="Netik\MediaBundle\Entity\SimpleFile", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(name="banner_id", referencedColumnName="id", onDelete="SET NULL")
	 **/
	private $banner;

    /**
     * @var string
     *
     * @ORM\Column(name="numworld", type="integer", nullable=true)
     */
    private $numWorld;


    /**
     * @var string
     *
     * @ORM\Column(name="numorder", type="integer", nullable=true)
     */
    private $numOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="is_video", type="boolean", nullable=false)
     */
    private $isVideo;


    /**
     * @ORM\OneToOne(targetEntity="Netik\MediaBundle\Entity\SimpleFile", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="image_parallax_id", referencedColumnName="id", onDelete="SET NULL")
     **/
    private $imageParallax;

    /**
     * @var string
     * @ORM\Column(name="video_parallax", type="string", length=255, nullable=true)
     */
    private $videoParallax;

    /**
     * @var string
     *
     * @ORM\Column(name="col1size", type="integer", nullable=false)
     */
    private $col1size = 4;

    /**
     * @var string
     *
     * @ORM\Column(name="col2size", type="integer", nullable=false)
     */
    private $col2size = 4;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Page
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set numWorld
     *
     * @param integer $numWorld
     *
     * @return Page
     */
    public function setNumWorld($numWorld)
    {
        $this->numWorld = $numWorld;

        return $this;
    }

    /**
     * Get numWorld
     *
     * @return integer
     */
    public function getNumWorld()
    {
        return $this->numWorld;
    }

    /**
     * Set banner
     *
     * @param \Netik\MediaBundle\Entity\SimpleFile $banner
     *
     * @return Page
     */
    public function setBanner(\Netik\MediaBundle\Entity\SimpleFile $banner = null)
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * Get banner
     *
     * @return \Netik\MediaBundle\Entity\SimpleFile
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set numOrder
     *
     * @param integer $numOrder
     *
     * @return Page
     */
    public function setNumOrder($numOrder)
    {
        $this->numOrder = $numOrder;

        return $this;
    }

    /**
     * Get numOrder
     *
     * @return integer
     */
    public function getNumOrder()
    {
        return $this->numOrder;
    }

    public function __call($method, $arguments)
    {
        if($method != 'folder' and $method != 'gallery') {
            return $this->proxyCurrentLocaleTranslation('get' . $method, $arguments);
        }
    }

    /**
     * Set imageParallax
     *
     * @param \Netik\MediaBundle\Entity\SimpleFile $imageParallax
     *
     * @return Page
     */
    public function setImageParallax(\Netik\MediaBundle\Entity\SimpleFile $imageParallax = null)
    {
        $this->imageParallax = $imageParallax;

        return $this;
    }

    /**
     * Get imageParallax
     *
     * @return \Netik\MediaBundle\Entity\SimpleFile
     */
    public function getImageParallax()
    {
        return $this->imageParallax;
    }

    /**
     * Set isvideo
     *
     * @param boolean $isvideo
     *
     * @return Page
     */
    public function setIsVideo($isVideo)
    {
        $this->isVideo = $isVideo;

        return $this;
    }

    /**
     * Get isvideo
     *
     * @return boolean
     */
    public function getIsVideo()
    {
        return $this->isVideo;
    }

    /**
     * Set videoParallax
     *
     * @param string $videoParallax
     *
     * @return Page
     */
    public function setVideoParallax($videoParallax)
    {
        $this->videoParallax = $videoParallax;

        return $this;
    }

    /**
     * Get videoParallax
     *
     * @return string
     */
    public function getVideoParallax()
    {
        return $this->videoParallax;
    }

    /**
     * @return string
     */
    public function getCol1size()
    {
        return $this->col1size;
    }

    /**
     * @param string $col1size
     */
    public function setCol1size($col1size)
    {
        $this->col1size = $col1size;
    }

    /**
     * @return string
     */
    public function getCol2size()
    {
        return $this->col2size;
    }

    /**
     * @param string $col2size
     */
    public function setCol2size($col2size)
    {
        $this->col2size = $col2size;
    }
}
