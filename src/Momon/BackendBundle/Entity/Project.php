<?php

namespace Momon\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Netik\MediaBundle\Model\FolderInterface;
use Netik\MediaBundle\Model\GalleryInterface;
use Netik\MediaBundle\Model\GalleryModel;
use Netik\MediaBundle\Model\Nueva;
use Netik\MediaBundle\Model\UploadFileModel;
use Symfony\Component\HttpFoundation\File\File;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="Momon\BackendBundle\Repository\ProjectRepository")
 */
class Project implements  GalleryInterface {

    use ORMBehaviors\Translatable\Translatable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @ORM\OneToOne(targetEntity="Netik\MediaBundle\Entity\SimpleFile", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(name="banner_id", referencedColumnName="id",onDelete="SET NULL")
	 **/
	private $banner;
	
	/**
	 * @ORM\OneToOne(targetEntity="Netik\MediaBundle\Entity\Gallery", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id")
	 **/
	private $gallery;

    /**
     * @ORM\ManyToOne(targetEntity="CategoryProject", inversedBy="projects")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     **/
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="numorder", type="integer", nullable=true)
     */
    private $numOrder;

    /**
     * @var string
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="Model")
     * @ORM\JoinColumn(name="model_id", referencedColumnName="id")
     **/
    private $model;

	/**
	 * Set gallery
	 *
	 * @param \Netik\MediaBundle\Entity\Gallery $gallery
	 */
	public function setGallery( \Netik\MediaBundle\Entity\Gallery $oGallery = null ) {
		$this->gallery = $oGallery;
	}

	/**
	 * Get gallery
	 *
	 * @return \Netik\MediaBundle\Entity\Gallery
	 */
	public function getGallery() {
		return $this->gallery;
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set banner
     *
     * @param \Netik\MediaBundle\Entity\SimpleFile $banner
     *
     * @return Project
     */
    public function setBanner(\Netik\MediaBundle\Entity\SimpleFile $banner = null)
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * Get banner
     *
     * @return \Netik\MediaBundle\Entity\SimpleFile
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * Set category
     *
     * @param \Momon\BackendBundle\Entity\CategoryProject $category
     *
     * @return Project
     */
    public function setCategory(\Momon\BackendBundle\Entity\CategoryProject $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Momon\BackendBundle\Entity\CategoryProject
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set numOrder
     *
     * @param integer $numOrder
     *
     * @return Project
     */
    public function setNumOrder($numOrder)
    {
        $this->numOrder = $numOrder;

        return $this;
    }

    /**
     * Get numOrder
     *
     * @return integer
     */
    public function getNumOrder()
    {
        return $this->numOrder;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Project
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    public function __call($method, $arguments)
    {
        if($method != 'folder') {
            return $this->proxyCurrentLocaleTranslation('get' . $method, $arguments);
        }
    }

    /**
     * Set model
     *
     * @param \Momon\BackendBundle\Entity\Model $model
     *
     * @return Project
     */
    public function setModel(\Momon\BackendBundle\Entity\Model $model = null)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return \Momon\BackendBundle\Entity\Model
     */
    public function getModel()
    {
        return $this->model;
    }
}
