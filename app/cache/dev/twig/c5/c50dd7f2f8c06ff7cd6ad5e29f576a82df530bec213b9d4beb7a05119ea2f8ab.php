<?php

/* FrontendBundle:Frontend:index.html.twig */
class __TwigTemplate_e6ac6b01b235e2299a3e3bea94b22d77466862a2aa19df93d80f64f11565a904 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle:Frontend:base.html.twig", "FrontendBundle:Frontend:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle:Frontend:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8fcd3d59326aaf652fcf59ee2f878ee764bb46e0388c111fd7bb253c862e57ae = $this->env->getExtension("native_profiler");
        $__internal_8fcd3d59326aaf652fcf59ee2f878ee764bb46e0388c111fd7bb253c862e57ae->enter($__internal_8fcd3d59326aaf652fcf59ee2f878ee764bb46e0388c111fd7bb253c862e57ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontendBundle:Frontend:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8fcd3d59326aaf652fcf59ee2f878ee764bb46e0388c111fd7bb253c862e57ae->leave($__internal_8fcd3d59326aaf652fcf59ee2f878ee764bb46e0388c111fd7bb253c862e57ae_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_34e881695312d5ce48f17f86d277bb24d03632593b963ebef621af8bfbf91efe = $this->env->getExtension("native_profiler");
        $__internal_34e881695312d5ce48f17f86d277bb24d03632593b963ebef621af8bfbf91efe->enter($__internal_34e881695312d5ce48f17f86d277bb24d03632593b963ebef621af8bfbf91efe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "NOMON CLOCKS | ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.title.index"), "html", null, true);
        
        $__internal_34e881695312d5ce48f17f86d277bb24d03632593b963ebef621af8bfbf91efe->leave($__internal_34e881695312d5ce48f17f86d277bb24d03632593b963ebef621af8bfbf91efe_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_54d66fd72e9596b6861c27c5d8a9e2e51359772c585a4059a7a2973eb77a5163 = $this->env->getExtension("native_profiler");
        $__internal_54d66fd72e9596b6861c27c5d8a9e2e51359772c585a4059a7a2973eb77a5163->enter($__internal_54d66fd72e9596b6861c27c5d8a9e2e51359772c585a4059a7a2973eb77a5163_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "
    <!-- SLIDER -->
    ";
        // line 6
        if ( !(null === $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : $this->getContext($context, "oBanner")), "url", array()))) {
            // line 7
            echo "        ";
            echo $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : $this->getContext($context, "oBanner")), "url", array());
            echo "
    ";
        }
        // line 9
        echo "    <section style=\"background-image: url('";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : $this->getContext($context, "oBanner")), "banner", array()), "webpath", array())), "html", null, true);
        echo "'); background-position: 50% 0px; height: 951px;\" id=\"hero\" class=\"module-hero module-parallax module-full-height bg-film\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : $this->getContext($context, "oBanner")), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

        <!-- HERO TEXT -->
        <div class=\"hero-caption\">
            <div class=\"hero-text\">
                <h5 class=\"mh-line-size-4 font-alt m-b-50\">";
        // line 14
        echo $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : $this->getContext($context, "oBanner")), "title1", array());
        echo "</h5>
                <h1 class=\"mh-line-size-1 font-alt m-b-60\">";
        // line 15
        echo $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : $this->getContext($context, "oBanner")), "title2", array());
        echo "</h1>
                <h5 class=\"mh-line-size-5 font-alt m-b-50\">";
        // line 16
        echo $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : $this->getContext($context, "oBanner")), "title3", array());
        echo "</h5>
            </div>
        </div>
        <!-- /HERO TEXT -->

    </section>
    ";
        // line 22
        if ( !(null === $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : $this->getContext($context, "oBanner")), "url", array()))) {
            // line 23
            echo "        </a>
    ";
        }
        // line 25
        echo "    <!-- /SLIDER -->

    ";
        // line 27
        echo twig_include($this->env, $context, "FrontendBundle:IndexSection:models_index.html.twig");
        echo "

    ";
        // line 29
        echo twig_include($this->env, $context, "FrontendBundle:IndexSection:divider_index.html.twig");
        echo "

    ";
        // line 32
        echo "
    ";
        // line 33
        echo twig_include($this->env, $context, "FrontendBundle:IndexSection:divider_index.html.twig");
        echo "

    ";
        // line 36
        echo "
    ";
        // line 38
        echo "
    ";
        // line 39
        echo twig_include($this->env, $context, "FrontendBundle:IndexSection:culture_index.html.twig");
        echo "

    <hr class=\"divider\"><!-- DIVIDER -->

";
        
        $__internal_54d66fd72e9596b6861c27c5d8a9e2e51359772c585a4059a7a2973eb77a5163->leave($__internal_54d66fd72e9596b6861c27c5d8a9e2e51359772c585a4059a7a2973eb77a5163_prof);

    }

    public function getTemplateName()
    {
        return "FrontendBundle:Frontend:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 39,  125 => 38,  122 => 36,  117 => 33,  114 => 32,  109 => 29,  104 => 27,  100 => 25,  96 => 23,  94 => 22,  85 => 16,  81 => 15,  77 => 14,  66 => 9,  60 => 7,  58 => 6,  54 => 4,  48 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle:Frontend:base.html.twig' %}*/
/* {% block title %}NOMON CLOCKS | {{ 'nomon.title.index' | trans }}{% endblock %}*/
/* {% block content %}*/
/* */
/*     <!-- SLIDER -->*/
/*     {% if oBanner.url is not null %}*/
/*         {{ oBanner.url | raw }}*/
/*     {% endif %}*/
/*     <section style="background-image: url('{{ asset( oBanner.banner.webpath) }}'); background-position: 50% 0px; height: 951px;" id="hero" class="module-hero module-parallax module-full-height bg-film" data-background="{{ asset( oBanner.banner.webpath) }}">*/
/* */
/*         <!-- HERO TEXT -->*/
/*         <div class="hero-caption">*/
/*             <div class="hero-text">*/
/*                 <h5 class="mh-line-size-4 font-alt m-b-50">{{ oBanner.title1 | raw }}</h5>*/
/*                 <h1 class="mh-line-size-1 font-alt m-b-60">{{ oBanner.title2 | raw }}</h1>*/
/*                 <h5 class="mh-line-size-5 font-alt m-b-50">{{ oBanner.title3 | raw }}</h5>*/
/*             </div>*/
/*         </div>*/
/*         <!-- /HERO TEXT -->*/
/* */
/*     </section>*/
/*     {% if oBanner.url is not null %}*/
/*         </a>*/
/*     {% endif %}*/
/*     <!-- /SLIDER -->*/
/* */
/*     {{ include('FrontendBundle:IndexSection:models_index.html.twig') }}*/
/* */
/*     {{ include('FrontendBundle:IndexSection:divider_index.html.twig') }}*/
/* */
/*     {#{ include('FrontendBundle:IndexSection:projects_index.html.twig') }#}*/
/* */
/*     {{ include('FrontendBundle:IndexSection:divider_index.html.twig') }}*/
/* */
/*     {#{ include('FrontendBundle:IndexSection:collaborators_index.html.twig') }#}*/
/* */
/*     {#{ include('FrontendBundle:IndexSection:home_index.html.twig') }#}*/
/* */
/*     {{ include('FrontendBundle:IndexSection:culture_index.html.twig') }}*/
/* */
/*     <hr class="divider"><!-- DIVIDER -->*/
/* */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
