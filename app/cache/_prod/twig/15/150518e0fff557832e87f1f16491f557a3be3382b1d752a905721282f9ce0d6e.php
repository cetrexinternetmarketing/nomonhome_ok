<?php

/* FrontendBundle:Project:list.html.twig */
class __TwigTemplate_721a89fb4d2f25bfe7de3a281f0189b26b639e24151882c98e48ea86fa96f85e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle:Frontend:base.html.twig", "FrontendBundle:Project:list.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle:Frontend:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo " NOMON CLOCKS | ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.title.projects"), "html", null, true);
        echo " ";
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "<!-- HERO -->
<section style=\"background-image: url('";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oSection"]) ? $context["oSection"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "');\" class=\"module module-parallax bg-dark-30\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oSection"]) ? $context["oSection"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

    <!-- HERO TEXT -->
    <div class=\"container\">

        <div class=\"row\">
            <div class=\"col-sm-12 text-center\">
                <h1 class=\"mh-line-size-3 font-alt m-b-20\">";
        // line 13
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["oSection"]) ? $context["oSection"] : null), "title", array())), "html", null, true);
        echo "</h1>
                <h5 class=\"mh-line-size-4 font-alt\">";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oSection"]) ? $context["oSection"] : null), "slogan", array()), "html", null, true);
        echo "</h5>
            </div>
        </div>

    </div>
    <!-- /HERO TEXT -->

</section>
<!-- /HERO -->

<!-- PORTFOLIO -->
<section class=\"module\">

    <div class=\"container\">

        <!-- FILTER -->
        <div class=\"row\">

            <div class=\"col-sm-12\">
                <ul id=\"filters\" class=\"filters font-alt\">
                    <li><a href=\"#\" data-filter=\"*\" class=\"current\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.project.all"), "html", null, true);
        echo "</a></li>
                    ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aCategoryProject"]) ? $context["aCategoryProject"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oCategoryProject"]) {
            // line 36
            echo "                        <li><a href=\"#\" data-filter=\".";
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCategoryProject"], "name", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCategoryProject"], "name", array()), "html", null, true);
            echo "</a></li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oCategoryProject'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "                </ul>
            </div>

        </div>
        <!-- /FILTER -->

        <!-- WORKS GRID -->
        <div class=\"works-grid-wrapper\">

            <div style=\"position: relative; height: 1196px;\" id=\"works-grid\" class=\"works-grid works-hover-w\">

                <!-- DO NOT DELETE THIS DIV -->
                <div class=\"grid-sizer\"></div>

                ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aProjects"]) ? $context["aProjects"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oProject"]) {
            // line 53
            echo "                <!-- PORTFOLIO ITEM -->
                <div style=\"height: 229px; position: absolute; left: 0px; top: 0px;\" class=\"work-item ";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["oProject"], "category", array()), "html", null, true);
            echo "\">
                    <a href=\"";
            // line 55
            if ( !(null === $this->getAttribute($context["oProject"], "model", array()))) {
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_model_view", array("sModelSlug" => $this->getAttribute($this->getAttribute($context["oProject"], "model", array()), "slug", array()))), "html", null, true);
                echo " ";
            } else {
                echo "#";
            }
            echo "\">
                        <img src=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute($context["oProject"], "banner", array()), "webpath", array())), "html", null, true);
            echo "\" alt=\"\">
                        <div class=\"work-caption font-alt\">
                            <h3 class=\"work-title\">";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["oProject"], "name", array()), "html", null, true);
            echo "</h3>
                            <div class=\"work-descr\">
                                ";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["oProject"], "category", array()), "name", array()), "html", null, true);
            echo "
                            </div>
                        </div>
                    </a>
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oProject'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "                <!-- /PORTFOLIO ITEM -->

            </div>

        </div>
        <!-- /WORKS GRID -->

    </div>

</section>
<!-- /PORTFOLIO -->
";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Project:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 66,  148 => 60,  143 => 58,  138 => 56,  128 => 55,  124 => 54,  121 => 53,  117 => 52,  101 => 38,  90 => 36,  86 => 35,  82 => 34,  59 => 14,  55 => 13,  43 => 6,  40 => 5,  37 => 4,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle:Frontend:base.html.twig' %}*/
/* {% block title %} NOMON CLOCKS | {{ 'nomon.title.projects' | trans }} {% endblock %}*/
/* */
/* {% block content %}*/
/* <!-- HERO -->*/
/* <section style="background-image: url('{{ asset(oSection.banner.webpath) }}');" class="module module-parallax bg-dark-30" data-background="{{ asset(oSection.banner.webpath) }}">*/
/* */
/*     <!-- HERO TEXT -->*/
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/*             <div class="col-sm-12 text-center">*/
/*                 <h1 class="mh-line-size-3 font-alt m-b-20">{{ oSection.title | upper }}</h1>*/
/*                 <h5 class="mh-line-size-4 font-alt">{{ oSection.slogan }}</h5>*/
/*             </div>*/
/*         </div>*/
/* */
/*     </div>*/
/*     <!-- /HERO TEXT -->*/
/* */
/* </section>*/
/* <!-- /HERO -->*/
/* */
/* <!-- PORTFOLIO -->*/
/* <section class="module">*/
/* */
/*     <div class="container">*/
/* */
/*         <!-- FILTER -->*/
/*         <div class="row">*/
/* */
/*             <div class="col-sm-12">*/
/*                 <ul id="filters" class="filters font-alt">*/
/*                     <li><a href="#" data-filter="*" class="current">{{ 'nomon.project.all' | trans }}</a></li>*/
/*                     {% for oCategoryProject in aCategoryProject %}*/
/*                         <li><a href="#" data-filter=".{{ oCategoryProject.name }}">{{ oCategoryProject.name }}</a></li>*/
/*                     {% endfor %}*/
/*                 </ul>*/
/*             </div>*/
/* */
/*         </div>*/
/*         <!-- /FILTER -->*/
/* */
/*         <!-- WORKS GRID -->*/
/*         <div class="works-grid-wrapper">*/
/* */
/*             <div style="position: relative; height: 1196px;" id="works-grid" class="works-grid works-hover-w">*/
/* */
/*                 <!-- DO NOT DELETE THIS DIV -->*/
/*                 <div class="grid-sizer"></div>*/
/* */
/*                 {% for oProject in aProjects %}*/
/*                 <!-- PORTFOLIO ITEM -->*/
/*                 <div style="height: 229px; position: absolute; left: 0px; top: 0px;" class="work-item {{ oProject.category }}">*/
/*                     <a href="{% if oProject.model is not null %} {{ path( 'frontend_model_view', { 'sModelSlug' : oProject.model.slug } ) }} {% else %}#{% endif %}">*/
/*                         <img src="{{ asset( oProject.banner.webpath ) }}" alt="">*/
/*                         <div class="work-caption font-alt">*/
/*                             <h3 class="work-title">{{ oProject.name }}</h3>*/
/*                             <div class="work-descr">*/
/*                                 {{ oProject.category.name }}*/
/*                             </div>*/
/*                         </div>*/
/*                     </a>*/
/*                 </div>*/
/*                 {% endfor %}*/
/*                 <!-- /PORTFOLIO ITEM -->*/
/* */
/*             </div>*/
/* */
/*         </div>*/
/*         <!-- /WORKS GRID -->*/
/* */
/*     </div>*/
/* */
/* </section>*/
/* <!-- /PORTFOLIO -->*/
/* {% endblock %}*/
