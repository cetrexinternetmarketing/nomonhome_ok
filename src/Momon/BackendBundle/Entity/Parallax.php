<?php

namespace Momon\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Netik\MediaBundle\Model\FolderInterface;
use Netik\MediaBundle\Model\GalleryInterface;
use Netik\MediaBundle\Model\GalleryModel;
use Netik\MediaBundle\Model\Nueva;
use Netik\MediaBundle\Model\UploadFileModel;
use Symfony\Component\HttpFoundation\File\File;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Table(name="parallax")
 * @ORM\Entity(repositoryClass="Momon\BackendBundle\Repository\ParallaxRepository")
 */
class Parallax {

    use ORMBehaviors\Translatable\Translatable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;

	/**
	 * @ORM\OneToOne(targetEntity="Netik\MediaBundle\Entity\SimpleFile", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(name="banner_id", referencedColumnName="id",onDelete="SET NULL")
	 **/
	private $banner;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Parallax
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set banner
     *
     * @param \Netik\MediaBundle\Entity\SimpleFile $banner
     *
     * @return Parallax
     */
    public function setBanner(\Netik\MediaBundle\Entity\SimpleFile $banner = null)
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * Get banner
     *
     * @return \Netik\MediaBundle\Entity\SimpleFile
     */
    public function getBanner()
    {
        return $this->banner;
    }

    public function __call($method, $arguments)
    {
        if($method != 'folder' and $method != 'gallery') {
            return $this->proxyCurrentLocaleTranslation('get' . $method, $arguments);
        }
    }
}
