<?php

namespace Momon\BackendBundle\Repository;

/**
 * SectionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SectionRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByOrder(  )
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT s FROM BackendBundle:Section s ORDER BY s.numOrder ASC'
            )
            ->getResult();
    }
}
