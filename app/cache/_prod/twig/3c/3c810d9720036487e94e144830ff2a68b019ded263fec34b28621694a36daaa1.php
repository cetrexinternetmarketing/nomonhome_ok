<?php

/* FrontendBundle:Model:view.html.twig */
class __TwigTemplate_b065236ee1c7a968fe34a8438f7b327e04f62d857e1ab334015e85e29d662681 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle:Frontend:base.html.twig", "FrontendBundle:Model:view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle:Frontend:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "NOMON CLOCKS | ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.title.collection"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "collection", array()), "name", array()), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "name", array()), "html", null, true);
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<!-- HERO -->
<section style=\"background-image: url('";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "');\" class=\"module module-parallax bg-dark-30\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

    <!-- HERO TEXT -->
    <div class=\"container\">

        <div class=\"row\">
            <div class=\"col-sm-12 text-center\">
                <h1 class=\"mh-line-size-3 font-alt m-b-20\">";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.collection"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "collection", array()), "name", array()), "html", null, true);
        echo "</h1>
                <h5 class=\"mh-line-size-4 font-alt\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "collection", array()), "slogan", array()), "html", null, true);
        echo "</h5>
            </div>
        </div>

    </div>
    <!-- /HERO TEXT -->

</section>
<!-- /HERO -->

<!-- PRODUCT -->
<section class=\"module\">

    <div class=\"container\">

        <div class=\"row\">

            <!-- PRODUCT IMAGES -->
            <div class=\"col-sm-6 m-b-sm-40 text-center\">

                ";
        // line 33
        if ($this->getAttribute((isset($context["oImagePrincipal"]) ? $context["oImagePrincipal"] : null), "webpath", array(), "any", true, true)) {
            // line 34
            echo "                    ";
            // line 35
            echo "                    <a class=\"fancybox\" id=\"fancy-image-principal\" rel=\"gallery1\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["oImagePrincipal"]) ? $context["oImagePrincipal"] : null), "webpath", array())), "html", null, true);
            echo "\">
                        <img src=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["oImagePrincipal"]) ? $context["oImagePrincipal"] : null), "webpath", array())), "html", null, true);
            echo "\" style=\"\" id=\"image-principal\" alt=\"\">
                    </a>
                    ";
            // line 39
            echo "                ";
        }
        // line 40
        echo "

                <ul class=\"product-gallery\">
                    ";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aImages"]) ? $context["aImages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oImage"]) {
            // line 44
            echo "                    <li>
                        ";
            // line 46
            echo "                            ";
            // line 47
            echo "                        ";
            // line 48
            echo "                        ";
            // line 49
            echo "                        <div class=\"col-sm-12 image-gallery\" style=\"height: 68px; background-size: 110px; background-position: center;  background-repeat: no-repeat; background-image: url('";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["oImage"], "webpath", array())), "html", null, true);
            echo "');\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["oImage"], "webpath", array())), "html", null, true);
            echo "\" class=\"fancybox\" rel=\"gallery1\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["oImage"], "webpath", array())), "html", null, true);
            echo "\">
                        </div>
                        ";
            // line 52
            echo "                    </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oImage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "                </ul>

            </div>
            <!-- PRODUCT IMAGES -->

            <!-- PRODUCT DESC -->
            <div class=\"col-sm-6 m-b-sm-40\">

                <!-- TITLE -->
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                        <h1 class=\"product-title font-alt\">";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "name", array()), "html", null, true);
        echo "</h1>
                    </div>
                </div>

                <!-- AMOUNT >
                <div class=\"row m-b-20\">
                    <div class=\"col-sm-12\">
                        <div class=\"price font-alt\">
                            <span class=\"amount\">£20.00</span>
                        </div>
                    </div>
                </div -->

                <!-- DESCRIPTION -->
                <div class=\"row m-b-20\">
                    <div class=\"col-sm-12\">
                        <div class=\"description\">
                            <p>
                                ";
        // line 83
        echo $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "description", array());
        echo "
                            </p>
                        </div>
                    </div>
                </div>

                <!-- DESCRIPTION LIST-->
                <div class=\"row m-b-20\">
                    <div class=\"col-sm-12\">
                        <ul class=\"list-unstyled\">
                            ";
        // line 93
        if (( !(null === $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "box", array())) &&  !twig_test_empty($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "box", array())))) {
            echo "<li><strong>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.box"), "html", null, true);
            echo ":</strong> ";
            echo $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "box", array());
            echo "</li>";
        }
        // line 94
        echo "                            ";
        if (( !(null === $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "points", array())) &&  !twig_test_empty($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "points", array())))) {
            echo "<li><strong>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.points"), "html", null, true);
            echo ":</strong> ";
            echo $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "points", array());
            echo "</li>";
        }
        // line 95
        echo "                            ";
        if (( !(null === $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "hoop", array())) &&  !twig_test_empty($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "hoop", array())))) {
            echo "<li><strong>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.hoop"), "html", null, true);
            echo ":</strong> ";
            echo $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "hoop", array());
            echo "</li>";
        }
        // line 96
        echo "                            ";
        if (( !(null === $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "pendulum", array())) &&  !twig_test_empty($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "pendulum", array())))) {
            echo "<li><strong>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.pendulum"), "html", null, true);
            echo ":</strong> ";
            echo $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "pendulum", array());
            echo "</li>";
        }
        // line 97
        echo "                            ";
        if (( !(null === $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "timeSignals", array())) &&  !twig_test_empty($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "timeSignals", array())))) {
            echo "<li><strong>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.timesignals"), "html", null, true);
            echo ":</strong> ";
            echo $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "timeSignals", array());
            echo "</li>";
        }
        // line 98
        echo "                            ";
        if (( !(null === $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "mechanism", array())) &&  !twig_test_empty($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "mechanism", array())))) {
            echo "<li><strong>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.mechanism"), "html", null, true);
            echo ":</strong> ";
            echo $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "mechanism", array());
            echo "</li>";
        }
        // line 99
        echo "                            ";
        if (( !(null === $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "leg", array())) &&  !twig_test_empty($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "leg", array())))) {
            echo "<li><strong>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.leg"), "html", null, true);
            echo ":</strong> ";
            echo $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "leg", array());
            echo "</li>";
        }
        // line 100
        echo "                            ";
        if (( !(null === $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "foot", array())) &&  !twig_test_empty($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "foot", array())))) {
            echo "<li><strong>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.foot"), "html", null, true);
            echo ":</strong> ";
            echo $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "foot", array());
            echo "</li>";
        }
        // line 101
        echo "                            ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "colors", array())) > 0)) {
            // line 102
            echo "                            <li>
                                <strong>";
            // line 103
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.colors"), "html", null, true);
            echo ":</strong>
                                <div>
                                    ";
            // line 105
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "getColorsHtml", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["oColor"]) {
                // line 106
                echo "                                        <div class=\"color\">
                                            <span style=\"background: ";
                // line 107
                echo twig_escape_filter($this->env, $this->getAttribute($context["oColor"], 0, array(), "array"), "html", null, true);
                echo "\"></span>
                                            ";
                // line 108
                echo twig_escape_filter($this->env, $this->getAttribute($context["oColor"], 1, array(), "array"), "html", null, true);
                echo "
                                        </div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oColor'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 111
            echo "                                </div>
                            </li>
                            ";
        }
        // line 114
        echo "                        </ul>
                    </div>
                </div>
                <div class=\"row m-b-20\">
                    <div class=\"col-sm-4 mb-sm-20\">
                        ";
        // line 119
        if ( !(null === $this->getAttribute($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "model3d", array()), "webpath", array()))) {
            // line 120
            echo "                        <a target=\"_blank\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "model3d", array()), "webpath", array())), "html", null, true);
            echo "\">
                            <img src=\"";
            // line 121
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/icon_2d3d.png"), "html", null, true);
            echo "\">
                            ";
            // line 122
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.download"), "html", null, true);
            echo " ";
            echo "3D
                        </a>
                        ";
        }
        // line 125
        echo "                    </div>

                    <div class=\"col-sm-4 mb-sm-20\">
                    ";
        // line 128
        if ( !(null === $this->getAttribute($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "datasheet", array()), "webpath", array()))) {
            // line 129
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oModel"]) ? $context["oModel"] : null), "datasheet", array()), "webpath", array())), "html", null, true);
            echo "\" target=\"_blank\" class=\"btn btn-lg btn-block btn-round btn-b\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.datasheet"), "html", null, true);
            echo "</a>
                    ";
        }
        // line 131
        echo "                    </div>
                </div>

                <!-- CATEGORIES >
                <div class=\"row m-b-20\">
                    <div class=\"col-sm-12\">
                        <div class=\"product_meta\">
                            Categories: <a href=\"#\">Man</a>, <a href=\"#\">Clothing</a>, <a href=\"#\" rel=\"tag\">T-shirts</a>.
                        </div>
                    </div>
                </div -->

            </div>
            <!-- PRODUCT DESC -->

        </div>

    </div>

</section>
<!-- /PRODUCT -->

<hr class=\"divider\"><!-- DIVIDER -->

";
        // line 155
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FrontendBundle:Model:otherCollection", array("nCollectionId" => $this->getAttribute($this->getAttribute(        // line 156
(isset($context["oModel"]) ? $context["oModel"] : null), "collection", array()), "id", array()))));
        // line 157
        echo "

";
    }

    // line 161
    public function block_javascripts($context, array $blocks = array())
    {
        // line 162
        echo "    <script>
        \$( document ).ready(function() {
            \$( \".image-gallery\" ).click(function() {
                \$(\"#image-principal\").attr('src', \$(this).attr('src') );
                \$(\"#fancy-image-principal\").attr('href', \$(this).attr('src') );
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Model:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  372 => 162,  369 => 161,  363 => 157,  361 => 156,  360 => 155,  334 => 131,  326 => 129,  324 => 128,  319 => 125,  312 => 122,  308 => 121,  303 => 120,  301 => 119,  294 => 114,  289 => 111,  280 => 108,  276 => 107,  273 => 106,  269 => 105,  264 => 103,  261 => 102,  258 => 101,  249 => 100,  240 => 99,  231 => 98,  222 => 97,  213 => 96,  204 => 95,  195 => 94,  187 => 93,  174 => 83,  153 => 65,  140 => 54,  133 => 52,  123 => 49,  121 => 48,  119 => 47,  117 => 46,  114 => 44,  110 => 43,  105 => 40,  102 => 39,  97 => 36,  92 => 35,  90 => 34,  88 => 33,  65 => 13,  59 => 12,  47 => 5,  44 => 4,  41 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle:Frontend:base.html.twig' %}*/
/* {% block title %}NOMON CLOCKS | {{ 'nomon.title.collection' | trans }} {{ oModel.collection.name }} | {{ oModel.name }}{% endblock %}*/
/* {% block content %}*/
/* <!-- HERO -->*/
/* <section style="background-image: url('{{ asset( oModel.banner.webpath ) }}');" class="module module-parallax bg-dark-30" data-background="{{ asset( oModel.banner.webpath ) }}">*/
/* */
/*     <!-- HERO TEXT -->*/
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/*             <div class="col-sm-12 text-center">*/
/*                 <h1 class="mh-line-size-3 font-alt m-b-20">{{ 'nomon.model.collection' | trans }} {{ oModel.collection.name }}</h1>*/
/*                 <h5 class="mh-line-size-4 font-alt">{{ oModel.collection.slogan }}</h5>*/
/*             </div>*/
/*         </div>*/
/* */
/*     </div>*/
/*     <!-- /HERO TEXT -->*/
/* */
/* </section>*/
/* <!-- /HERO -->*/
/* */
/* <!-- PRODUCT -->*/
/* <section class="module">*/
/* */
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/* */
/*             <!-- PRODUCT IMAGES -->*/
/*             <div class="col-sm-6 m-b-sm-40 text-center">*/
/* */
/*                 {% if oImagePrincipal.webpath is defined  %}*/
/*                     {#<a href="{{ asset(oImagePrincipal.webpath) }}" class="gallery">#}*/
/*                     <a class="fancybox" id="fancy-image-principal" rel="gallery1" href="{{ asset(oImagePrincipal.webpath) }}">*/
/*                         <img src="{{ asset(oImagePrincipal.webpath) }}" style="" id="image-principal" alt="">*/
/*                     </a>*/
/*                     {#</a>#}*/
/*                 {% endif %}*/
/* */
/* */
/*                 <ul class="product-gallery">*/
/*                     {% for oImage in aImages %}*/
/*                     <li>*/
/*                         {#<a href="{{  asset(oImage.webpath )  }}" class="gallery">#}*/
/*                             {##}*/
/*                         {#</a>#}*/
/*                         {#<a class="fancybox" rel="gallery1" href="{{ asset(oImage.webpath) }}">#}*/
/*                         <div class="col-sm-12 image-gallery" style="height: 68px; background-size: 110px; background-position: center;  background-repeat: no-repeat; background-image: url('{{ asset( oImage.webpath ) }}');" src="{{ asset( oImage.webpath ) }}" class="fancybox" rel="gallery1" href="{{ asset(oImage.webpath) }}">*/
/*                         </div>*/
/*                         {#</a>#}*/
/*                     </li>*/
/*                     {% endfor %}*/
/*                 </ul>*/
/* */
/*             </div>*/
/*             <!-- PRODUCT IMAGES -->*/
/* */
/*             <!-- PRODUCT DESC -->*/
/*             <div class="col-sm-6 m-b-sm-40">*/
/* */
/*                 <!-- TITLE -->*/
/*                 <div class="row">*/
/*                     <div class="col-sm-12">*/
/*                         <h1 class="product-title font-alt">{{ oModel.name }}</h1>*/
/*                     </div>*/
/*                 </div>*/
/* */
/*                 <!-- AMOUNT >*/
/*                 <div class="row m-b-20">*/
/*                     <div class="col-sm-12">*/
/*                         <div class="price font-alt">*/
/*                             <span class="amount">£20.00</span>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div -->*/
/* */
/*                 <!-- DESCRIPTION -->*/
/*                 <div class="row m-b-20">*/
/*                     <div class="col-sm-12">*/
/*                         <div class="description">*/
/*                             <p>*/
/*                                 {{ oModel.description | raw }}*/
/*                             </p>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/* */
/*                 <!-- DESCRIPTION LIST-->*/
/*                 <div class="row m-b-20">*/
/*                     <div class="col-sm-12">*/
/*                         <ul class="list-unstyled">*/
/*                             {% if oModel.box is not null and oModel.box is not empty %}<li><strong>{{ 'nomon.model.box' | trans }}:</strong> {{ oModel.box | raw }}</li>{% endif %}*/
/*                             {% if oModel.points is not null and oModel.points is not empty %}<li><strong>{{ 'nomon.model.points' | trans }}:</strong> {{ oModel.points | raw }}</li>{% endif %}*/
/*                             {% if oModel.hoop is not null and oModel.hoop is not empty %}<li><strong>{{ 'nomon.model.hoop' | trans }}:</strong> {{ oModel.hoop | raw}}</li>{% endif %}*/
/*                             {% if oModel.pendulum is not null and oModel.pendulum is not empty %}<li><strong>{{ 'nomon.model.pendulum' | trans }}:</strong> {{ oModel.pendulum | raw}}</li>{% endif %}*/
/*                             {% if oModel.timeSignals is not null and oModel.timeSignals is not empty %}<li><strong>{{ 'nomon.model.timesignals' | trans }}:</strong> {{ oModel.timeSignals | raw}}</li>{% endif %}*/
/*                             {% if oModel.mechanism is not null and oModel.mechanism is not empty %}<li><strong>{{ 'nomon.model.mechanism' | trans }}:</strong> {{ oModel.mechanism | raw }}</li>{% endif %}*/
/*                             {% if oModel.leg is not null and oModel.leg is not empty %}<li><strong>{{ 'nomon.model.leg' | trans }}:</strong> {{ oModel.leg | raw }}</li>{% endif %}*/
/*                             {% if oModel.foot is not null and oModel.foot is not empty %}<li><strong>{{ 'nomon.model.foot' | trans }}:</strong> {{ oModel.foot | raw }}</li>{% endif %}*/
/*                             {% if oModel.colors | length > 0 %}*/
/*                             <li>*/
/*                                 <strong>{{ 'nomon.model.colors' | trans }}:</strong>*/
/*                                 <div>*/
/*                                     {% for oColor in oModel.getColorsHtml %}*/
/*                                         <div class="color">*/
/*                                             <span style="background: {{ oColor[0] }}"></span>*/
/*                                             {{ oColor[1] }}*/
/*                                         </div>*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </li>*/
/*                             {% endif %}*/
/*                         </ul>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="row m-b-20">*/
/*                     <div class="col-sm-4 mb-sm-20">*/
/*                         {% if oModel.model3d.webpath is not null %}*/
/*                         <a target="_blank" href="{{ asset( oModel.model3d.webpath ) }}">*/
/*                             <img src="{{ asset('bundles/frontend/images/icon_2d3d.png') }}">*/
/*                             {{ 'nomon.model.download' | trans }} {#<a target="_blank" href="{{ asset( oModel.model2d.webpath ) }}">2D</a>/#}3D*/
/*                         </a>*/
/*                         {% endif %}*/
/*                     </div>*/
/* */
/*                     <div class="col-sm-4 mb-sm-20">*/
/*                     {% if oModel.datasheet.webpath is not null %}*/
/*                         <a href="{{ asset( oModel.datasheet.webpath ) }}" target="_blank" class="btn btn-lg btn-block btn-round btn-b">{{ 'nomon.model.datasheet' | trans }}</a>*/
/*                     {% endif %}*/
/*                     </div>*/
/*                 </div>*/
/* */
/*                 <!-- CATEGORIES >*/
/*                 <div class="row m-b-20">*/
/*                     <div class="col-sm-12">*/
/*                         <div class="product_meta">*/
/*                             Categories: <a href="#">Man</a>, <a href="#">Clothing</a>, <a href="#" rel="tag">T-shirts</a>.*/
/*                         </div>*/
/*                     </div>*/
/*                 </div -->*/
/* */
/*             </div>*/
/*             <!-- PRODUCT DESC -->*/
/* */
/*         </div>*/
/* */
/*     </div>*/
/* */
/* </section>*/
/* <!-- /PRODUCT -->*/
/* */
/* <hr class="divider"><!-- DIVIDER -->*/
/* */
/* {{ render(controller(*/
/* 'FrontendBundle:Model:otherCollection', {'nCollectionId': oModel.collection.id}*/
/* )) }}*/
/* */
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     <script>*/
/*         $( document ).ready(function() {*/
/*             $( ".image-gallery" ).click(function() {*/
/*                 $("#image-principal").attr('src', $(this).attr('src') );*/
/*                 $("#fancy-image-principal").attr('href', $(this).attr('src') );*/
/*             });*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
