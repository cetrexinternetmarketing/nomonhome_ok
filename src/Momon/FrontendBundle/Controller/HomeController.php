<?php

namespace Momon\FrontendBundle\Controller;

use Netik\MediaBundle\Collection\ImageCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\Common\Collections\ArrayCollection;

class HomeController extends Controller {

	/**
	 * @Route("/nomonhome", name="frontend_home_index")
	 * @Template()
	 */
	public function indexAction() {
		$oManager = $this->getDoctrine()->getManager();

		$aHomeModels = $oManager->getRepository( 'BackendBundle:HomeModel' )->findByOrder( );
		$oClocksParallax = $oManager->getRepository('BackendBundle:Parallax')->findByName('nomon_clocks');
        $oBanner = $oManager->getRepository('BackendBundle:Banner')->findOneByName('home');

		$aCollections = $oManager->getRepository('BackendBundle:CollectionHome')->findByOrder();

		$oNovedadesCollection = $oManager->getRepository('BackendBundle:CollectionHome')->find(2);
		$oNovedadesCollectionModels = $oManager->getRepository('BackendBundle:HomeModel')->findByIsNew();

		$aCollections = new ArrayCollection($aCollections);
		$aCollections->removeElement($oNovedadesCollection);
        
		return array(
			'oClocksParallax' => $oClocksParallax[0],
			'aHomeModels' => $aHomeModels,
            'oBanner' => $oBanner,
			'oNovedadesCollection' => $oNovedadesCollection,
			'oNovedadesCollectionModels' => $oNovedadesCollectionModels,
			'aCollections' => $aCollections,
		);
	}

	/**
	 * @Template()
	 */
	public function menuAction(){

		$oManager = $this->getDoctrine()->getManager();

		$aHomeModels = $oManager->getRepository( 'BackendBundle:HomeModel' )->findByOrder( );
		$aCollections = $oManager->getRepository('BackendBundle:CollectionHome')->findByOrder();

		$oNovedadesCollection = $oManager->getRepository('BackendBundle:CollectionHome')->find(2);
		$oNovedadesCollectionModels = $oManager->getRepository('BackendBundle:HomeModel')->findByIsNew();

		$aCollections = new ArrayCollection($aCollections);
		$aCollections->removeElement($oNovedadesCollection);

		return array(
			'aCollection' => $aCollections,
			'oMenuNovedadesCollection' => $oNovedadesCollection,
			'aMenuNovedadesModels' => $oNovedadesCollectionModels
		);

	}
}