<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\Banner;
use Momon\BackendBundle\Form\BannerType;

/**
 * Banner controller.
 *
 * @Route("/banner")
 */
class BannerController extends Controller
{

    /**
     * INDEX ACTION - Banner.
     *
     * @Route("/", name="backend_banner_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aBanners = $em->getRepository( 'BackendBundle:Banner' )->findAll();

        return array(
            'aBanners' => $aBanners,
        );
    }
    /**
     * CREATE ACTION - Banner.
     *
     * @Route("/create", name="backend_banner_create")
     * @Template()
     */
    public function createAction( Request $oRequest )
    {
        $oBanner = new Banner();
        $oForm = $this->createForm( new BannerType(), $oBanner );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );

            if( $oForm->isValid() ){
                $em = $this->getDoctrine()->getManager();
                $em->persist( $oBanner );
                $em->flush();

                $this->addFlash(
                    'success',
                    'Creado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_banner_view', array( 'nBannerId' => $oBanner->getId() ) ) );            }
        }

        return array(
            'oBanner' => $oBanner,
            'oForm'   => $oForm->createView(),
        );
    }
    /**
     * VIEW ACTION - Banner.
     *
     * @Route("/view/{nBannerId}", name="backend_banner_view")
     * @Template()
     */
    public function viewAction( $nBannerId )
    {
        $em = $this->getDoctrine()->getManager();

        $oBanner = $em->getRepository( 'BackendBundle:Banner' )->find( $nBannerId );

        if ( !$oBanner ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Banner' );
        }

        $oForm = $this->createForm( new BannerType(), $oBanner );

        return array(
            'oBanner' => $oBanner,
            'oForm' => $oForm->createView()
        );
    }

    /**
     * EDIT ACTION - Banner.
     *
     * @Route("/edit/{nBannerId}", name="backend_banner_edit")
     * @Template()
     */
    public function editAction( $nBannerId, Request $oRequest )
    {
        $em = $this->getDoctrine()->getManager();

        $oBanner = $em->getRepository( 'BackendBundle:Banner' )->find( $nBannerId );

        if ( !$oBanner ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Banner' );
        }

        $oForm = $this->createForm( new BannerType(), $oBanner );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );
            if( $oForm->isValid() ){
                $em->flush();

                $this->addFlash(
                'success',
                'Editado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_banner_view', array( 'nBannerId' => $nBannerId ) ) );
            }
        }

        return array(
            'oBanner' => $oBanner,
            'oForm'         => $oForm->createView(),
        );
    }
    /**
     * DELETE ACTION - Banner.
     *
     * @Route("/delete/{nBannerId}", name="backend_banner_delete")
     */
    public function deleteAction( $nBannerId )
    {
        $em = $this->getDoctrine()->getManager();

        $oBanner = $em->getRepository( 'BackendBundle:Banner' )->find( $nBannerId );

        if ( !$oBanner ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Banner' );
        }

        $em->remove( $oBanner );
        $em->flush();

        $this->addFlash(
        'success',
        'Eliminado correctamente!'
        );

        return $this->redirect( $this->generateUrl( 'backend_banner_index' ) );
    }}
