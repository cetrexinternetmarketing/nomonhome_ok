<?php

/* FrontendBundle:Home:base.html.twig */
class __TwigTemplate_c1b97d8cbfc6b62736f146635368ab9b3cea15223d62fb6ad702d70c69279e6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">


    <title>NOMON HOME | ";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        echo "</title>


    <!-- Favicons -->
    <link rel=\"shortcut icon\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/favicon.ico"), "html", null, true);
        echo "\">
    <link rel=\"apple-touch-icon\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/apple-touch-icon.png"), "html", null, true);
        echo "\">
    <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/apple-touch-icon-72x72.png"), "html", null, true);
        echo "\">
    <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/apple-touch-icon-114x114.png"), "html", null, true);
        echo "\">

    <!-- Bootstrap core CSS -->
    <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Tipo icono -->
    <link href=\"http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/icons.css"), "html", null, true);
        echo "\">

    <!-- Plugins -->
    <!-- link href=\"content/font-awesome.css\" rel=\"stylesheet\">
    <link href=\"content/font-awesome.min.css\" rel=\"stylesheet\" -->
    <link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/et-line-font.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/simpletextrotator.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/magnific-popup.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/owl.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/superslides.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/vertical.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/animate.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Template core CSS -->
    <link href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <style id=\"fit-vids-style\">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>
    <script style=\"\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/common.js"), "html", null, true);
        echo "\" charset=\"UTF-8\" type=\"text/javascript\"></script>
    <script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/util.js"), "html", null, true);
        echo "\" charset=\"UTF-8\" type=\"text/javascript\"></script>
    <script src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/stats.js"), "html", null, true);
        echo "\" charset=\"UTF-8\" type=\"text/javascript\"></script>
    <script src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/AuthenticationService.js"), "html", null, true);
        echo "\" charset=\"UTF-8\" type=\"text/javascript\"></script>

    <link href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/fancybox/jquery.fancybox.css?v=2.1.5"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/nomon.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
</head>
<body>
<!-- PRELOADER -->
<div style=\"display: none;\" class=\"page-loader\">
    <div style=\"display: none;\" class=\"loader\">Loading...</div>
</div>
<!-- /PRELOADER -->

<!-- WRAPPER -->
<div class=\"wrapper\">
    <div style=\"position: fixed; right: 0; left: 0; z-index: 999999;\" id=\"selector-idiomas\">
        <div class=\"container\">
            <div style=\"float: right; font-size: 11px; margin-top: 5px;\">
                <a style=\"margin-right: 5px\" href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), twig_array_merge($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route_params"), "method"), array("_locale" => "es"))), "html", null, true);
        echo "\">ES </a>
                <span>|</span>
                <a style=\"margin-left: 5px\" href=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), twig_array_merge($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route_params"), "method"), array("_locale" => "en"))), "html", null, true);
        echo "\"> EN </a>
            </div>
        </div>
    </div>
    ";
        // line 68
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FrontendBundle:Home:menu"));
        // line 70
        echo "
    ";
        // line 71
        $this->displayBlock('content', $context, $blocks);
        // line 73
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FrontendBundle:Frontend:footer"));
        // line 75
        echo "
</div>

<!-- SCROLLTOP -->
<div style=\"display: none;\" class=\"scroll-up\">
    <a href=\"#totop\"><i class=\"fa fa-angle-double-up\"></i></a>
</div>

<!-- Javascript files -->
<script src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery-2.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_003.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_006.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_007.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/owl.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_002.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/imagesloaded.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/isotope.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/packery-mode.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/appear.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_005.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/wow.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jqBootstrapValidation.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_004.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/js.js"), "html", null, true);
        echo "\"></script><script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/main.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/gmaps.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/contact.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/custom.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/fancybox/jquery.fancybox.pack.js?v=2.1.5"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    \$(document).ready(function() {
        \$(\".fancybox\").fancybox();
    });
</script>

";
        // line 112
        $this->displayBlock('javascripts', $context, $blocks);
        // line 114
        echo "</body>
</html>

";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        echo "Relojes";
    }

    // line 71
    public function block_content($context, array $blocks = array())
    {
        // line 72
        echo "    ";
    }

    // line 112
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Home:base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  296 => 112,  292 => 72,  289 => 71,  283 => 11,  276 => 114,  274 => 112,  264 => 105,  259 => 103,  255 => 102,  251 => 101,  245 => 100,  241 => 99,  237 => 98,  233 => 97,  229 => 96,  225 => 95,  221 => 94,  217 => 93,  213 => 92,  209 => 91,  205 => 90,  201 => 89,  197 => 88,  193 => 87,  189 => 86,  185 => 85,  181 => 84,  170 => 75,  167 => 73,  165 => 71,  162 => 70,  160 => 68,  153 => 64,  148 => 62,  131 => 48,  126 => 46,  121 => 44,  117 => 43,  113 => 42,  109 => 41,  104 => 39,  98 => 36,  94 => 35,  90 => 34,  86 => 33,  82 => 32,  78 => 31,  74 => 30,  66 => 25,  59 => 21,  53 => 18,  49 => 17,  45 => 16,  41 => 15,  34 => 11,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* <head>*/
/*     <meta http-equiv="content-type" content="text/html; charset=UTF-8">*/
/*     <meta charset="UTF-8">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/*     <meta name="description" content="">*/
/*     <meta name="author" content="">*/
/* */
/* */
/*     <title>NOMON HOME | {% block title %}Relojes{% endblock %}</title>*/
/* */
/* */
/*     <!-- Favicons -->*/
/*     <link rel="shortcut icon" href="{{ asset('bundles/frontend/images/favicon.ico') }}">*/
/*     <link rel="apple-touch-icon" href="{{ asset('bundles/frontend/images/apple-touch-icon.png')  }}">*/
/*     <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('bundles/frontend/images/apple-touch-icon-72x72.png') }}">*/
/*     <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('bundles/frontend/images/apple-touch-icon-114x114.png') }}">*/
/* */
/*     <!-- Bootstrap core CSS -->*/
/*     <link href="{{ asset('bundles/frontend/content/bootstrap.css') }}" rel="stylesheet">*/
/* */
/*     <!-- Tipo icono -->*/
/*     <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">*/
/*     <link rel="stylesheet" type="text/css" href="{{ asset('bundles/frontend/icons.css') }}">*/
/* */
/*     <!-- Plugins -->*/
/*     <!-- link href="content/font-awesome.css" rel="stylesheet">*/
/*     <link href="content/font-awesome.min.css" rel="stylesheet" -->*/
/*     <link href="{{ asset('bundles/frontend/content/et-line-font.css') }}" rel="stylesheet">*/
/*     <link href="{{ asset('bundles/frontend/content/simpletextrotator.css' ) }}" rel="stylesheet">*/
/*     <link href="{{ asset('bundles/frontend/content/magnific-popup.css' ) }}" rel="stylesheet">*/
/*     <link href="{{ asset('bundles/frontend/content/owl.css' ) }}" rel="stylesheet">*/
/*     <link href="{{ asset('bundles/frontend/content/superslides.css' ) }}" rel="stylesheet">*/
/*     <link href="{{ asset('bundles/frontend/content/vertical.css' ) }}" rel="stylesheet">*/
/*     <link href="{{ asset('bundles/frontend/content/animate.css' ) }}" rel="stylesheet">*/
/* */
/*     <!-- Template core CSS -->*/
/*     <link href="{{ asset('bundles/frontend/content/style.css') }}" rel="stylesheet">*/
/*     <style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>*/
/*     <script style="" src="{{ asset('bundles/frontend/content/common.js')}}" charset="UTF-8" type="text/javascript"></script>*/
/*     <script src="{{ asset('bundles/frontend/content/util.js')}}" charset="UTF-8" type="text/javascript"></script>*/
/*     <script src="{{ asset('bundles/frontend/content/stats.js' ) }}" charset="UTF-8" type="text/javascript"></script>*/
/*     <script src="{{ asset('bundles/frontend/content/AuthenticationService.js' ) }}" charset="UTF-8" type="text/javascript"></script>*/
/* */
/*     <link href="{{ asset('bundles/frontend/fancybox/jquery.fancybox.css?v=2.1.5') }}" rel="stylesheet">*/
/* */
/*     <link href="{{ asset('bundles/frontend/content/nomon.css') }}" rel="stylesheet">*/
/* </head>*/
/* <body>*/
/* <!-- PRELOADER -->*/
/* <div style="display: none;" class="page-loader">*/
/*     <div style="display: none;" class="loader">Loading...</div>*/
/* </div>*/
/* <!-- /PRELOADER -->*/
/* */
/* <!-- WRAPPER -->*/
/* <div class="wrapper">*/
/*     <div style="position: fixed; right: 0; left: 0; z-index: 999999;" id="selector-idiomas">*/
/*         <div class="container">*/
/*             <div style="float: right; font-size: 11px; margin-top: 5px;">*/
/*                 <a style="margin-right: 5px" href="{{ path(app.request.attributes.get('_route'), app.request.attributes.get('_route_params')|merge({'_locale': 'es'})) }}">ES </a>*/
/*                 <span>|</span>*/
/*                 <a style="margin-left: 5px" href="{{ path(app.request.attributes.get('_route'), app.request.attributes.get('_route_params')|merge({'_locale': 'en'})) }}"> EN </a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     {{ render(controller(*/
/*     'FrontendBundle:Home:menu'*/
/*     )) }}*/
/*     {% block content %}*/
/*     {% endblock %}*/
/*     {{ render(controller(*/
/*     'FrontendBundle:Frontend:footer'*/
/*     )) }}*/
/* </div>*/
/* */
/* <!-- SCROLLTOP -->*/
/* <div style="display: none;" class="scroll-up">*/
/*     <a href="#totop"><i class="fa fa-angle-double-up"></i></a>*/
/* </div>*/
/* */
/* <!-- Javascript files -->*/
/* <script src="{{ asset('bundles/frontend/content/jquery-2.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/bootstrap.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/jquery_003.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/jquery_006.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/jquery_007.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/owl.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/jquery_002.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/imagesloaded.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/isotope.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/packery-mode.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/appear.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/jquery_005.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/wow.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/jqBootstrapValidation.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/jquery.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/jquery_004.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/js.js')}}"></script><script src="{{ asset('bundles/frontend/content/main.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/gmaps.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/contact.js')}}"></script>*/
/* <script src="{{ asset('bundles/frontend/content/custom.js')}}"></script>*/
/* */
/* <script src="{{ asset('bundles/frontend/fancybox/jquery.fancybox.pack.js?v=2.1.5')}}"></script>*/
/* <script type="text/javascript">*/
/*     $(document).ready(function() {*/
/*         $(".fancybox").fancybox();*/
/*     });*/
/* </script>*/
/* */
/* {% block javascripts %}*/
/* {% endblock %}*/
/* </body>*/
/* </html>*/
/* */
/* */
