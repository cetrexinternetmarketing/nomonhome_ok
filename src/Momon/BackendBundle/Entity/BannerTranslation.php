<?php


namespace Momon\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class BannerTranslation {

    use ORMBehaviors\Translatable\Translation;

    /**
     * @var string
     *
     * @ORM\Column(name="title1", type="string", length=255, nullable=true)
     */
    protected $title1;

    /**
     * @var string
     *
     * @ORM\Column(name="title2", type="string", length=255, nullable=true)
     */
    protected $title2;

    /**
     * @var string
     *
     * @ORM\Column(name="title3", type="string", length=255, nullable=true)
     */
    protected $title3;


    /**
     * Set title
     *
     * @param string $title
     *
     * @return Parallax
     */
    public function setTitle1($title)
    {
        $this->title1 = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle1()
    {
        return $this->title1;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Parallax
     */
    public function setTitle2($title)
    {
        $this->title2 = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle2()
    {
        return $this->title2;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Parallax
     */
    public function setTitle3($title)
    {
        $this->title3 = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle3()
    {
        return $this->title3;
    }
}
