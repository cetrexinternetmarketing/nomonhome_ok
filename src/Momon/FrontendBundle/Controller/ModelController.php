<?php

namespace Momon\FrontendBundle\Controller;

use Netik\MediaBundle\Collection\ImageCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ModelController extends Controller
{
	/**
	 * @Route("/model/{sModelSlug}", name="frontend_model_view")
	 * @Template()
	 */
	public function viewAction( $sModelSlug )
	{
		$oManager = $this->getDoctrine()->getManager();
		$oModel = $oManager->getRepository('BackendBundle:Model')->findOneBySlug( $sModelSlug );

		$oGallery = $oModel->getGallery();

		$aImages = new ImageCollection( $oManager->getRepository('MediaBundle:Image')->findByGalleryOrdered( $oGallery->getId() ) );

		return array(
			'oImagePrincipal' => $aImages->first(),
			'aImages' => $aImages,
			'oModel' => $oModel
		);
	}

	/**
	 * @Template()
	 */
	public function otherCollectionAction( $nCollectionId = null ){

		$oManager = $this->getDoctrine()->getManager();

		if( $nCollectionId ) {
			$aModels = $oManager->getRepository( 'BackendBundle:Model' )->findByCollection( $nCollectionId );
		}else{
			$aModels = $oManager->getRepository('BackendBundle:Model')->findAll();
		}

		$max = count( $aModels ) >= 4 ? 4 : count( $aModels );
		$aKeys = array_rand($aModels, $max);

		if (is_array( $aKeys ) ) {
			$aModels = array_intersect_key( $aModels, array_flip( $aKeys ) );
		}
		else{
			$aModels = array( $aModels[$aKeys] );
		}


		return array(
			'aModels' => $aModels
		);
	}
}
