<?php

/* FrontendBundle:IndexSection:projects_index.html.twig */
class __TwigTemplate_662386104af672da8fe14d3ac43cefa34482d1a93a8b79f688403198a63739ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- PROYECTOS -->
<section id=\"portfolio\" class=\"module\">

    <div class=\"container\">

        <!-- MODULE TITLE -->
        <div class=\"row\">
            <div class=\"col-sm-6 col-sm-offset-3\">
                <h2 class=\"module-title font-alt\">";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.menu.projects"), "html", null, true);
        echo "</h2>
            </div>
        </div>
        <!-- /MODULE TITLE -->

        <!-- FILTER -->
        <div class=\"row\">

            <div class=\"col-sm-12\">
                <ul id=\"filters\" class=\"filters font-alt\">
                    <li><a href=\"#\" data-filter=\"*\" class=\"current\">TODOS</a></li>
                    ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aCategoryProjects"]) ? $context["aCategoryProjects"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oCategoryProject"]) {
            // line 21
            echo "                        <li><a href=\"#\" data-filter=\".";
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCategoryProject"], "name", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCategoryProject"], "name", array()), "html", null, true);
            echo "</a></li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oCategoryProject'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "                </ul>
            </div>

        </div>
        <!-- /FILTER -->

        <!-- WORKS GRID -->
        <div class=\"works-grid-wrapper\">

            <div style=\"position: relative; height: 1196px;\" id=\"works-grid\" class=\"works-grid works-hover-w\">

                <!-- DO NOT DELETE THIS DIV -->
                <div class=\"grid-sizer\"></div>

                ";
        // line 37
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aProjects"]) ? $context["aProjects"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oProject"]) {
            // line 38
            echo "                    <!-- PORTFOLIO ITEM -->
                    <div style=\"height: 229px; position: absolute; left: 0px; top: 0px;\" class=\"work-item ";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["oProject"], "category", array()), "name", array()), "html", null, true);
            echo "\">
                        <a href=\"";
            // line 40
            if ( !(null === $this->getAttribute($context["oProject"], "model", array()))) {
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_model_view", array("sModelSlug" => $this->getAttribute($this->getAttribute($context["oProject"], "model", array()), "slug", array()))), "html", null, true);
                echo " ";
            } else {
                echo "#";
            }
            echo "\">
                            <img src=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute($context["oProject"], "banner", array()), "webpath", array())), "html", null, true);
            echo "\" alt=\"\">
                            <div class=\"work-caption font-alt\">
                                <h3 class=\"work-title\">";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["oProject"], "name", array()), "html", null, true);
            echo "</h3>
                                <div class=\"work-descr\">
                                    ";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["oProject"], "category", array()), "name", array()), "html", null, true);
            echo "
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- /PORTFOLIO ITEM -->
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oProject'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "
            </div>

        </div>
        <!-- /WORKS GRID -->

        <!-- SHOW MORE -->
        <div class=\"row m-t-70 text-center\">
            <div class=\"col-sm-12\">

                <a id=\"\" class=\"btn btn-round btn-b\" href=\"";
        // line 62
        echo $this->env->getExtension('routing')->getPath("frontend_project_list");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.home.moreprojects"), "html", null, true);
        echo "</a>

            </div>
        </div>
        <!-- /SHOW MORE -->

    </div>

</section>
<!-- /PROYECTOS -->";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:IndexSection:projects_index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 62,  118 => 52,  105 => 45,  100 => 43,  95 => 41,  85 => 40,  81 => 39,  78 => 38,  74 => 37,  58 => 23,  47 => 21,  43 => 20,  29 => 9,  19 => 1,);
    }
}
/* <!-- PROYECTOS -->*/
/* <section id="portfolio" class="module">*/
/* */
/*     <div class="container">*/
/* */
/*         <!-- MODULE TITLE -->*/
/*         <div class="row">*/
/*             <div class="col-sm-6 col-sm-offset-3">*/
/*                 <h2 class="module-title font-alt">{{ 'nomon.menu.projects' | trans }}</h2>*/
/*             </div>*/
/*         </div>*/
/*         <!-- /MODULE TITLE -->*/
/* */
/*         <!-- FILTER -->*/
/*         <div class="row">*/
/* */
/*             <div class="col-sm-12">*/
/*                 <ul id="filters" class="filters font-alt">*/
/*                     <li><a href="#" data-filter="*" class="current">TODOS</a></li>*/
/*                     {% for oCategoryProject in aCategoryProjects %}*/
/*                         <li><a href="#" data-filter=".{{ oCategoryProject.name }}">{{ oCategoryProject.name }}</a></li>*/
/*                     {% endfor %}*/
/*                 </ul>*/
/*             </div>*/
/* */
/*         </div>*/
/*         <!-- /FILTER -->*/
/* */
/*         <!-- WORKS GRID -->*/
/*         <div class="works-grid-wrapper">*/
/* */
/*             <div style="position: relative; height: 1196px;" id="works-grid" class="works-grid works-hover-w">*/
/* */
/*                 <!-- DO NOT DELETE THIS DIV -->*/
/*                 <div class="grid-sizer"></div>*/
/* */
/*                 {% for oProject in aProjects %}*/
/*                     <!-- PORTFOLIO ITEM -->*/
/*                     <div style="height: 229px; position: absolute; left: 0px; top: 0px;" class="work-item {{ oProject.category.name }}">*/
/*                         <a href="{% if oProject.model is not null %} {{ path( 'frontend_model_view', { 'sModelSlug' : oProject.model.slug } ) }} {% else %}#{% endif %}">*/
/*                             <img src="{{ asset( oProject.banner.webpath ) }}" alt="">*/
/*                             <div class="work-caption font-alt">*/
/*                                 <h3 class="work-title">{{ oProject.name }}</h3>*/
/*                                 <div class="work-descr">*/
/*                                     {{ oProject.category.name }}*/
/*                                 </div>*/
/*                             </div>*/
/*                         </a>*/
/*                     </div>*/
/*                     <!-- /PORTFOLIO ITEM -->*/
/*                 {% endfor %}*/
/* */
/*             </div>*/
/* */
/*         </div>*/
/*         <!-- /WORKS GRID -->*/
/* */
/*         <!-- SHOW MORE -->*/
/*         <div class="row m-t-70 text-center">*/
/*             <div class="col-sm-12">*/
/* */
/*                 <a id="" class="btn btn-round btn-b" href="{{ path( 'frontend_project_list') }}">{{ 'nomon.home.moreprojects' | trans }}</a>*/
/* */
/*             </div>*/
/*         </div>*/
/*         <!-- /SHOW MORE -->*/
/* */
/*     </div>*/
/* */
/* </section>*/
/* <!-- /PROYECTOS -->*/
