<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\Model;
use Momon\BackendBundle\Form\ModelType;

/**
 * Model controller.
 *
 * @Route("/model")
 */
class ModelController extends Controller
{

    /**
     * INDEX ACTION - Model.
     *
     * @Route("/", name="backend_model_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aModels = $em->getRepository( 'BackendBundle:Model' )->findAll();

        return array(
            'aModels' => $aModels,
        );
    }
    /**
     * CREATE ACTION - Model.
     *
     * @Route("/create", name="backend_model_create")
     * @Template()
     */
    public function createAction( Request $oRequest )
    {
        $oModel = new Model();
        $oForm = $this->createForm( new ModelType(), $oModel );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );

            if( $oForm->isValid() ){
                $em = $this->getDoctrine()->getManager();
                $em->persist( $oModel );
                $em->flush();

                $this->addFlash(
                    'success',
                    'Creado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_model_view', array( 'nModelId' => $oModel->getId() ) ) );            }
        }

        return array(
            'oModel' => $oModel,
            'oForm'   => $oForm->createView(),
        );
    }
    /**
     * VIEW ACTION - Model.
     *
     * @Route("/view/{nModelId}", name="backend_model_view")
     * @Template()
     */
    public function viewAction( $nModelId )
    {
        $em = $this->getDoctrine()->getManager();

        $oModel = $em->getRepository( 'BackendBundle:Model' )->find( $nModelId );

        if ( !$oModel ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Model' );
        }

        $oForm = $this->createForm( new ModelType(), $oModel );

        return array(
            'oModel' => $oModel,
            'oForm' => $oForm->createView()
        );
    }

    /**
     * EDIT ACTION - Model.
     *
     * @Route("/edit/{nModelId}", name="backend_model_edit")
     * @Template()
     */
    public function editAction( $nModelId, Request $oRequest )
    {
        $em = $this->getDoctrine()->getManager();

        $oModel = $em->getRepository( 'BackendBundle:Model' )->find( $nModelId );

        if ( !$oModel ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Model' );
        }

        $oForm = $this->createForm( new ModelType(), $oModel );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );
            if( $oForm->isValid() ){
                $em->flush();

                $this->addFlash(
                'success',
                'Editado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_model_view', array( 'nModelId' => $nModelId ) ) );
            }
        }

        return array(
            'oModel' => $oModel,
            'oForm'         => $oForm->createView(),
        );
    }
    /**
     * DELETE ACTION - Model.
     *
     * @Route("/delete/{nModelId}", name="backend_model_delete")
     */
    public function deleteAction( $nModelId )
    {
        $em = $this->getDoctrine()->getManager();

        $oModel = $em->getRepository( 'BackendBundle:Model' )->find( $nModelId );

        if ( !$oModel ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Model' );
        }

        $em->remove( $oModel );
        $em->flush();

        $this->addFlash(
        'success',
        'Eliminado correctamente!'
        );

        return $this->redirect( $this->generateUrl( 'backend_model_index' ) );
    }}
