<?php

namespace Momon\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller {

	/**
	 * @Route("/page/{sSlug}", name="frontend_page_view")
	 * @Template()
	 */
	public function viewAction( $sSlug ){

		$oManager = $this->getDoctrine()->getManager();

		$oPage = $oManager->getRepository( 'BackendBundle:Page' )->findOneBySlug( $sSlug );


		if(!$oPage){
			throw $this->createNotFoundException( 'No se ha podido encontrar la página' );
		}

		return array(
			'oPage' => $oPage
		);
	}
}