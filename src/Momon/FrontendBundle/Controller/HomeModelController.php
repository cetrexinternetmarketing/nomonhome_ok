<?php

namespace Momon\FrontendBundle\Controller;

use Netik\MediaBundle\Collection\ImageCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class HomeModelController extends Controller {

	/**
	 * @Route("/nomonhome/view/{sHomeModel}", name="frontend_homemodel_view")
	 * @Template()
	 */
	public function viewAction( $sHomeModel ) {
		$oManager = $this->getDoctrine()->getManager();

		$oHomeModel = $oManager->getRepository( 'BackendBundle:HomeModel' )->findOneBySlug( $sHomeModel );
		$oGallery = $oHomeModel->getGallery();

		$aImages = new ImageCollection( $oManager->getRepository('MediaBundle:Image')->findByGalleryOrdered( $oGallery->getId() ) );

		return array(
			'oImagePrincipal' => $aImages->first(),
			'aImages' => $aImages,
			'oHomeModel' => $oHomeModel
		);
	}

	/**
	 * @Template()
	 */
	public function otherModelAction( $nCollectionId = null ){

		$oManager = $this->getDoctrine()->getManager();

		if( $nCollectionId ) {
			$aModels = $oManager->getRepository('BackendBundle:HomeModel')->findByCollection( $nCollectionId );
		}else{
			$aModels = $oManager->getRepository('BackendBundle:HomeModel')->findAll();
		}

		$max = count( $aModels ) >= 4 ? 4 : count( $aModels );
		$aKeys = array_rand($aModels, $max);

		if (is_array( $aKeys ) ) {
			$aModels = array_intersect_key( $aModels, array_flip( $aKeys ) );
		}
		else{
			$aModels = array( $aModels[$aKeys] );
		}


		return array(
			'aModels' => $aModels
		);
	}

}