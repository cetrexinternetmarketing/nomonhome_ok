<?php

/* FrontendBundle:Home:menu.html.twig */
class __TwigTemplate_68336b7d72e9f3b2e8e382e82e6de59a9ae8f50b9a9a62058b977c6d650ac504 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- NAVIGATION -->
<nav class=\"navbar navbar-custom navbar-fixed-top navbar-transparent\">

    <div class=\"container\">

        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#custom-collapse\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>

            <!-- YOU LOGO HERE -->
            <a class=\"navbar-brand\" href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("frontend_home_index");
        echo "\">
                <!-- IMAGE OR SIMPLE TEXT -->
                <img class=\"dark-logo\" src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/logonomon_black.png"), "html", null, true);
        echo "\" alt=\"\" width=\"95\">
                <img class=\"light-logo\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/logonomon_white.png"), "html", null, true);
        echo "\" alt=\"\" width=\"95\">
            </a>
        </div>

        <div class=\"collapse navbar-collapse\" id=\"custom-collapse\">

            <ul class=\"nav navbar-nav navbar-right\">
                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">NOMON WORLD</a>
                    <ul class=\"dropdown-menu\" role=\"menu\">

                        <li class=\"dropdown\">
                            <a href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("frontend_page_view", array("sSlug" => "home-saber-hacer"));
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.menu.saberhacer"), "html", null, true);
        echo "</a>
                        </li>
                        
                        ";
        // line 42
        echo "
                        <li class=\"dropdown\">
                            <a href=\"";
        // line 44
        echo $this->env->getExtension('routing')->getPath("frontend_page_view", array("sSlug" => "home-customer-service"));
        echo "\">CUSTOMER SERVICE</a>
                        </li>
                    </ul>
                </li>

                <li class=\"dropdown\">
                    <a href=\"";
        // line 50
        echo $this->env->getExtension('routing')->getPath("frontend_homecollection_list");
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.menu.collections"), "html", null, true);
        echo "</a>
                    <ul class=\"dropdown-menu\" role=\"menu\">
                        ";
        // line 57
        echo "                        ";
        if ((twig_length_filter($this->env, (isset($context["aMenuNovedadesModels"]) ? $context["aMenuNovedadesModels"] : null)) > 0)) {
            // line 58
            echo "                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oMenuNovedadesCollection"]) ? $context["oMenuNovedadesCollection"] : null), "name", array()), "html", null, true);
            echo "</a>
                                <ul class=\"dropdown-menu\">
                                    ";
            // line 61
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["aMenuNovedadesModels"]) ? $context["aMenuNovedadesModels"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["oModel"]) {
                // line 62
                echo "                                        <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_homemodel_view", array("sHomeModel" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["oModel"], "name", array()), "html", null, true);
                echo "</a></li>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oModel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "                                </ul>
                            </li>
                        ";
        }
        // line 67
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aCollection"]) ? $context["aCollection"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oCollection"]) {
            // line 68
            echo "                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCollection"], "name", array()), "html", null, true);
            echo "</a>
                                <ul class=\"dropdown-menu\">
                                    ";
            // line 71
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["oCollection"], "models", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["oModel"]) {
                // line 72
                echo "                                        <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_homemodel_view", array("sHomeModel" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["oModel"], "name", array()), "html", null, true);
                echo "</a></li>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oModel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 74
            echo "                                </ul>
                            </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oCollection'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "
                    </ul>
                </li>

                <li>
                    <a href=\"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("frontend_frontend_index");
        echo "\" style=\"font-weight:bold\">NOMON CLOCKS</a>
                </li>

                <li><a href=\"";
        // line 85
        echo $this->env->getExtension('routing')->getPath("frontend_frontend_contact");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.menu.contact"), "html", null, true);
        echo "</a></li>

            </ul>
        </div>

    </div>

</nav>
<!-- /NAVIGATION -->";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Home:menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 85,  161 => 82,  154 => 77,  146 => 74,  135 => 72,  131 => 71,  126 => 69,  123 => 68,  118 => 67,  113 => 64,  102 => 62,  98 => 61,  93 => 59,  90 => 58,  87 => 57,  80 => 50,  71 => 44,  67 => 42,  59 => 30,  44 => 18,  40 => 17,  35 => 15,  19 => 1,);
    }
}
/* <!-- NAVIGATION -->*/
/* <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent">*/
/* */
/*     <div class="container">*/
/* */
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">*/
/*                 <span class="sr-only">Toggle navigation</span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/* */
/*             <!-- YOU LOGO HERE -->*/
/*             <a class="navbar-brand" href="{{ path( 'frontend_home_index' ) }}">*/
/*                 <!-- IMAGE OR SIMPLE TEXT -->*/
/*                 <img class="dark-logo" src="{{ asset('bundles/frontend/images/logonomon_black.png')}}" alt="" width="95">*/
/*                 <img class="light-logo" src="{{ asset('bundles/frontend/images/logonomon_white.png')}}" alt="" width="95">*/
/*             </a>*/
/*         </div>*/
/* */
/*         <div class="collapse navbar-collapse" id="custom-collapse">*/
/* */
/*             <ul class="nav navbar-nav navbar-right">*/
/*                 <li class="dropdown">*/
/*                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">NOMON WORLD</a>*/
/*                     <ul class="dropdown-menu" role="menu">*/
/* */
/*                         <li class="dropdown">*/
/*                             <a href="{{ path('frontend_page_view', { 'sSlug' : 'home-saber-hacer' }) }}">{{ 'nomon.menu.saberhacer' | trans }}</a>*/
/*                         </li>*/
/*                         */
/*                         {#*/
/*                         <li class="dropdown">*/
/*                             <a href="{{ path('frontend_distributor_list') }}">{{ 'nomon.menu.distributors' | trans }}</a>*/
/*                         </li>*/
/* */
/*                         <li class="dropdown">*/
/*                             <a href="{{ path('frontend_page_view', { 'sSlug' : 'nomon-friends' }) }}">NOMON & FRIENDS</a>*/
/*                         </li>*/
/*                         #}*/
/* */
/*                         <li class="dropdown">*/
/*                             <a href="{{ path('frontend_page_view', { 'sSlug' : 'home-customer-service' }) }}">CUSTOMER SERVICE</a>*/
/*                         </li>*/
/*                     </ul>*/
/*                 </li>*/
/* */
/*                 <li class="dropdown">*/
/*                     <a href="{{ path("frontend_homecollection_list") }}" class="dropdown-toggle" data-toggle="dropdown">{{ 'nomon.menu.collections' | trans }}</a>*/
/*                     <ul class="dropdown-menu" role="menu">*/
/*                         {# for oHomeModel in aHomeModels %}*/
/*                         <li class="dropdown">*/
/*                             <a href="{{ path('frontend_homemodel_view', { 'sHomeModel' : oHomeModel.slug }) }}" >{{ oHomeModel.name }}</a>*/
/*                         </li>*/
/*                         {% endfor #}*/
/*                         {% if aMenuNovedadesModels | length > 0 %}*/
/*                             <li class="dropdown">*/
/*                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ oMenuNovedadesCollection.name }}</a>*/
/*                                 <ul class="dropdown-menu">*/
/*                                     {% for oModel in aMenuNovedadesModels %}*/
/*                                         <li><a href="{{ path('frontend_homemodel_view',{ 'sHomeModel' : oModel.slug }) }}">{{ oModel.name }}</a></li>*/
/*                                     {% endfor %}*/
/*                                 </ul>*/
/*                             </li>*/
/*                         {% endif %}*/
/*                         {% for oCollection in aCollection %}*/
/*                             <li class="dropdown">*/
/*                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ oCollection.name }}</a>*/
/*                                 <ul class="dropdown-menu">*/
/*                                     {% for oModel in oCollection.models %}*/
/*                                         <li><a href="{{ path('frontend_homemodel_view',{ 'sHomeModel' : oModel.slug }) }}">{{ oModel.name }}</a></li>*/
/*                                     {% endfor %}*/
/*                                 </ul>*/
/*                             </li>*/
/*                         {% endfor %}*/
/* */
/*                     </ul>*/
/*                 </li>*/
/* */
/*                 <li>*/
/*                     <a href="{{ path('frontend_frontend_index') }}" style="font-weight:bold">NOMON CLOCKS</a>*/
/*                 </li>*/
/* */
/*                 <li><a href="{{ path('frontend_frontend_contact') }}">{{ 'nomon.menu.contact' | trans }}</a></li>*/
/* */
/*             </ul>*/
/*         </div>*/
/* */
/*     </div>*/
/* */
/* </nav>*/
/* <!-- /NAVIGATION -->*/
