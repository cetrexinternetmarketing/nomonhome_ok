<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\Collaborator;
use Momon\BackendBundle\Form\CollaboratorType;

/**
 * Collaborator controller.
 *
 * @Route("/collaborator")
 */
class CollaboratorController extends Controller
{

    /**
     * INDEX ACTION - Collaborator.
     *
     * @Route("/", name="backend_collaborator_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aCollaborators = $em->getRepository( 'BackendBundle:Collaborator' )->findAll();

        return array(
            'aCollaborators' => $aCollaborators,
        );
    }
    /**
     * CREATE ACTION - Collaborator.
     *
     * @Route("/create", name="backend_collaborator_create")
     * @Template()
     */
    public function createAction( Request $oRequest )
    {
        $oCollaborator = new Collaborator();
        $oForm = $this->createForm( new CollaboratorType(), $oCollaborator );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );

            if( $oForm->isValid() ){
                $em = $this->getDoctrine()->getManager();
                $em->persist( $oCollaborator );
                $em->flush();

                $this->addFlash(
                    'success',
                    'Creado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_collaborator_view', array( 'nCollaboratorId' => $oCollaborator->getId() ) ) );            }
        }

        return array(
            'oCollaborator' => $oCollaborator,
            'oForm'   => $oForm->createView(),
        );
    }
    /**
     * VIEW ACTION - Collaborator.
     *
     * @Route("/view/{nCollaboratorId}", name="backend_collaborator_view")
     * @Template()
     */
    public function viewAction( $nCollaboratorId )
    {
        $em = $this->getDoctrine()->getManager();

        $oCollaborator = $em->getRepository( 'BackendBundle:Collaborator' )->find( $nCollaboratorId );

        if ( !$oCollaborator ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Collaborator' );
        }

        $oForm = $this->createForm( new CollaboratorType(), $oCollaborator );

        return array(
            'oCollaborator' => $oCollaborator,
            'oForm' => $oForm->createView()
        );
    }

    /**
     * EDIT ACTION - Collaborator.
     *
     * @Route("/edit/{nCollaboratorId}", name="backend_collaborator_edit")
     * @Template()
     */
    public function editAction( $nCollaboratorId, Request $oRequest )
    {
        $em = $this->getDoctrine()->getManager();

        $oCollaborator = $em->getRepository( 'BackendBundle:Collaborator' )->find( $nCollaboratorId );

        if ( !$oCollaborator ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Collaborator' );
        }

        $oForm = $this->createForm( new CollaboratorType(), $oCollaborator );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );
            if( $oForm->isValid() ){
                $em->flush();

                $this->addFlash(
                'success',
                'Editado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_collaborator_view', array( 'nCollaboratorId' => $nCollaboratorId ) ) );
            }
        }

        return array(
            'oCollaborator' => $oCollaborator,
            'oForm'         => $oForm->createView(),
        );
    }
    /**
     * DELETE ACTION - Collaborator.
     *
     * @Route("/delete/{nCollaboratorId}", name="backend_collaborator_delete")
     */
    public function deleteAction( $nCollaboratorId )
    {
        $em = $this->getDoctrine()->getManager();

        $oCollaborator = $em->getRepository( 'BackendBundle:Collaborator' )->find( $nCollaboratorId );

        if ( !$oCollaborator ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Collaborator' );
        }

        $em->remove( $oCollaborator );
        $em->flush();

        $this->addFlash(
        'success',
        'Eliminado correctamente!'
        );

        return $this->redirect( $this->generateUrl( 'backend_collaborator_index' ) );
    }}
