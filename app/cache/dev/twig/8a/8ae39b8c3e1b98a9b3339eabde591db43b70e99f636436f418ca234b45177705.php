<?php

/* FrontendBundle:Distributor:list.html.twig */
class __TwigTemplate_dc0dbc47bad10e0991f40f322af97daac50453981872eb86c9b541249744975f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle:Frontend:base.html.twig", "FrontendBundle:Distributor:list.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle:Frontend:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8755f33db00f10e8ff0dd6d9b21c16aef20fb2221313c69d367d02025ba00e1 = $this->env->getExtension("native_profiler");
        $__internal_d8755f33db00f10e8ff0dd6d9b21c16aef20fb2221313c69d367d02025ba00e1->enter($__internal_d8755f33db00f10e8ff0dd6d9b21c16aef20fb2221313c69d367d02025ba00e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontendBundle:Distributor:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d8755f33db00f10e8ff0dd6d9b21c16aef20fb2221313c69d367d02025ba00e1->leave($__internal_d8755f33db00f10e8ff0dd6d9b21c16aef20fb2221313c69d367d02025ba00e1_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_7f8603b41ccbb931cd2920caf7b63ccf3da2b41ca6b4298f1a2b8a58df8d7faf = $this->env->getExtension("native_profiler");
        $__internal_7f8603b41ccbb931cd2920caf7b63ccf3da2b41ca6b4298f1a2b8a58df8d7faf->enter($__internal_7f8603b41ccbb931cd2920caf7b63ccf3da2b41ca6b4298f1a2b8a58df8d7faf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "NOMON CLOCKS | ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.title.distributors"), "html", null, true);
        
        $__internal_7f8603b41ccbb931cd2920caf7b63ccf3da2b41ca6b4298f1a2b8a58df8d7faf->leave($__internal_7f8603b41ccbb931cd2920caf7b63ccf3da2b41ca6b4298f1a2b8a58df8d7faf_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_6f736258c13fbc40bf5d62de29193c0b9ecebd0eca3f89a20792a962e2cc470c = $this->env->getExtension("native_profiler");
        $__internal_6f736258c13fbc40bf5d62de29193c0b9ecebd0eca3f89a20792a962e2cc470c->enter($__internal_6f736258c13fbc40bf5d62de29193c0b9ecebd0eca3f89a20792a962e2cc470c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "<!-- HERO -->
<section style=\"background-image: url('";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oSection"]) ? $context["oSection"] : $this->getContext($context, "oSection")), "banner", array()), "webpath", array())), "html", null, true);
        echo "');\" class=\"module module-parallax bg-dark-30\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oSection"]) ? $context["oSection"] : $this->getContext($context, "oSection")), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

    <!-- HERO TEXT -->
    <div class=\"container\">

        <div class=\"row\">
            <div class=\"col-sm-12 text-center\">
                <h1 class=\"mh-line-size-3 font-alt m-b-20\">";
        // line 12
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["oSection"]) ? $context["oSection"] : $this->getContext($context, "oSection")), "title", array())), "html", null, true);
        echo "</h1>
                <h5 class=\"mh-line-size-4 font-alt\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oSection"]) ? $context["oSection"] : $this->getContext($context, "oSection")), "slogan", array()), "html", null, true);
        echo "</h5>
            </div>
        </div>

    </div>
    <!-- /HERO TEXT -->

</section>
<!-- /HERO -->

<!-- PORTFOLIO -->
<section id=\"portfolio\" class=\"module\">

    <div class=\"container\">

        <!-- WORKS GRID -->
        <div class=\"works-grid-wrapper\">

            <div style=\"position: relative; height: 1196px;\" id=\"works-grid\" class=\"works-grid works-hover-w\">

                <!-- DO NOT DELETE THIS DIV -->
                <div class=\"grid-sizer\"></div>

                ";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aDistributors"]) ? $context["aDistributors"] : $this->getContext($context, "aDistributors")));
        foreach ($context['_seq'] as $context["_key"] => $context["oDistributor"]) {
            // line 37
            echo "                <!-- PORTFOLIO ITEM -->
                <div style=\"height: 229px; position: absolute; left: 0px; top: 0px;\" class=\"work-item\">
                    <a href=\"#.html\" style=\"cursor: default\">
                        <img src=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute($context["oDistributor"], "banner", array()), "webpath", array())), "html", null, true);
            echo " \" alt=\"\">
                        <div class=\"work-caption font-alt\">
                            <h3 class=\"work-title\">";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["oDistributor"], "city", array()), "html", null, true);
            echo "</h3>
                            <div class=\"work-descr\">
                                ";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["oDistributor"], "country", array()), "html", null, true);
            echo "
                            </div>
                        </div>
                    </a>
                </div>
                <!-- /PORTFOLIO ITEM -->
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oDistributor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "
            </div>

        </div>
        <!-- /WORKS GRID -->

    </div>

</section>
<!-- /PORTFOLIO -->
";
        
        $__internal_6f736258c13fbc40bf5d62de29193c0b9ecebd0eca3f89a20792a962e2cc470c->leave($__internal_6f736258c13fbc40bf5d62de29193c0b9ecebd0eca3f89a20792a962e2cc470c_prof);

    }

    public function getTemplateName()
    {
        return "FrontendBundle:Distributor:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 51,  118 => 44,  113 => 42,  108 => 40,  103 => 37,  99 => 36,  73 => 13,  69 => 12,  57 => 5,  54 => 4,  48 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle:Frontend:base.html.twig' %}*/
/* {% block title %}NOMON CLOCKS | {{ 'nomon.title.distributors' | trans }}{% endblock %}*/
/* {% block content %}*/
/* <!-- HERO -->*/
/* <section style="background-image: url('{{ asset(oSection.banner.webpath) }}');" class="module module-parallax bg-dark-30" data-background="{{ asset(oSection.banner.webpath) }}">*/
/* */
/*     <!-- HERO TEXT -->*/
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/*             <div class="col-sm-12 text-center">*/
/*                 <h1 class="mh-line-size-3 font-alt m-b-20">{{ oSection.title | upper }}</h1>*/
/*                 <h5 class="mh-line-size-4 font-alt">{{ oSection.slogan }}</h5>*/
/*             </div>*/
/*         </div>*/
/* */
/*     </div>*/
/*     <!-- /HERO TEXT -->*/
/* */
/* </section>*/
/* <!-- /HERO -->*/
/* */
/* <!-- PORTFOLIO -->*/
/* <section id="portfolio" class="module">*/
/* */
/*     <div class="container">*/
/* */
/*         <!-- WORKS GRID -->*/
/*         <div class="works-grid-wrapper">*/
/* */
/*             <div style="position: relative; height: 1196px;" id="works-grid" class="works-grid works-hover-w">*/
/* */
/*                 <!-- DO NOT DELETE THIS DIV -->*/
/*                 <div class="grid-sizer"></div>*/
/* */
/*                 {% for oDistributor in aDistributors %}*/
/*                 <!-- PORTFOLIO ITEM -->*/
/*                 <div style="height: 229px; position: absolute; left: 0px; top: 0px;" class="work-item">*/
/*                     <a href="#.html" style="cursor: default">*/
/*                         <img src="{{ asset( oDistributor.banner.webpath ) }} " alt="">*/
/*                         <div class="work-caption font-alt">*/
/*                             <h3 class="work-title">{{ oDistributor.city }}</h3>*/
/*                             <div class="work-descr">*/
/*                                 {{ oDistributor.country }}*/
/*                             </div>*/
/*                         </div>*/
/*                     </a>*/
/*                 </div>*/
/*                 <!-- /PORTFOLIO ITEM -->*/
/*                 {% endfor %}*/
/* */
/*             </div>*/
/* */
/*         </div>*/
/*         <!-- /WORKS GRID -->*/
/* */
/*     </div>*/
/* */
/* </section>*/
/* <!-- /PORTFOLIO -->*/
/* {% endblock %}*/
/* */
/* */
