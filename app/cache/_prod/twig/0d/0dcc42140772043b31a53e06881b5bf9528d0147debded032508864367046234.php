<?php

/* FrontendBundle:Frontend:base.html.twig */
class __TwigTemplate_7d901568074ad44edb46f6f5251b2f760014854cb550adeed1de0075883a6f00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">

        <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        <!-- Favicons -->
        <link rel=\"shortcut icon\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/favicon.ico"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/apple-touch-icon.png"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/apple-touch-icon-72x72.png"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/apple-touch-icon-114x114.png"), "html", null, true);
        echo "\">

        <!-- Bootstrap core CSS -->
        <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">


        <!-- Tipo icono -->
        <link href=\"http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/icons.css"), "html", null, true);
        echo "\">

        <!-- Plugins -->
        <!-- link href=\"content/font-awesome.css\" rel=\"stylesheet\">
        <link href=\"content/font-awesome.min.css\" rel=\"stylesheet\" -->
        <link href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/et-line-font.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/simpletextrotator.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/magnific-popup.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/owl.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/superslides.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/vertical.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/animate.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <!-- Template core CSS -->
        <link href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <style id=\"fit-vids-style\">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>
        <style>

            .color {
                float: left;
                position: relative;
                margin: 5px;
                text-align: center;
            }

            .color span {
                display: block;
                width: 50px;
                height: 50px;
                margin-bottom: 5px;
                border-radius: 180px;
            }


        </style>
        <script style=\"\" src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/common.js"), "html", null, true);
        echo "\" charset=\"UTF-8\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/util.js"), "html", null, true);
        echo "\" charset=\"UTF-8\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/stats.js"), "html", null, true);
        echo "\" charset=\"UTF-8\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/AuthenticationService.js"), "html", null, true);
        echo "\" charset=\"UTF-8\" type=\"text/javascript\"></script>

        <link href=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/fancybox/jquery.fancybox.css?v=2.1.5"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <link href=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/nomon.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/cookies.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        
        <!-- SCRIPT CONTROL DE COOKIES -->
        <script type=\"text/javascript\">
        function controlcookies() {
            
            // si variable no existe se crea (al clicar en Aceptar)
            localStorage.controlcookie = (localStorage.controlcookie || 0);
         
            localStorage.controlcookie++; // incrementamos cuenta de la cookie
            cookie1.style.display='none'; // Esconde la política de cookies
        }
        </script>
        <div class=\"cookiesms\" id=\"cookie1\" style=\"text-align: center\">
            Esta web utiliza cookies, puedes ver nuestra  <a href=\"";
        // line 81
        echo $this->env->getExtension('routing')->getPath("frontend_page_view", array("sSlug" => "cookies"));
        echo "\">la política de cookies, aquí</a> 
            Si continuas navegando estás aceptándola
            <button onclick=\"controlcookies()\">Aceptar</button>    
        </div>
        <script type=\"text/javascript\">
            if (localStorage.controlcookie>0){ 
                document.getElementById('cookie1').style.bottom = '-200px';
            }
        </script>

    </head>
    <body>
        <!-- PRELOADER -->
        <div style=\"display: none;\" class=\"page-loader\">
            <div style=\"display: none;\" class=\"loader\">Loading...</div>
        </div>
        <!-- /PRELOADER -->

        <!-- WRAPPER -->
        <div class=\"wrapper\">

            <div id=\"selector-idiomas\" style=\"position: fixed; right: 0; left: 0; z-index: 999999;\">
                <div class=\"container\">
                    <div style=\"float: right; font-size: 11px; margin-top: 5px;\">
                        <a class=\"selector-idiomas-item\" style=\"margin-right: 5px\" href=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), twig_array_merge($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route_params"), "method"), array("_locale" => "es"))), "html", null, true);
        echo "\">ES </a>
                        <span>|</span>
                        <a class=\"selector-idiomas-item\" style=\"margin-left: 5px\" href=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), twig_array_merge($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route_params"), "method"), array("_locale" => "en"))), "html", null, true);
        echo "\"> EN </a>
                    </div>
                </div>
            </div>

            ";
        // line 112
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FrontendBundle:Frontend:menu"));
        // line 114
        echo "
            ";
        // line 115
        $this->displayBlock('content', $context, $blocks);
        // line 117
        echo "            ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FrontendBundle:Frontend:footer"));
        // line 119
        echo "
        </div>

        <!-- SCROLLTOP -->
        <div style=\"display: none;\" class=\"scroll-up\">
            <a href=\"#totop\"><i class=\"fa fa-angle-double-up\"></i></a>
        </div>
                

        <!-- Javascript files -->
        <script src=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery-2.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/bootstrap.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_003.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_006.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_007.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/owl.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_002.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/imagesloaded.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/isotope.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/packery-mode.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/appear.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_005.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/wow.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jqBootstrapValidation.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_004.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/js.js"), "html", null, true);
        echo "\"></script><script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/main.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/gmaps.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 148
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/custom.js"), "html", null, true);
        echo "\"></script>

        <script src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/fancybox/jquery.fancybox.pack.js?v=2.1.5"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\">
            \$(document).ready(function() {
                \$(\".fancybox\").fancybox();
            });
        </script>
        ";
        // line 156
        $this->displayBlock('javascripts', $context, $blocks);
        // line 158
        echo "    </body>
</html>

";
    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
        echo "Relojes";
    }

    // line 115
    public function block_content($context, array $blocks = array())
    {
        // line 116
        echo "            ";
    }

    // line 156
    public function block_javascripts($context, array $blocks = array())
    {
        // line 157
        echo "        ";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Frontend:base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  346 => 157,  343 => 156,  339 => 116,  336 => 115,  330 => 10,  323 => 158,  321 => 156,  312 => 150,  306 => 148,  302 => 146,  296 => 145,  292 => 144,  288 => 143,  284 => 142,  280 => 141,  276 => 140,  272 => 139,  268 => 138,  264 => 137,  260 => 136,  256 => 135,  252 => 134,  248 => 133,  244 => 132,  240 => 131,  236 => 130,  232 => 129,  220 => 119,  217 => 117,  215 => 115,  212 => 114,  210 => 112,  202 => 107,  197 => 105,  170 => 81,  153 => 67,  149 => 66,  144 => 64,  139 => 62,  135 => 61,  131 => 60,  127 => 59,  103 => 38,  97 => 35,  93 => 34,  89 => 33,  85 => 32,  81 => 31,  77 => 30,  73 => 29,  65 => 24,  57 => 19,  51 => 16,  47 => 15,  43 => 14,  39 => 13,  33 => 10,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/*     <head>*/
/*         <meta http-equiv="content-type" content="text/html; charset=UTF-8">*/
/*         <meta charset="UTF-8">*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/*         <meta name="description" content="">*/
/*         <meta name="author" content="">*/
/* */
/*         <title>{% block title %}Relojes{% endblock %}</title>*/
/* */
/*         <!-- Favicons -->*/
/*         <link rel="shortcut icon" href="{{ asset('bundles/frontend/images/favicon.ico') }}">*/
/*         <link rel="apple-touch-icon" href="{{ asset('bundles/frontend/images/apple-touch-icon.png')  }}">*/
/*         <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('bundles/frontend/images/apple-touch-icon-72x72.png') }}">*/
/*         <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('bundles/frontend/images/apple-touch-icon-114x114.png') }}">*/
/* */
/*         <!-- Bootstrap core CSS -->*/
/*         <link href="{{ asset('bundles/frontend/content/bootstrap.css') }}" rel="stylesheet">*/
/* */
/* */
/*         <!-- Tipo icono -->*/
/*         <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">*/
/*         <link rel="stylesheet" type="text/css" href="{{ asset('bundles/frontend/icons.css') }}">*/
/* */
/*         <!-- Plugins -->*/
/*         <!-- link href="content/font-awesome.css" rel="stylesheet">*/
/*         <link href="content/font-awesome.min.css" rel="stylesheet" -->*/
/*         <link href="{{ asset('bundles/frontend/content/et-line-font.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/simpletextrotator.css' ) }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/magnific-popup.css' ) }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/owl.css' ) }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/superslides.css' ) }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/vertical.css' ) }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/animate.css' ) }}" rel="stylesheet">*/
/* */
/*         <!-- Template core CSS -->*/
/*         <link href="{{ asset('bundles/frontend/content/style.css') }}" rel="stylesheet">*/
/*         <style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>*/
/*         <style>*/
/* */
/*             .color {*/
/*                 float: left;*/
/*                 position: relative;*/
/*                 margin: 5px;*/
/*                 text-align: center;*/
/*             }*/
/* */
/*             .color span {*/
/*                 display: block;*/
/*                 width: 50px;*/
/*                 height: 50px;*/
/*                 margin-bottom: 5px;*/
/*                 border-radius: 180px;*/
/*             }*/
/* */
/* */
/*         </style>*/
/*         <script style="" src="{{ asset('bundles/frontend/content/common.js')}}" charset="UTF-8" type="text/javascript"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/util.js')}}" charset="UTF-8" type="text/javascript"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/stats.js' ) }}" charset="UTF-8" type="text/javascript"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/AuthenticationService.js' ) }}" charset="UTF-8" type="text/javascript"></script>*/
/* */
/*         <link href="{{ asset('bundles/frontend/fancybox/jquery.fancybox.css?v=2.1.5') }}" rel="stylesheet">*/
/* */
/*         <link href="{{ asset('bundles/frontend/content/nomon.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/cookies.css') }}" rel="stylesheet">*/
/*         */
/*         <!-- SCRIPT CONTROL DE COOKIES -->*/
/*         <script type="text/javascript">*/
/*         function controlcookies() {*/
/*             */
/*             // si variable no existe se crea (al clicar en Aceptar)*/
/*             localStorage.controlcookie = (localStorage.controlcookie || 0);*/
/*          */
/*             localStorage.controlcookie++; // incrementamos cuenta de la cookie*/
/*             cookie1.style.display='none'; // Esconde la política de cookies*/
/*         }*/
/*         </script>*/
/*         <div class="cookiesms" id="cookie1" style="text-align: center">*/
/*             Esta web utiliza cookies, puedes ver nuestra  <a href="{{ path('frontend_page_view', { 'sSlug' : 'cookies' }) }}">la política de cookies, aquí</a> */
/*             Si continuas navegando estás aceptándola*/
/*             <button onclick="controlcookies()">Aceptar</button>    */
/*         </div>*/
/*         <script type="text/javascript">*/
/*             if (localStorage.controlcookie>0){ */
/*                 document.getElementById('cookie1').style.bottom = '-200px';*/
/*             }*/
/*         </script>*/
/* */
/*     </head>*/
/*     <body>*/
/*         <!-- PRELOADER -->*/
/*         <div style="display: none;" class="page-loader">*/
/*             <div style="display: none;" class="loader">Loading...</div>*/
/*         </div>*/
/*         <!-- /PRELOADER -->*/
/* */
/*         <!-- WRAPPER -->*/
/*         <div class="wrapper">*/
/* */
/*             <div id="selector-idiomas" style="position: fixed; right: 0; left: 0; z-index: 999999;">*/
/*                 <div class="container">*/
/*                     <div style="float: right; font-size: 11px; margin-top: 5px;">*/
/*                         <a class="selector-idiomas-item" style="margin-right: 5px" href="{{ path(app.request.attributes.get('_route'), app.request.attributes.get('_route_params')|merge({'_locale': 'es'})) }}">ES </a>*/
/*                         <span>|</span>*/
/*                         <a class="selector-idiomas-item" style="margin-left: 5px" href="{{ path(app.request.attributes.get('_route'), app.request.attributes.get('_route_params')|merge({'_locale': 'en'})) }}"> EN </a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             {{ render(controller(*/
/*             'FrontendBundle:Frontend:menu'*/
/*             )) }}*/
/*             {% block content %}*/
/*             {% endblock %}*/
/*             {{ render(controller(*/
/*             'FrontendBundle:Frontend:footer'*/
/*             )) }}*/
/*         </div>*/
/* */
/*         <!-- SCROLLTOP -->*/
/*         <div style="display: none;" class="scroll-up">*/
/*             <a href="#totop"><i class="fa fa-angle-double-up"></i></a>*/
/*         </div>*/
/*                 */
/* */
/*         <!-- Javascript files -->*/
/*         <script src="{{ asset('bundles/frontend/content/jquery-2.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/bootstrap.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery_003.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery_006.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery_007.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/owl.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery_002.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/imagesloaded.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/isotope.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/packery-mode.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/appear.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery_005.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/wow.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jqBootstrapValidation.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery_004.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/js.js')}}"></script><script src="{{ asset('bundles/frontend/content/main.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/gmaps.js')}}"></script>*/
/*         {#<script src="{{ asset('bundles/frontend/content/contact.js')}}"></script>#}*/
/*         <script src="{{ asset('bundles/frontend/content/custom.js')}}"></script>*/
/* */
/*         <script src="{{ asset('bundles/frontend/fancybox/jquery.fancybox.pack.js?v=2.1.5')}}"></script>*/
/*         <script type="text/javascript">*/
/*             $(document).ready(function() {*/
/*                 $(".fancybox").fancybox();*/
/*             });*/
/*         </script>*/
/*         {% block javascripts %}*/
/*         {% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
/* */
