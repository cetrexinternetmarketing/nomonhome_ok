<?php

/* FrontendBundle:Home:index.html.twig */
class __TwigTemplate_04d7e1e705ed31e193030e109b6cd93aadd770fcd5cf477b2bd3a2730323eeaf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle:Home:base.html.twig", "FrontendBundle:Home:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle:Home:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.title.homeindex"), "html", null, true);
        echo " ";
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
    <!-- SLIDER -->
    ";
        // line 6
        if ( !(null === $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "url", array()))) {
            // line 7
            echo "        ";
            echo $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "url", array());
            echo "
    ";
        }
        // line 9
        echo "    <section style=\"background-image: url('";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "'); background-position: 50% 0px; height: 951px;\" id=\"hero\" class=\"module-hero module-parallax module-full-height bg-film\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

        <!-- HERO TEXT -->
        <div class=\"hero-caption\">
            <div class=\"hero-text\">
                <h5 class=\"mh-line-size-4 font-alt m-b-50\">";
        // line 14
        echo $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "title1", array());
        echo "</h5>
                <h1 class=\"mh-line-size-1 font-alt m-b-60\">";
        // line 15
        echo $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "title2", array());
        echo "</h1>
                <h5 class=\"mh-line-size-5 font-alt m-b-50\">";
        // line 16
        echo $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "title3", array());
        echo "</h5>
            </div>
        </div>
        <!-- /HERO TEXT -->

    </section>
    ";
        // line 22
        if ( !(null === $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "url", array()))) {
            // line 23
            echo "        </a>
    ";
        }
        // line 25
        echo "    <!-- /SLIDER -->

    <!-- ABOUT -->
    <section class=\"module\">

        <div class=\"container\">

            <!-- MODULE TITLE -->
            <div class=\"row\">
                <div class=\"col-sm-6 col-sm-offset-3\">
                    <h2 class=\"module-title font-alt\">NOMON HOME</h2>
                    <div class=\"module-subtitle font-serif\">
                        <p>";
        // line 37
        echo $this->env->getExtension('translator')->trans("nomon.nomonhome.text");
        echo "</p>
                    </div>
                </div>
            </div>

            <hr class=\"divider\"><!-- DIVIDER -->
            <section class=\"module\">
                <!-- MODULE TITLE -->
                <div class=\"row\">
                    <div class=\"col-sm-6 col-sm-offset-3\">
                        <h2 class=\"module-title font-alt\">";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.nomonhome.collections"), "html", null, true);
        echo "</h2>
                    </div>
                </div>
                <!-- /MODULE TITLE -->

                <div class=\"row multi-columns-row\">

                    ";
        // line 72
        echo "
                    ";
        // line 73
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aCollections"]) ? $context["aCollections"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oCollection"]) {
            // line 74
            echo "                    <!-- POST -->
                    <div class=\"col-sm-6 col-md-4 col-lg-4 m-b-60\">
                        <div class=\"post\">
                            <div class=\"post-media\">
                                <img src=\"";
            // line 78
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute($context["oCollection"], "image", array()), "webpath", array())), "html", null, true);
            echo "\" alt=\"\">
                            </div>
                            <div class=\"post-header\">
                                <h5 class=\"post-title font-alt\">
                                    ";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCollection"], "name", array()), "html", null, true);
            echo "
                                </h5>
                            </div>
                            <div class=\"post-meta font-alt\">
                                ";
            // line 86
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["oCollection"], "models", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["oModel"]) {
                // line 87
                echo "                                    <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_homemodel_view", array("sHomeModel" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["oModel"], "name", array()), "html", null, true);
                echo "</a> ";
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo "/";
                }
                // line 88
                echo "                                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oModel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "                            </div>
                        </div>
                    </div>
                    <!-- /POST -->
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oCollection'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "                    ";
        if ( !(null === (isset($context["oNovedadesCollection"]) ? $context["oNovedadesCollection"] : null))) {
            // line 95
            echo "                        <div class=\"col-sm-6 col-md-4 col-lg-4 m-b-60\">
                            <div class=\"post\">
                                <div class=\"post-media\">
                                    <img src=\"";
            // line 98
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oNovedadesCollection"]) ? $context["oNovedadesCollection"] : null), "image", array()), "webpath", array())), "html", null, true);
            echo "\" alt=\"\">
                                </div>
                                <div class=\"post-header\">
                                    <h5 class=\"post-title font-alt\">
                                        ";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oNovedadesCollection"]) ? $context["oNovedadesCollection"] : null), "name", array()), "html", null, true);
            echo "
                                    </h5>
                                </div>
                                <div class=\"post-meta font-alt\">
                                    ";
            // line 106
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["oNovedadesCollectionModels"]) ? $context["oNovedadesCollectionModels"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["oModel"]) {
                // line 107
                echo "                                        <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_homemodel_view", array("sHomeModel" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["oModel"], "name", array()), "html", null, true);
                echo "</a> ";
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo "/";
                }
                // line 108
                echo "                                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oModel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 109
            echo "                                </div>
                            </div>
                        </div>
                    ";
        }
        // line 113
        echo "
                </div>
            </section>
            <!-- BLOG 3 COLUMN -->
        </div>

    </section>
    <!-- /ABOUT -->

    <hr class=\"divider\"><!-- DIVIDER -->
    <!-- HERO -->
    <a href=\"";
        // line 124
        echo $this->env->getExtension('routing')->getPath("frontend_frontend_index");
        echo "\">
        <section style=\"background-image: url('";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oClocksParallax"]) ? $context["oClocksParallax"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "'); height: 350px;\" class=\"module module-parallax bg-dark-30\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oClocksParallax"]) ? $context["oClocksParallax"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

            <!-- HERO TEXT -->
            <div class=\"container\" style=\"margin-top: 7%;\">

                <div class=\"row\">
                    <div class=\"col-sm-12 text-center\">
                        <h1 class=\"mh-line-size-3 font-alt m-b-20\">";
        // line 132
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oClocksParallax"]) ? $context["oClocksParallax"] : null), "title", array()), "html", null, true);
        echo "</h1>
                        <h5 class=\"mh-line-size-4 font-alt\">";
        // line 133
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oClocksParallax"]) ? $context["oClocksParallax"] : null), "slogan", array()), "html", null, true);
        echo "</h5>
                    </div>
                </div>

            </div>
            <!-- /HERO TEXT -->

        </section>
    </a>
    <!-- /HERO -->
";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Home:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  301 => 133,  297 => 132,  285 => 125,  281 => 124,  268 => 113,  262 => 109,  248 => 108,  239 => 107,  222 => 106,  215 => 102,  208 => 98,  203 => 95,  200 => 94,  190 => 89,  176 => 88,  167 => 87,  150 => 86,  143 => 82,  136 => 78,  130 => 74,  126 => 73,  123 => 72,  113 => 47,  100 => 37,  86 => 25,  82 => 23,  80 => 22,  71 => 16,  67 => 15,  63 => 14,  52 => 9,  46 => 7,  44 => 6,  40 => 4,  37 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle:Home:base.html.twig' %}*/
/* {% block title %} {{ 'nomon.title.homeindex' | trans }} {% endblock %}*/
/* {% block content %}*/
/* */
/*     <!-- SLIDER -->*/
/*     {% if oBanner.url is not null %}*/
/*         {{ oBanner.url | raw }}*/
/*     {% endif %}*/
/*     <section style="background-image: url('{{ asset( oBanner.banner.webpath) }}'); background-position: 50% 0px; height: 951px;" id="hero" class="module-hero module-parallax module-full-height bg-film" data-background="{{ asset( oBanner.banner.webpath) }}">*/
/* */
/*         <!-- HERO TEXT -->*/
/*         <div class="hero-caption">*/
/*             <div class="hero-text">*/
/*                 <h5 class="mh-line-size-4 font-alt m-b-50">{{ oBanner.title1 | raw }}</h5>*/
/*                 <h1 class="mh-line-size-1 font-alt m-b-60">{{ oBanner.title2 | raw }}</h1>*/
/*                 <h5 class="mh-line-size-5 font-alt m-b-50">{{ oBanner.title3 | raw }}</h5>*/
/*             </div>*/
/*         </div>*/
/*         <!-- /HERO TEXT -->*/
/* */
/*     </section>*/
/*     {% if oBanner.url is not null %}*/
/*         </a>*/
/*     {% endif %}*/
/*     <!-- /SLIDER -->*/
/* */
/*     <!-- ABOUT -->*/
/*     <section class="module">*/
/* */
/*         <div class="container">*/
/* */
/*             <!-- MODULE TITLE -->*/
/*             <div class="row">*/
/*                 <div class="col-sm-6 col-sm-offset-3">*/
/*                     <h2 class="module-title font-alt">NOMON HOME</h2>*/
/*                     <div class="module-subtitle font-serif">*/
/*                         <p>{{ 'nomon.nomonhome.text' | trans | raw }}</p>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <hr class="divider"><!-- DIVIDER -->*/
/*             <section class="module">*/
/*                 <!-- MODULE TITLE -->*/
/*                 <div class="row">*/
/*                     <div class="col-sm-6 col-sm-offset-3">*/
/*                         <h2 class="module-title font-alt">{{ 'nomon.nomonhome.collections' | trans }}</h2>*/
/*                     </div>*/
/*                 </div>*/
/*                 <!-- /MODULE TITLE -->*/
/* */
/*                 <div class="row multi-columns-row">*/
/* */
/*                     {# for oHomeModel in aHomeModels %}*/
/*                     <!-- POST -->*/
/*                     <div class="col-sm-6 col-md-4 col-lg-4 m-b-60">*/
/*                         <div class="post">*/
/*                             <div class="post-media">*/
/*                                 <a href="{{ path('frontend_homemodel_view', { 'sHomeModel' : oHomeModel.slug } ) }}">*/
/*                                     <img src="{{ asset( oHomeModel.banner.webpath ) }}" alt="">*/
/*                                 </a>*/
/*                             </div>*/
/*                             <div class="post-header">*/
/*                                 <h5 class="post-title font-alt">*/
/*                                     <a href="{{ path('frontend_homemodel_view', { 'sHomeModel' : oHomeModel.slug } ) }}">{{ oHomeModel.name }}</a>*/
/*                                 </h5>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <!-- /POST -->*/
/*                     {% endfor #}*/
/* */
/*                     {% for oCollection in aCollections %}*/
/*                     <!-- POST -->*/
/*                     <div class="col-sm-6 col-md-4 col-lg-4 m-b-60">*/
/*                         <div class="post">*/
/*                             <div class="post-media">*/
/*                                 <img src="{{ asset( oCollection.image.webpath )}}" alt="">*/
/*                             </div>*/
/*                             <div class="post-header">*/
/*                                 <h5 class="post-title font-alt">*/
/*                                     {{ oCollection.name }}*/
/*                                 </h5>*/
/*                             </div>*/
/*                             <div class="post-meta font-alt">*/
/*                                 {% for oModel in oCollection.models %}*/
/*                                     <a href="{{ path('frontend_homemodel_view', { 'sHomeModel' : oModel.slug }) }}">{{ oModel.name  }}</a> {% if not loop.last %}/{% endif %}*/
/*                                 {% endfor %}*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <!-- /POST -->*/
/*                     {% endfor %}*/
/*                     {% if oNovedadesCollection is not null%}*/
/*                         <div class="col-sm-6 col-md-4 col-lg-4 m-b-60">*/
/*                             <div class="post">*/
/*                                 <div class="post-media">*/
/*                                     <img src="{{ asset( oNovedadesCollection.image.webpath )}}" alt="">*/
/*                                 </div>*/
/*                                 <div class="post-header">*/
/*                                     <h5 class="post-title font-alt">*/
/*                                         {{ oNovedadesCollection.name }}*/
/*                                     </h5>*/
/*                                 </div>*/
/*                                 <div class="post-meta font-alt">*/
/*                                     {% for oModel in oNovedadesCollectionModels %}*/
/*                                         <a href="{{ path('frontend_homemodel_view', { 'sHomeModel' : oModel.slug }) }}">{{ oModel.name  }}</a> {% if not loop.last %}/{% endif %}*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% endif %}*/
/* */
/*                 </div>*/
/*             </section>*/
/*             <!-- BLOG 3 COLUMN -->*/
/*         </div>*/
/* */
/*     </section>*/
/*     <!-- /ABOUT -->*/
/* */
/*     <hr class="divider"><!-- DIVIDER -->*/
/*     <!-- HERO -->*/
/*     <a href="{{ path('frontend_frontend_index') }}">*/
/*         <section style="background-image: url('{{ asset(oClocksParallax.banner.webpath) }}'); height: 350px;" class="module module-parallax bg-dark-30" data-background="{{ asset(oClocksParallax.banner.webpath) }}">*/
/* */
/*             <!-- HERO TEXT -->*/
/*             <div class="container" style="margin-top: 7%;">*/
/* */
/*                 <div class="row">*/
/*                     <div class="col-sm-12 text-center">*/
/*                         <h1 class="mh-line-size-3 font-alt m-b-20">{{ oClocksParallax.title }}</h1>*/
/*                         <h5 class="mh-line-size-4 font-alt">{{ oClocksParallax.slogan }}</h5>*/
/*                     </div>*/
/*                 </div>*/
/* */
/*             </div>*/
/*             <!-- /HERO TEXT -->*/
/* */
/*         </section>*/
/*     </a>*/
/*     <!-- /HERO -->*/
/* {% endblock %}*/
/* */
