<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\Page;
use Momon\BackendBundle\Form\PageType;

/**
 * Page controller.
 *
 * @Route("/page")
 */
class PageController extends Controller
{

    /**
     * INDEX ACTION - Page.
     *
     * @Route("/", name="backend_page_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aPages = $em->getRepository( 'BackendBundle:Page' )->findAll();

        return array(
            'aPages' => $aPages,
        );
    }
    /**
     * CREATE ACTION - Page.
     *
     * @Route("/create", name="backend_page_create")
     * @Template()
     */
    public function createAction( Request $oRequest )
    {
        $oPage = new Page();
        $oForm = $this->createForm( new PageType(), $oPage );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );

            if( $oForm->isValid() ){
                $em = $this->getDoctrine()->getManager();
                $em->persist( $oPage );
                $em->flush();

                $this->addFlash(
                    'success',
                    'Creado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_page_view', array( 'nPageId' => $oPage->getId() ) ) );            }
        }

        return array(
            'oPage' => $oPage,
            'oForm'   => $oForm->createView(),
        );
    }
    /**
     * VIEW ACTION - Page.
     *
     * @Route("/view/{nPageId}", name="backend_page_view")
     * @Template()
     */
    public function viewAction( $nPageId )
    {
        $em = $this->getDoctrine()->getManager();

        $oPage = $em->getRepository( 'BackendBundle:Page' )->find( $nPageId );

        if ( !$oPage ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Page' );
        }

        $oForm = $this->createForm( new PageType(), $oPage );

        return array(
            'oPage' => $oPage,
            'oForm' => $oForm->createView()
        );
    }

    /**
     * EDIT ACTION - Page.
     *
     * @Route("/edit/{nPageId}", name="backend_page_edit")
     * @Template()
     */
    public function editAction( $nPageId, Request $oRequest )
    {
        $em = $this->getDoctrine()->getManager();

        $oPage = $em->getRepository( 'BackendBundle:Page' )->find( $nPageId );

        if ( !$oPage ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Page' );
        }

        $oForm = $this->createForm( new PageType(), $oPage );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );
            if( $oForm->isValid() ){
                $em->flush();

                $this->addFlash(
                'success',
                'Editado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_page_view', array( 'nPageId' => $nPageId ) ) );
            }
        }

        return array(
            'oPage' => $oPage,
            'oForm'         => $oForm->createView(),
        );
    }
    /**
     * DELETE ACTION - Page.
     *
     * @Route("/delete/{nPageId}", name="backend_page_delete")
     */
    public function deleteAction( $nPageId )
    {
        $em = $this->getDoctrine()->getManager();

        $oPage = $em->getRepository( 'BackendBundle:Page' )->find( $nPageId );

        if ( !$oPage ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Page' );
        }

        $em->remove( $oPage );
        $em->flush();

        $this->addFlash(
        'success',
        'Eliminado correctamente!'
        );

        return $this->redirect( $this->generateUrl( 'backend_page_index' ) );
    }}
