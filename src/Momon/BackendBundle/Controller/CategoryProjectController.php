<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\CategoryProject;
use Momon\BackendBundle\Form\CategoryProjectType;

/**
 * CategoryProject controller.
 *
 * @Route("/categoryproject")
 */
class CategoryProjectController extends Controller
{

    /**
     * INDEX ACTION - CategoryProject.
     *
     * @Route("/", name="backend_categoryproject_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aCategoryProjects = $em->getRepository( 'BackendBundle:CategoryProject' )->findAll();

        return array(
            'aCategoryProjects' => $aCategoryProjects,
        );
    }
    /**
     * CREATE ACTION - CategoryProject.
     *
     * @Route("/create", name="backend_categoryproject_create")
     * @Template()
     */
    public function createAction( Request $oRequest )
    {
        $oCategoryProject = new CategoryProject();
        $oForm = $this->createForm( new CategoryProjectType(), $oCategoryProject );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );

            if( $oForm->isValid() ){
                $em = $this->getDoctrine()->getManager();
                $em->persist( $oCategoryProject );
                $em->flush();

                $this->addFlash(
                    'success',
                    'Creado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_categoryproject_view', array( 'nCategoryProjectId' => $oCategoryProject->getId() ) ) );            }
        }

        return array(
            'oCategoryProject' => $oCategoryProject,
            'oForm'   => $oForm->createView(),
        );
    }
    /**
     * VIEW ACTION - CategoryProject.
     *
     * @Route("/view/{nCategoryProjectId}", name="backend_categoryproject_view")
     * @Template()
     */
    public function viewAction( $nCategoryProjectId )
    {
        $em = $this->getDoctrine()->getManager();

        $oCategoryProject = $em->getRepository( 'BackendBundle:CategoryProject' )->find( $nCategoryProjectId );

        if ( !$oCategoryProject ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la CategoryProject' );
        }

        return array(
            'oCategoryProject' => $oCategoryProject,
        );
    }

    /**
     * EDIT ACTION - CategoryProject.
     *
     * @Route("/edit/{nCategoryProjectId}", name="backend_categoryproject_edit")
     * @Template()
     */
    public function editAction( $nCategoryProjectId, Request $oRequest )
    {
        $em = $this->getDoctrine()->getManager();

        $oCategoryProject = $em->getRepository( 'BackendBundle:CategoryProject' )->find( $nCategoryProjectId );

        if ( !$oCategoryProject ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la CategoryProject' );
        }

        $oForm = $this->createForm( new CategoryProjectType(), $oCategoryProject );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );
            if( $oForm->isValid() ){
                $em->flush();

                $this->addFlash(
                'success',
                'Editado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_categoryproject_view', array( 'nCategoryProjectId' => $nCategoryProjectId ) ) );
            }
        }

        return array(
            'oCategoryProject' => $oCategoryProject,
            'oForm'         => $oForm->createView(),
        );
    }
    /**
     * DELETE ACTION - CategoryProject.
     *
     * @Route("/delete/{nCategoryProjectId}", name="backend_categoryproject_delete")
     */
    public function deleteAction( $nCategoryProjectId )
    {
        $em = $this->getDoctrine()->getManager();

        $oCategoryProject = $em->getRepository( 'BackendBundle:CategoryProject' )->find( $nCategoryProjectId );

        if ( !$oCategoryProject ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la CategoryProject' );
        }

        $em->remove( $oCategoryProject );
        $em->flush();

        $this->addFlash(
        'success',
        'Eliminado correctamente!'
        );

        return $this->redirect( $this->generateUrl( 'backend_categoryproject_index' ) );
    }}
