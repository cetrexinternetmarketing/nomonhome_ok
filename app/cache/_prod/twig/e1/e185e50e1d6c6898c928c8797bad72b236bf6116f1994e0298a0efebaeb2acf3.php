<?php

/* FrontendBundle:Page:view.html.twig */
class __TwigTemplate_9ddc6ec3a657704082272558f3b1ec0595b3a0892282c58c5348a284a7ed131b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle:Frontend:base.html.twig", "FrontendBundle:Page:view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle:Frontend:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "NOMON CLOCKS | ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "title", array()), "html", null, true);
        echo " ";
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<!-- HERO -->
<section style=\"background-image: url( ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo " );\" class=\"module module-parallax bg-dark-30\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

    <!-- HERO TEXT -->
    <div class=\"container\">

        <div class=\"row\">
            <div class=\"col-sm-12 text-center\">
                <h1 class=\"mh-line-size-3 font-alt m-b-20\">NOMON CULTURE</h1>
                <h5 class=\"mh-line-size-4 font-alt\">";
        // line 13
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "title", array())), "html", null, true);
        echo "</h5>
            </div>
        </div>

    </div>
    <!-- /HERO TEXT -->

</section>
<!-- /HERO -->

<!-- ABOUT -->
<section class=\"module\">

    <div class=\"container\">

        <div class=\"row\">

            ";
        // line 30
        if ( !(null === $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "col2", array()))) {
            // line 31
            echo "                <!-- ABOUT  -->
                <div class=\"col-sm-";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "col1size", array()), "html", null, true);
            echo " text-justify\">
                    ";
            // line 33
            echo $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "col1", array());
            echo "
                </div>
                <!-- /ABOUT  -->

                <!-- HECHO A MANO -->
                <div class=\"col-sm-";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "col2size", array()), "html", null, true);
            echo " text-justify\">
                    ";
            // line 39
            echo $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "col2", array());
            echo "
                </div>
                <!-- /HECHO A MANO -->
            ";
        } else {
            // line 43
            echo "                <!-- DESIGNER -->
                <div class=\"col-sm-12 text-justify\">
                    ";
            // line 45
            echo $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "col1", array());
            echo "
                </div>
                <!-- /DESIGNER -->
            ";
        }
        // line 49
        echo "
        </div>

    </div>

</section>

<!-- /ABOUT -->
";
        // line 57
        if ($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "isVideo", array())) {
            // line 58
            echo "    ";
            if ( !(null === $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "videoParallax", array()))) {
                // line 59
                echo "        <!-- COUNTERS -->
        <section style=\"height: 400px;\" class=\"module module-video bg-dark\" data-background=\"";
                // line 60
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "imageParallax", array()), "webpath", array()), "html", null, true);
                echo "\">

            <div style=\"position: absolute; z-index: 0; min-width: 100%; min-height: 100%; left: 0px; top: 0px; overflow: hidden; opacity: 1; transition-property: opacity; transition-duration: 2000ms;\" id=\"wrapper_mbYTP_video_1441722771889\" class=\"mbYTP_wrapper\">
                <iframe width=\"560\" height=\"315\" src=\"";
                // line 63
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "videoParallax", array()), "html", null, true);
                echo "\" frameborder=\"0\" allowfullscreen autoPlay:true></iframe>
                ";
                // line 65
                echo "            </div>


            <!-- YOUTUBE VIDEO-->
            <div style=\"display: yes; position: relative; background-image: none;\" id=\"video_1441722771889\" class=\"video-player mb_YTPlayer isMuted\" data-property=\"{videoURL:'";
                // line 69
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "videoParallax", array()), "html", null, true);
                echo "', containment:'.module-video', quality:'large', startAt:0, autoPlay:true, loop:true, opacity:1, showControls:false, showYTLogo:false, vol:0, mute:true}\">
            </div>
            <!-- /YOUTUBE VIDEO-->

        </section>
        <!-- /COUNTERS -->
    ";
            }
        } else {
            // line 77
            echo "    ";
            if ( !(null === $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "imageParallax", array()))) {
                // line 78
                echo "        ";
                if ( !(null === $this->getAttribute($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "imageParallax", array()), "webpath", array()))) {
                    // line 79
                    echo "            <!-- COUNTERS -->
            <section style=\"height: 400px; background-size: 100%; background-position: center;\" class=\"module module-video bg-dark\" data-background=\"";
                    // line 80
                    echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : null), "imageParallax", array()), "webpath", array())), "html", null, true);
                    echo "\">

            </section>
        ";
                }
                // line 84
                echo "    ";
            }
        }
        // line 86
        echo "
";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Page:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 86,  176 => 84,  169 => 80,  166 => 79,  163 => 78,  160 => 77,  149 => 69,  143 => 65,  139 => 63,  133 => 60,  130 => 59,  127 => 58,  125 => 57,  115 => 49,  108 => 45,  104 => 43,  97 => 39,  93 => 38,  85 => 33,  81 => 32,  78 => 31,  76 => 30,  56 => 13,  43 => 5,  40 => 4,  37 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle:Frontend:base.html.twig' %}*/
/* {% block title %}NOMON CLOCKS | {{ oPage.title }} {% endblock %}*/
/* {% block content %}*/
/* <!-- HERO -->*/
/* <section style="background-image: url( {{ asset( oPage.banner.webpath ) }} );" class="module module-parallax bg-dark-30" data-background="{{ asset( oPage.banner.webpath ) }}">*/
/* */
/*     <!-- HERO TEXT -->*/
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/*             <div class="col-sm-12 text-center">*/
/*                 <h1 class="mh-line-size-3 font-alt m-b-20">NOMON CULTURE</h1>*/
/*                 <h5 class="mh-line-size-4 font-alt">{{ oPage.title | upper }}</h5>*/
/*             </div>*/
/*         </div>*/
/* */
/*     </div>*/
/*     <!-- /HERO TEXT -->*/
/* */
/* </section>*/
/* <!-- /HERO -->*/
/* */
/* <!-- ABOUT -->*/
/* <section class="module">*/
/* */
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/* */
/*             {% if oPage.col2 is not null %}*/
/*                 <!-- ABOUT  -->*/
/*                 <div class="col-sm-{{ oPage.col1size }} text-justify">*/
/*                     {{ oPage.col1 | raw }}*/
/*                 </div>*/
/*                 <!-- /ABOUT  -->*/
/* */
/*                 <!-- HECHO A MANO -->*/
/*                 <div class="col-sm-{{ oPage.col2size }} text-justify">*/
/*                     {{ oPage.col2 | raw }}*/
/*                 </div>*/
/*                 <!-- /HECHO A MANO -->*/
/*             {% else %}*/
/*                 <!-- DESIGNER -->*/
/*                 <div class="col-sm-12 text-justify">*/
/*                     {{ oPage.col1 | raw }}*/
/*                 </div>*/
/*                 <!-- /DESIGNER -->*/
/*             {% endif %}*/
/* */
/*         </div>*/
/* */
/*     </div>*/
/* */
/* </section>*/
/* */
/* <!-- /ABOUT -->*/
/* {% if oPage.isVideo %}*/
/*     {% if oPage.videoParallax is not null %}*/
/*         <!-- COUNTERS -->*/
/*         <section style="height: 400px;" class="module module-video bg-dark" data-background="{{ oPage.imageParallax.webpath }}">*/
/* */
/*             <div style="position: absolute; z-index: 0; min-width: 100%; min-height: 100%; left: 0px; top: 0px; overflow: hidden; opacity: 1; transition-property: opacity; transition-duration: 2000ms;" id="wrapper_mbYTP_video_1441722771889" class="mbYTP_wrapper">*/
/*                 <iframe width="560" height="315" src="{{ oPage.videoParallax }}" frameborder="0" allowfullscreen autoPlay:true></iframe>*/
/*                 {#<div class="YTPOverlay" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%;"></div>#}*/
/*             </div>*/
/* */
/* */
/*             <!-- YOUTUBE VIDEO-->*/
/*             <div style="display: yes; position: relative; background-image: none;" id="video_1441722771889" class="video-player mb_YTPlayer isMuted" data-property="{videoURL:'{{ oPage.videoParallax }}', containment:'.module-video', quality:'large', startAt:0, autoPlay:true, loop:true, opacity:1, showControls:false, showYTLogo:false, vol:0, mute:true}">*/
/*             </div>*/
/*             <!-- /YOUTUBE VIDEO-->*/
/* */
/*         </section>*/
/*         <!-- /COUNTERS -->*/
/*     {% endif %}*/
/* {% else %}*/
/*     {% if oPage.imageParallax is not null %}*/
/*         {% if oPage.imageParallax.webpath is not null %}*/
/*             <!-- COUNTERS -->*/
/*             <section style="height: 400px; background-size: 100%; background-position: center;" class="module module-video bg-dark" data-background="{{ asset(oPage.imageParallax.webpath) }}">*/
/* */
/*             </section>*/
/*         {% endif %}*/
/*     {% endif %}*/
/* {% endif %}*/
/* */
/* {% endblock %}*/
