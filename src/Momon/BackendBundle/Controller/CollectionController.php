<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\Collection;
use Momon\BackendBundle\Form\CollectionType;

/**
 * Collection controller.
 *
 * @Route("/collection")
 */
class CollectionController extends Controller
{

    /**
     * INDEX ACTION - Collection.
     *
     * @Route("/", name="backend_collection_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aCollections = $em->getRepository( 'BackendBundle:Collection' )->findAll();

        return array(
            'aCollections' => $aCollections,
        );
    }
    /**
     * CREATE ACTION - Collection.
     *
     * @Route("/create", name="backend_collection_create")
     * @Template()
     */
    public function createAction( Request $oRequest )
    {
        $oCollection = new Collection();
        $oForm = $this->createForm( new CollectionType(), $oCollection );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );

            if( $oForm->isValid() ){
                $em = $this->getDoctrine()->getManager();
                $em->persist( $oCollection );
                $em->flush();

                $this->addFlash(
                    'success',
                    'Creado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_collection_view', array( 'nCollectionId' => $oCollection->getId() ) ) );            }
        }

        return array(
            'oCollection' => $oCollection,
            'oForm'   => $oForm->createView(),
        );
    }
    /**
     * VIEW ACTION - Collection.
     *
     * @Route("/view/{nCollectionId}", name="backend_collection_view")
     * @Template()
     */
    public function viewAction( $nCollectionId )
    {
        $em = $this->getDoctrine()->getManager();

        $oCollection = $em->getRepository( 'BackendBundle:Collection' )->find( $nCollectionId );

        if ( !$oCollection ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Collection' );
        }

        return array(
            'oCollection' => $oCollection,
        );
    }

    /**
     * EDIT ACTION - Collection.
     *
     * @Route("/edit/{nCollectionId}", name="backend_collection_edit")
     * @Template()
     */
    public function editAction( $nCollectionId, Request $oRequest )
    {
        $em = $this->getDoctrine()->getManager();

        $oCollection = $em->getRepository( 'BackendBundle:Collection' )->find( $nCollectionId );

        if ( !$oCollection ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Collection' );
        }

        $oForm = $this->createForm( new CollectionType(), $oCollection );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );
            if( $oForm->isValid() ){
                $em->flush();

                $this->addFlash(
                'success',
                'Editado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_collection_view', array( 'nCollectionId' => $nCollectionId ) ) );
            }
        }

        return array(
            'oCollection' => $oCollection,
            'oForm'         => $oForm->createView(),
        );
    }
    /**
     * DELETE ACTION - Collection.
     *
     * @Route("/delete/{nCollectionId}", name="backend_collection_delete")
     */
    public function deleteAction( $nCollectionId )
    {
        $em = $this->getDoctrine()->getManager();

        $oCollection = $em->getRepository( 'BackendBundle:Collection' )->find( $nCollectionId );

        if ( !$oCollection ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Collection' );
        }

        $em->remove( $oCollection );
        $em->flush();

        $this->addFlash(
        'success',
        'Eliminado correctamente!'
        );

        return $this->redirect( $this->generateUrl( 'backend_collection_index' ) );
    }}
