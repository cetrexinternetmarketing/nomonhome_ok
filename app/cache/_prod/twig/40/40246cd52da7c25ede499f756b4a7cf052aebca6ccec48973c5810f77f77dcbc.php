<?php

/* FrontendBundle:IndexSection:home_index.html.twig */
class __TwigTemplate_3a616a29c231cf693433e80699f4a87a497037652eebef420ed9df14c9ab4364 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- NOMON HOME -->
<a href=\"";
        // line 2
        echo $this->env->getExtension('routing')->getPath("frontend_home_index");
        echo "\">
    <section style=\"background-image: url('";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oHomeParallax"]) ? $context["oHomeParallax"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "'); height: 350px;\" class=\"module module-parallax bg-dark-30\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oHomeParallax"]) ? $context["oHomeParallax"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

        <!-- HERO TEXT -->
        <div class=\"container\" style=\"margin-top: 7%;\">

            <div class=\"row\">
                <div class=\"col-sm-12 text-center\">
                    <h1 class=\"mh-line-size-3 font-alt m-b-20\">";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oHomeParallax"]) ? $context["oHomeParallax"] : null), "title", array()), "html", null, true);
        echo "</h1>
                    <h5 class=\"mh-line-size-4 font-alt\">";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oHomeParallax"]) ? $context["oHomeParallax"] : null), "slogan", array()), "html", null, true);
        echo "</h5>
                </div>
            </div>

        </div>
        <!-- /HERO TEXT -->

    </section>
</a>
<!-- /NOMON HOME -->";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:IndexSection:home_index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 11,  38 => 10,  26 => 3,  22 => 2,  19 => 1,);
    }
}
/* <!-- NOMON HOME -->*/
/* <a href="{{ path('frontend_home_index') }}">*/
/*     <section style="background-image: url('{{ asset(oHomeParallax.banner.webpath) }}'); height: 350px;" class="module module-parallax bg-dark-30" data-background="{{ asset(oHomeParallax.banner.webpath) }}">*/
/* */
/*         <!-- HERO TEXT -->*/
/*         <div class="container" style="margin-top: 7%;">*/
/* */
/*             <div class="row">*/
/*                 <div class="col-sm-12 text-center">*/
/*                     <h1 class="mh-line-size-3 font-alt m-b-20">{{ oHomeParallax.title }}</h1>*/
/*                     <h5 class="mh-line-size-4 font-alt">{{ oHomeParallax.slogan }}</h5>*/
/*                 </div>*/
/*             </div>*/
/* */
/*         </div>*/
/*         <!-- /HERO TEXT -->*/
/* */
/*     </section>*/
/* </a>*/
/* <!-- /NOMON HOME -->*/
