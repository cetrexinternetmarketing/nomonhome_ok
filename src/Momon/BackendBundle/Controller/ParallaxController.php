<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\Parallax;
use Momon\BackendBundle\Form\ParallaxType;

/**
 * Parallax controller.
 *
 * @Route("/parallax")
 */
class ParallaxController extends Controller
{

    /**
     * INDEX ACTION - Parallax.
     *
     * @Route("/", name="backend_parallax_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aParallaxs = $em->getRepository( 'BackendBundle:Parallax' )->findAll();

        return array(
            'aParallaxs' => $aParallaxs,
        );
    }
    /**
     * CREATE ACTION - Parallax.
     *
     * @Route("/create", name="backend_parallax_create")
     * @Template()
     */
    public function createAction( Request $oRequest )
    {
        $oParallax = new Parallax();
        $oForm = $this->createForm( new ParallaxType(), $oParallax );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );

            if( $oForm->isValid() ){
                $em = $this->getDoctrine()->getManager();
                $em->persist( $oParallax );
                $em->flush();

                $this->addFlash(
                    'success',
                    'Creado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_parallax_view', array( 'nParallaxId' => $oParallax->getId() ) ) );            }
        }

        return array(
            'oParallax' => $oParallax,
            'oForm'   => $oForm->createView(),
        );
    }
    /**
     * VIEW ACTION - Parallax.
     *
     * @Route("/view/{nParallaxId}", name="backend_parallax_view")
     * @Template()
     */
    public function viewAction( $nParallaxId )
    {
        $em = $this->getDoctrine()->getManager();

        $oParallax = $em->getRepository( 'BackendBundle:Parallax' )->find( $nParallaxId );

        if ( !$oParallax ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Parallax' );
        }

        $oForm = $this->createForm( new ParallaxType(), $oParallax );

        return array(
            'oParallax' => $oParallax,
            'oForm' => $oForm->createView()
        );
    }

    /**
     * EDIT ACTION - Parallax.
     *
     * @Route("/edit/{nParallaxId}", name="backend_parallax_edit")
     * @Template()
     */
    public function editAction( $nParallaxId, Request $oRequest )
    {
        $em = $this->getDoctrine()->getManager();

        $oParallax = $em->getRepository( 'BackendBundle:Parallax' )->find( $nParallaxId );

        if ( !$oParallax ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Parallax' );
        }

        $oForm = $this->createForm( new ParallaxType(), $oParallax );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );
            if( $oForm->isValid() ){
                $em->flush();

                $this->addFlash(
                'success',
                'Editado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_parallax_view', array( 'nParallaxId' => $nParallaxId ) ) );
            }
        }

        return array(
            'oParallax' => $oParallax,
            'oForm'         => $oForm->createView(),
        );
    }
    /**
     * DELETE ACTION - Parallax.
     *
     * @Route("/delete/{nParallaxId}", name="backend_parallax_delete")
     */
    public function deleteAction( $nParallaxId )
    {
        $em = $this->getDoctrine()->getManager();

        $oParallax = $em->getRepository( 'BackendBundle:Parallax' )->find( $nParallaxId );

        if ( !$oParallax ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Parallax' );
        }

        $em->remove( $oParallax );
        $em->flush();

        $this->addFlash(
        'success',
        'Eliminado correctamente!'
        );

        return $this->redirect( $this->generateUrl( 'backend_parallax_index' ) );
    }}
