<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\HomeModel;
use Momon\BackendBundle\Form\HomeModelType;

/**
 * HomeModel controller.
 *
 * @Route("/homemodel")
 */
class HomeModelController extends Controller
{

    /**
     * INDEX ACTION - HomeModel.
     *
     * @Route("/", name="backend_homemodel_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aHomeModels = $em->getRepository( 'BackendBundle:HomeModel' )->findAll();

        return array(
            'aHomeModels' => $aHomeModels,
        );
    }
    /**
     * CREATE ACTION - HomeModel.
     *
     * @Route("/create", name="backend_homemodel_create")
     * @Template()
     */
    public function createAction( Request $oRequest )
    {
        $oHomeModel = new HomeModel();
        $oForm = $this->createForm( new HomeModelType(), $oHomeModel );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );

            if( $oForm->isValid() ){
                $em = $this->getDoctrine()->getManager();
                $em->persist( $oHomeModel );
                $em->flush();

                $this->addFlash(
                    'success',
                    'Creado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_homemodel_view', array( 'nHomeModelId' => $oHomeModel->getId() ) ) );            }
        }

        return array(
            'oHomeModel' => $oHomeModel,
            'oForm'   => $oForm->createView(),
        );
    }
    /**
     * VIEW ACTION - HomeModel.
     *
     * @Route("/view/{nHomeModelId}", name="backend_homemodel_view")
     * @Template()
     */
    public function viewAction( $nHomeModelId )
    {
        $em = $this->getDoctrine()->getManager();

        $oHomeModel = $em->getRepository( 'BackendBundle:HomeModel' )->find( $nHomeModelId );

        if ( !$oHomeModel ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la HomeModel' );
        }

        return array(
            'oHomeModel' => $oHomeModel,
        );
    }

    /**
     * EDIT ACTION - HomeModel.
     *
     * @Route("/edit/{nHomeModelId}", name="backend_homemodel_edit")
     * @Template()
     */
    public function editAction( $nHomeModelId, Request $oRequest )
    {
        $em = $this->getDoctrine()->getManager();

        $oHomeModel = $em->getRepository( 'BackendBundle:HomeModel' )->find( $nHomeModelId );

        if ( !$oHomeModel ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la HomeModel' );
        }

        $oForm = $this->createForm( new HomeModelType(), $oHomeModel );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );
            if( $oForm->isValid() ){
                $em->flush();

                $this->addFlash(
                'success',
                'Editado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_homemodel_view', array( 'nHomeModelId' => $nHomeModelId ) ) );
            }
        }

        return array(
            'oHomeModel' => $oHomeModel,
            'oForm'         => $oForm->createView(),
        );
    }
    /**
     * DELETE ACTION - HomeModel.
     *
     * @Route("/delete/{nHomeModelId}", name="backend_homemodel_delete")
     */
    public function deleteAction( $nHomeModelId )
    {
        $em = $this->getDoctrine()->getManager();

        $oHomeModel = $em->getRepository( 'BackendBundle:HomeModel' )->find( $nHomeModelId );

        if ( !$oHomeModel ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la HomeModel' );
        }

        $em->remove( $oHomeModel );
        $em->flush();

        $this->addFlash(
        'success',
        'Eliminado correctamente!'
        );

        return $this->redirect( $this->generateUrl( 'backend_homemodel_index' ) );
    }}
