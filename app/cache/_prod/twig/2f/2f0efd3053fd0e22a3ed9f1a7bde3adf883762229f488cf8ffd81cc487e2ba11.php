<?php

/* FrontendBundle:Model:otherCollection.html.twig */
class __TwigTemplate_1e2c078881ab506e7a654521673ba465020a57cbe2e9fbee4f20b079040509e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!-- LATEST PRODUCTS -->
<section class=\"module-small\">
    <div class=\"container\">

        <!-- MODULE TITLE -->
        <div class=\"row\">
            <div class=\"col-sm-6 col-sm-offset-3\">
                <h2 class=\"module-title font-alt\">";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.other"), "html", null, true);
        echo "</h2>
            </div>
        </div>
        <!-- /MODULE TITLE -->

        <div class=\"row multi-columns-row\">
            ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aModels"]) ? $context["aModels"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oModel"]) {
            // line 16
            echo "                <!-- SHOP ITEM -->
                <div class=\"col-sm-6 col-md-3 col-lg-3\">
                    <div class=\"shop-item\">
                        <div class=\"shop-item-image\">
                            <img src=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute($context["oModel"], "banner", array()), "webpath", array())), "html", null, true);
            echo "\" alt=\"\">
                            <div class=\"shop-item-detail\">
                                <a class=\"btn btn-round btn-b\" href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_model_view", array("sModelSlug" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.view"), "html", null, true);
            echo "</a>
                            </div>
                        </div>
                        <h5 class=\"shop-item-title font-alt\"><a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_model_view", array("sModelSlug" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["oModel"], "name", array()), "html", null, true);
            echo "</a></h5>
                    </div>
                </div>
                <!-- /SHOP ITEM -->
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oModel'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "        </div>

    </div>

</section>
<!-- /LATEST PRODUCTS -->";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Model:otherCollection.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 30,  61 => 25,  53 => 22,  48 => 20,  42 => 16,  38 => 15,  29 => 9,  19 => 1,);
    }
}
/* */
/* <!-- LATEST PRODUCTS -->*/
/* <section class="module-small">*/
/*     <div class="container">*/
/* */
/*         <!-- MODULE TITLE -->*/
/*         <div class="row">*/
/*             <div class="col-sm-6 col-sm-offset-3">*/
/*                 <h2 class="module-title font-alt">{{ 'nomon.model.other' | trans }}</h2>*/
/*             </div>*/
/*         </div>*/
/*         <!-- /MODULE TITLE -->*/
/* */
/*         <div class="row multi-columns-row">*/
/*             {% for oModel in aModels %}*/
/*                 <!-- SHOP ITEM -->*/
/*                 <div class="col-sm-6 col-md-3 col-lg-3">*/
/*                     <div class="shop-item">*/
/*                         <div class="shop-item-image">*/
/*                             <img src="{{ asset( oModel.banner.webpath ) }}" alt="">*/
/*                             <div class="shop-item-detail">*/
/*                                 <a class="btn btn-round btn-b" href="{{ path('frontend_model_view',{'sModelSlug' : oModel.slug}) }}">{{ 'nomon.model.view' | trans }}</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         <h5 class="shop-item-title font-alt"><a href="{{ path('frontend_model_view',{'sModelSlug' : oModel.slug}) }}">{{ oModel.name }}</a></h5>*/
/*                     </div>*/
/*                 </div>*/
/*                 <!-- /SHOP ITEM -->*/
/*             {% endfor %}*/
/*         </div>*/
/* */
/*     </div>*/
/* */
/* </section>*/
/* <!-- /LATEST PRODUCTS -->*/
