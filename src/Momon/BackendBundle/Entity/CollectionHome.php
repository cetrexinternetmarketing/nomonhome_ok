<?php

namespace Momon\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Netik\MediaBundle\Model\FolderInterface;
use Netik\MediaBundle\Model\GalleryInterface;
use Netik\MediaBundle\Model\GalleryModel;
use Netik\MediaBundle\Model\Nueva;
use Netik\MediaBundle\Model\UploadFileModel;
use Symfony\Component\HttpFoundation\File\File;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Campo
 *
 * @ORM\Table(name="collection_home")
 * @ORM\Entity(repositoryClass="Momon\BackendBundle\Repository\CollectionHomeRepository")
 */
class CollectionHome
{
    use ORMBehaviors\Translatable\Translatable;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;

    /**
     * @ORM\OneToOne(targetEntity="Netik\MediaBundle\Entity\SimpleFile", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="image", referencedColumnName="id",onDelete="SET NULL")
     **/
    private $image;

	/**
	 * @ORM\OneToMany(targetEntity="HomeModel", mappedBy="collection")
     * @ORM\OrderBy({"numOrder" = "ASC"})
	 **/
	private $models;

    /**
     * @var string
     *
     * @ORM\Column(name="numorder", type="integer", nullable=true)
     */
    private $numOrder;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->models = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Collection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set image
     *
     * @param \Netik\MediaBundle\Entity\SimpleFile $image
     *
     * @return Collection
     */
    public function setImage(\Netik\MediaBundle\Entity\SimpleFile $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Netik\MediaBundle\Entity\SimpleFile
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add model
     *
     * @param \Momon\BackendBundle\Entity\Model $model
     *
     * @return Collection
     */
    public function addModel(\Momon\BackendBundle\Entity\HomeModel $model)
    {
        $this->models[] = $model;

        return $this;
    }

    /**
     * Remove model
     *
     * @param \Momon\BackendBundle\Entity\Model $model
     */
    public function removeModel(\Momon\BackendBundle\Entity\HomeModel $model)
    {
        $this->models->removeElement($model);
    }

    /**
     * Get models
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModels()
    {
        return $this->models;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Set numOrder
     *
     * @param integer $numOrder
     *
     * @return Collection
     */
    public function setNumOrder($numOrder)
    {
        $this->numOrder = $numOrder;

        return $this;
    }

    /**
     * Get numOrder
     *
     * @return integer
     */
    public function getNumOrder()
    {
        return $this->numOrder;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Collection
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function __call($method, $arguments)
    {
        if($method != 'folder' and $method != 'gallery') {
            return $this->proxyCurrentLocaleTranslation('get' . $method, $arguments);
        }
    }
}
