<?php

/* FrontendBundle:Page:view.html.twig */
class __TwigTemplate_a136160a97ffac81507142997d121fdf74eeb9306ec2a7175d7487ed7ded40d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle:Frontend:base.html.twig", "FrontendBundle:Page:view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle:Frontend:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3f621042fd0899e32eeb694ae8276dcc8a04297fcf1c854fd1027ecf90f75fd4 = $this->env->getExtension("native_profiler");
        $__internal_3f621042fd0899e32eeb694ae8276dcc8a04297fcf1c854fd1027ecf90f75fd4->enter($__internal_3f621042fd0899e32eeb694ae8276dcc8a04297fcf1c854fd1027ecf90f75fd4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontendBundle:Page:view.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3f621042fd0899e32eeb694ae8276dcc8a04297fcf1c854fd1027ecf90f75fd4->leave($__internal_3f621042fd0899e32eeb694ae8276dcc8a04297fcf1c854fd1027ecf90f75fd4_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_ec5a35d2f805e7f0f38e0b2b273a10947ada03f1f940358e1c0b17691c29653a = $this->env->getExtension("native_profiler");
        $__internal_ec5a35d2f805e7f0f38e0b2b273a10947ada03f1f940358e1c0b17691c29653a->enter($__internal_ec5a35d2f805e7f0f38e0b2b273a10947ada03f1f940358e1c0b17691c29653a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "NOMON CLOCKS | ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "title", array()), "html", null, true);
        echo " ";
        
        $__internal_ec5a35d2f805e7f0f38e0b2b273a10947ada03f1f940358e1c0b17691c29653a->leave($__internal_ec5a35d2f805e7f0f38e0b2b273a10947ada03f1f940358e1c0b17691c29653a_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_3bd6f8fcb466d9676769dd53f8696967982dd7011affd9afd52ef54d32ed27fb = $this->env->getExtension("native_profiler");
        $__internal_3bd6f8fcb466d9676769dd53f8696967982dd7011affd9afd52ef54d32ed27fb->enter($__internal_3bd6f8fcb466d9676769dd53f8696967982dd7011affd9afd52ef54d32ed27fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "<!-- HERO -->
<section style=\"background-image: url( ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "banner", array()), "webpath", array())), "html", null, true);
        echo " );\" class=\"module module-parallax bg-dark-30\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

    <!-- HERO TEXT -->
    <div class=\"container\">

        <div class=\"row\">
            <div class=\"col-sm-12 text-center\">
                <h1 class=\"mh-line-size-3 font-alt m-b-20\">NOMON CULTURE</h1>
                <h5 class=\"mh-line-size-4 font-alt\">";
        // line 13
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "title", array())), "html", null, true);
        echo "</h5>
            </div>
        </div>

    </div>
    <!-- /HERO TEXT -->

</section>
<!-- /HERO -->

<!-- ABOUT -->
<section class=\"module\">

    <div class=\"container\">

        <div class=\"row\">

            <div class=\"module-subtitle font-serif\">
                <p>";
        // line 31
        echo $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "slogan", array()));
        echo "</p>
            </div>

            <hr class=\"divider\">
            
            ";
        // line 36
        if ( !(null === $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "col2", array()))) {
            // line 37
            echo "                <!-- ABOUT  -->
                <div class=\"col-sm-";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "col1size", array()), "html", null, true);
            echo " text-justify\">
                    ";
            // line 39
            echo $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "col1", array());
            echo "
                </div>
                <!-- /ABOUT  -->

                <!-- HECHO A MANO -->
                <div class=\"col-sm-";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "col2size", array()), "html", null, true);
            echo " text-justify\">
                    ";
            // line 45
            echo $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "col2", array());
            echo "
                </div>
                <!-- /HECHO A MANO -->
            ";
        } else {
            // line 49
            echo "                <!-- DESIGNER -->
                <div class=\"col-sm-12 text-justify\">
                    ";
            // line 51
            echo $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "col1", array());
            echo "
                </div>
                <!-- /DESIGNER -->
            ";
        }
        // line 55
        echo "
        </div>

    </div>

</section>

<!-- /ABOUT -->
";
        // line 63
        if ($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "isVideo", array())) {
            // line 64
            echo "    ";
            if ( !(null === $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "videoParallax", array()))) {
                // line 65
                echo "        <!-- COUNTERS -->
        <section style=\"height: 400px;\" class=\"module module-video bg-dark\" data-background=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "imageParallax", array()), "webpath", array()), "html", null, true);
                echo "\">

            <div style=\"position: absolute; z-index: 0; min-width: 100%; min-height: 100%; left: 0px; top: 0px; overflow: hidden; opacity: 1; transition-property: opacity; transition-duration: 2000ms;\" id=\"wrapper_mbYTP_video_1441722771889\" class=\"mbYTP_wrapper\">
                <iframe width=\"560\" height=\"315\" src=\"";
                // line 69
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "videoParallax", array()), "html", null, true);
                echo "\" frameborder=\"0\" allowfullscreen autoPlay:true></iframe>
                ";
                // line 71
                echo "            </div>


            <!-- YOUTUBE VIDEO-->
            <div style=\"display: yes; position: relative; background-image: none;\" id=\"video_1441722771889\" class=\"video-player mb_YTPlayer isMuted\" data-property=\"{videoURL:'";
                // line 75
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "videoParallax", array()), "html", null, true);
                echo "', containment:'.module-video', quality:'large', startAt:0, autoPlay:true, loop:true, opacity:1, showControls:false, showYTLogo:false, vol:0, mute:true}\">
            </div>
            <!-- /YOUTUBE VIDEO-->

        </section>
        <!-- /COUNTERS -->
    ";
            }
        } else {
            // line 83
            echo "    ";
            if ( !(null === $this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "imageParallax", array()))) {
                // line 84
                echo "        ";
                if ( !(null === $this->getAttribute($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "imageParallax", array()), "webpath", array()))) {
                    // line 85
                    echo "            <!-- COUNTERS -->
            <section style=\"height: 400px; background-size: 100%; background-position: center;\" class=\"module module-video bg-dark\" data-background=\"";
                    // line 86
                    echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oPage"]) ? $context["oPage"] : $this->getContext($context, "oPage")), "imageParallax", array()), "webpath", array())), "html", null, true);
                    echo "\">

            </section>
        ";
                }
                // line 90
                echo "    ";
            }
        }
        // line 92
        echo "
";
        
        $__internal_3bd6f8fcb466d9676769dd53f8696967982dd7011affd9afd52ef54d32ed27fb->leave($__internal_3bd6f8fcb466d9676769dd53f8696967982dd7011affd9afd52ef54d32ed27fb_prof);

    }

    public function getTemplateName()
    {
        return "FrontendBundle:Page:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 92,  200 => 90,  193 => 86,  190 => 85,  187 => 84,  184 => 83,  173 => 75,  167 => 71,  163 => 69,  157 => 66,  154 => 65,  151 => 64,  149 => 63,  139 => 55,  132 => 51,  128 => 49,  121 => 45,  117 => 44,  109 => 39,  105 => 38,  102 => 37,  100 => 36,  92 => 31,  71 => 13,  58 => 5,  55 => 4,  49 => 3,  35 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle:Frontend:base.html.twig' %}*/
/* {% block title %}NOMON CLOCKS | {{ oPage.title }} {% endblock %}*/
/* {% block content %}*/
/* <!-- HERO -->*/
/* <section style="background-image: url( {{ asset( oPage.banner.webpath ) }} );" class="module module-parallax bg-dark-30" data-background="{{ asset( oPage.banner.webpath ) }}">*/
/* */
/*     <!-- HERO TEXT -->*/
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/*             <div class="col-sm-12 text-center">*/
/*                 <h1 class="mh-line-size-3 font-alt m-b-20">NOMON CULTURE</h1>*/
/*                 <h5 class="mh-line-size-4 font-alt">{{ oPage.title | upper }}</h5>*/
/*             </div>*/
/*         </div>*/
/* */
/*     </div>*/
/*     <!-- /HERO TEXT -->*/
/* */
/* </section>*/
/* <!-- /HERO -->*/
/* */
/* <!-- ABOUT -->*/
/* <section class="module">*/
/* */
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/* */
/*             <div class="module-subtitle font-serif">*/
/*                 <p>{{ oPage.slogan | trans | raw }}</p>*/
/*             </div>*/
/* */
/*             <hr class="divider">*/
/*             */
/*             {% if oPage.col2 is not null %}*/
/*                 <!-- ABOUT  -->*/
/*                 <div class="col-sm-{{ oPage.col1size }} text-justify">*/
/*                     {{ oPage.col1 | raw }}*/
/*                 </div>*/
/*                 <!-- /ABOUT  -->*/
/* */
/*                 <!-- HECHO A MANO -->*/
/*                 <div class="col-sm-{{ oPage.col2size }} text-justify">*/
/*                     {{ oPage.col2 | raw }}*/
/*                 </div>*/
/*                 <!-- /HECHO A MANO -->*/
/*             {% else %}*/
/*                 <!-- DESIGNER -->*/
/*                 <div class="col-sm-12 text-justify">*/
/*                     {{ oPage.col1 | raw }}*/
/*                 </div>*/
/*                 <!-- /DESIGNER -->*/
/*             {% endif %}*/
/* */
/*         </div>*/
/* */
/*     </div>*/
/* */
/* </section>*/
/* */
/* <!-- /ABOUT -->*/
/* {% if oPage.isVideo %}*/
/*     {% if oPage.videoParallax is not null %}*/
/*         <!-- COUNTERS -->*/
/*         <section style="height: 400px;" class="module module-video bg-dark" data-background="{{ oPage.imageParallax.webpath }}">*/
/* */
/*             <div style="position: absolute; z-index: 0; min-width: 100%; min-height: 100%; left: 0px; top: 0px; overflow: hidden; opacity: 1; transition-property: opacity; transition-duration: 2000ms;" id="wrapper_mbYTP_video_1441722771889" class="mbYTP_wrapper">*/
/*                 <iframe width="560" height="315" src="{{ oPage.videoParallax }}" frameborder="0" allowfullscreen autoPlay:true></iframe>*/
/*                 {#<div class="YTPOverlay" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%;"></div>#}*/
/*             </div>*/
/* */
/* */
/*             <!-- YOUTUBE VIDEO-->*/
/*             <div style="display: yes; position: relative; background-image: none;" id="video_1441722771889" class="video-player mb_YTPlayer isMuted" data-property="{videoURL:'{{ oPage.videoParallax }}', containment:'.module-video', quality:'large', startAt:0, autoPlay:true, loop:true, opacity:1, showControls:false, showYTLogo:false, vol:0, mute:true}">*/
/*             </div>*/
/*             <!-- /YOUTUBE VIDEO-->*/
/* */
/*         </section>*/
/*         <!-- /COUNTERS -->*/
/*     {% endif %}*/
/* {% else %}*/
/*     {% if oPage.imageParallax is not null %}*/
/*         {% if oPage.imageParallax.webpath is not null %}*/
/*             <!-- COUNTERS -->*/
/*             <section style="height: 400px; background-size: 100%; background-position: center;" class="module module-video bg-dark" data-background="{{ asset(oPage.imageParallax.webpath) }}">*/
/* */
/*             </section>*/
/*         {% endif %}*/
/*     {% endif %}*/
/* {% endif %}*/
/* */
/* {% endblock %}*/
