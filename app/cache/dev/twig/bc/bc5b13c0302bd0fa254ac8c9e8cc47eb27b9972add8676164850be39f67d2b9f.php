<?php

/* FrontendBundle:Frontend:base.html.twig */
class __TwigTemplate_96f9b7ebe757cbd0c0ea5ad027d0c327e8f03e8f478a0b7f262d04f5992766aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_32c6d0e68112569fca6810dc4f1ee787131cc14bd0afb9fc4f80bf4fbd939902 = $this->env->getExtension("native_profiler");
        $__internal_32c6d0e68112569fca6810dc4f1ee787131cc14bd0afb9fc4f80bf4fbd939902->enter($__internal_32c6d0e68112569fca6810dc4f1ee787131cc14bd0afb9fc4f80bf4fbd939902_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontendBundle:Frontend:base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">

        <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        <!-- Favicons -->
        <link rel=\"shortcut icon\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/favicon.ico"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/apple-touch-icon.png"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/apple-touch-icon-72x72.png"), "html", null, true);
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/apple-touch-icon-114x114.png"), "html", null, true);
        echo "\">

        <!-- Bootstrap core CSS -->
        <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">


        <!-- Tipo icono -->
        <link href=\"http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/icons.css"), "html", null, true);
        echo "\">

        <!-- Plugins -->
        <!-- link href=\"content/font-awesome.css\" rel=\"stylesheet\">
        <link href=\"content/font-awesome.min.css\" rel=\"stylesheet\" -->
        <link href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/et-line-font.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/simpletextrotator.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/magnific-popup.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/owl.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/superslides.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/vertical.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/animate.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <!-- Template core CSS -->
        <link href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <style id=\"fit-vids-style\">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>
        <style>

            .color {
                float: left;
                position: relative;
                margin: 5px;
                text-align: center;
            }

            .color span {
                display: block;
                width: 50px;
                height: 50px;
                margin-bottom: 5px;
                border-radius: 180px;
            }


        </style>
        <script style=\"\" src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/common.js"), "html", null, true);
        echo "\" charset=\"UTF-8\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/util.js"), "html", null, true);
        echo "\" charset=\"UTF-8\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/stats.js"), "html", null, true);
        echo "\" charset=\"UTF-8\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/AuthenticationService.js"), "html", null, true);
        echo "\" charset=\"UTF-8\" type=\"text/javascript\"></script>

        <link href=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/fancybox/jquery.fancybox.css?v=2.1.5"), "html", null, true);
        echo "\" rel=\"stylesheet\">

        <link href=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/nomon.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/cookies.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        
        <!-- SCRIPT CONTROL DE COOKIES -->
        <script type=\"text/javascript\">
        function controlcookies() {
            
            // si variable no existe se crea (al clicar en Aceptar)
            localStorage.controlcookie = (localStorage.controlcookie || 0);
         
            localStorage.controlcookie++; // incrementamos cuenta de la cookie
            cookie1.style.display='none'; // Esconde la política de cookies
        }
        </script>
        <div class=\"cookiesms\" id=\"cookie1\" style=\"text-align: center\">
            Esta web utiliza cookies, puedes ver nuestra  <a href=\"";
        // line 81
        echo $this->env->getExtension('routing')->getPath("frontend_page_view", array("sSlug" => "cookies"));
        echo "\">la política de cookies, aquí</a> 
            Si continuas navegando estás aceptándola
            <button onclick=\"controlcookies()\">Aceptar</button>    
        </div>
        <script type=\"text/javascript\">
            if (localStorage.controlcookie>0){ 
                document.getElementById('cookie1').style.bottom = '-200px';
            }
        </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-90563832-6', 'auto');
  ga('send', 'pageview');

</script>
    </head>
    <body>
        <!-- PRELOADER -->
        <div style=\"display: none;\" class=\"page-loader\">
            <div style=\"display: none;\" class=\"loader\">Loading...</div>
        </div>
        <!-- /PRELOADER -->

        <!-- WRAPPER -->
        <div class=\"wrapper\">

            <div id=\"selector-idiomas\" style=\"position: fixed; right: 0; left: 0; z-index: 999999;\">
                <div class=\"container\">
                    <div style=\"float: right; font-size: 11px; margin-top: 5px;\">
                        <a class=\"selector-idiomas-item\" style=\"margin-right: 5px\" href=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), twig_array_merge($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route_params"), "method"), array("_locale" => "es"))), "html", null, true);
        echo "\">ES </a>
                        <span>|</span>
                        <a class=\"selector-idiomas-item\" style=\"margin-left: 5px\" href=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), twig_array_merge($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route_params"), "method"), array("_locale" => "en"))), "html", null, true);
        echo "\"> EN </a>
                    </div>
                </div>
            </div>

            ";
        // line 121
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FrontendBundle:Frontend:menu"));
        // line 123
        echo "
            ";
        // line 124
        $this->displayBlock('content', $context, $blocks);
        // line 126
        echo "            ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FrontendBundle:Frontend:footer"));
        // line 128
        echo "
        </div>

        <!-- SCROLLTOP -->
        <div style=\"display: none;\" class=\"scroll-up\">
            <a href=\"#totop\"><i class=\"fa fa-angle-double-up\"></i></a>
        </div>
                

        <!-- Javascript files -->
        <script src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery-2.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/bootstrap.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_003.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_006.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_007.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/owl.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_002.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/imagesloaded.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/isotope.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/packery-mode.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/appear.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_005.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/wow.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jqBootstrapValidation.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/jquery_004.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/js.js"), "html", null, true);
        echo "\"></script><script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/main.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/gmaps.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 157
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/content/custom.js"), "html", null, true);
        echo "\"></script>

        <script src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/fancybox/jquery.fancybox.pack.js?v=2.1.5"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\">
            \$(document).ready(function() {
                \$(\".fancybox\").fancybox();
            });
        </script>
        ";
        // line 165
        $this->displayBlock('javascripts', $context, $blocks);
        // line 167
        echo "    </body>
</html>

";
        
        $__internal_32c6d0e68112569fca6810dc4f1ee787131cc14bd0afb9fc4f80bf4fbd939902->leave($__internal_32c6d0e68112569fca6810dc4f1ee787131cc14bd0afb9fc4f80bf4fbd939902_prof);

    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
        $__internal_f245ba7ded31cab8b141261d7e871b26408423db51353f96de769a761e21987c = $this->env->getExtension("native_profiler");
        $__internal_f245ba7ded31cab8b141261d7e871b26408423db51353f96de769a761e21987c->enter($__internal_f245ba7ded31cab8b141261d7e871b26408423db51353f96de769a761e21987c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Relojes";
        
        $__internal_f245ba7ded31cab8b141261d7e871b26408423db51353f96de769a761e21987c->leave($__internal_f245ba7ded31cab8b141261d7e871b26408423db51353f96de769a761e21987c_prof);

    }

    // line 124
    public function block_content($context, array $blocks = array())
    {
        $__internal_79ee7c37d5d1365534d18a74f6ccb91539ea52159c7ca3f96b128a622893e7bf = $this->env->getExtension("native_profiler");
        $__internal_79ee7c37d5d1365534d18a74f6ccb91539ea52159c7ca3f96b128a622893e7bf->enter($__internal_79ee7c37d5d1365534d18a74f6ccb91539ea52159c7ca3f96b128a622893e7bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 125
        echo "            ";
        
        $__internal_79ee7c37d5d1365534d18a74f6ccb91539ea52159c7ca3f96b128a622893e7bf->leave($__internal_79ee7c37d5d1365534d18a74f6ccb91539ea52159c7ca3f96b128a622893e7bf_prof);

    }

    // line 165
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_6c5250c53db10f56e5a8ab1f167de3e87d6de18534246e49a0e98dea1ddb7990 = $this->env->getExtension("native_profiler");
        $__internal_6c5250c53db10f56e5a8ab1f167de3e87d6de18534246e49a0e98dea1ddb7990->enter($__internal_6c5250c53db10f56e5a8ab1f167de3e87d6de18534246e49a0e98dea1ddb7990_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 166
        echo "        ";
        
        $__internal_6c5250c53db10f56e5a8ab1f167de3e87d6de18534246e49a0e98dea1ddb7990->leave($__internal_6c5250c53db10f56e5a8ab1f167de3e87d6de18534246e49a0e98dea1ddb7990_prof);

    }

    public function getTemplateName()
    {
        return "FrontendBundle:Frontend:base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  376 => 166,  370 => 165,  363 => 125,  357 => 124,  345 => 10,  335 => 167,  333 => 165,  324 => 159,  318 => 157,  314 => 155,  308 => 154,  304 => 153,  300 => 152,  296 => 151,  292 => 150,  288 => 149,  284 => 148,  280 => 147,  276 => 146,  272 => 145,  268 => 144,  264 => 143,  260 => 142,  256 => 141,  252 => 140,  248 => 139,  244 => 138,  232 => 128,  229 => 126,  227 => 124,  224 => 123,  222 => 121,  214 => 116,  209 => 114,  173 => 81,  156 => 67,  152 => 66,  147 => 64,  142 => 62,  138 => 61,  134 => 60,  130 => 59,  106 => 38,  100 => 35,  96 => 34,  92 => 33,  88 => 32,  84 => 31,  80 => 30,  76 => 29,  68 => 24,  60 => 19,  54 => 16,  50 => 15,  46 => 14,  42 => 13,  36 => 10,  25 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/*     <head>*/
/*         <meta http-equiv="content-type" content="text/html; charset=UTF-8">*/
/*         <meta charset="UTF-8">*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/*         <meta name="description" content="">*/
/*         <meta name="author" content="">*/
/* */
/*         <title>{% block title %}Relojes{% endblock %}</title>*/
/* */
/*         <!-- Favicons -->*/
/*         <link rel="shortcut icon" href="{{ asset('bundles/frontend/images/favicon.ico') }}">*/
/*         <link rel="apple-touch-icon" href="{{ asset('bundles/frontend/images/apple-touch-icon.png')  }}">*/
/*         <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('bundles/frontend/images/apple-touch-icon-72x72.png') }}">*/
/*         <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('bundles/frontend/images/apple-touch-icon-114x114.png') }}">*/
/* */
/*         <!-- Bootstrap core CSS -->*/
/*         <link href="{{ asset('bundles/frontend/content/bootstrap.css') }}" rel="stylesheet">*/
/* */
/* */
/*         <!-- Tipo icono -->*/
/*         <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">*/
/*         <link rel="stylesheet" type="text/css" href="{{ asset('bundles/frontend/icons.css') }}">*/
/* */
/*         <!-- Plugins -->*/
/*         <!-- link href="content/font-awesome.css" rel="stylesheet">*/
/*         <link href="content/font-awesome.min.css" rel="stylesheet" -->*/
/*         <link href="{{ asset('bundles/frontend/content/et-line-font.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/simpletextrotator.css' ) }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/magnific-popup.css' ) }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/owl.css' ) }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/superslides.css' ) }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/vertical.css' ) }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/animate.css' ) }}" rel="stylesheet">*/
/* */
/*         <!-- Template core CSS -->*/
/*         <link href="{{ asset('bundles/frontend/content/style.css') }}" rel="stylesheet">*/
/*         <style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>*/
/*         <style>*/
/* */
/*             .color {*/
/*                 float: left;*/
/*                 position: relative;*/
/*                 margin: 5px;*/
/*                 text-align: center;*/
/*             }*/
/* */
/*             .color span {*/
/*                 display: block;*/
/*                 width: 50px;*/
/*                 height: 50px;*/
/*                 margin-bottom: 5px;*/
/*                 border-radius: 180px;*/
/*             }*/
/* */
/* */
/*         </style>*/
/*         <script style="" src="{{ asset('bundles/frontend/content/common.js')}}" charset="UTF-8" type="text/javascript"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/util.js')}}" charset="UTF-8" type="text/javascript"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/stats.js' ) }}" charset="UTF-8" type="text/javascript"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/AuthenticationService.js' ) }}" charset="UTF-8" type="text/javascript"></script>*/
/* */
/*         <link href="{{ asset('bundles/frontend/fancybox/jquery.fancybox.css?v=2.1.5') }}" rel="stylesheet">*/
/* */
/*         <link href="{{ asset('bundles/frontend/content/nomon.css') }}" rel="stylesheet">*/
/*         <link href="{{ asset('bundles/frontend/content/cookies.css') }}" rel="stylesheet">*/
/*         */
/*         <!-- SCRIPT CONTROL DE COOKIES -->*/
/*         <script type="text/javascript">*/
/*         function controlcookies() {*/
/*             */
/*             // si variable no existe se crea (al clicar en Aceptar)*/
/*             localStorage.controlcookie = (localStorage.controlcookie || 0);*/
/*          */
/*             localStorage.controlcookie++; // incrementamos cuenta de la cookie*/
/*             cookie1.style.display='none'; // Esconde la política de cookies*/
/*         }*/
/*         </script>*/
/*         <div class="cookiesms" id="cookie1" style="text-align: center">*/
/*             Esta web utiliza cookies, puedes ver nuestra  <a href="{{ path('frontend_page_view', { 'sSlug' : 'cookies' }) }}">la política de cookies, aquí</a> */
/*             Si continuas navegando estás aceptándola*/
/*             <button onclick="controlcookies()">Aceptar</button>    */
/*         </div>*/
/*         <script type="text/javascript">*/
/*             if (localStorage.controlcookie>0){ */
/*                 document.getElementById('cookie1').style.bottom = '-200px';*/
/*             }*/
/*         </script>*/
/* <script>*/
/*   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){*/
/*   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),*/
/*   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)*/
/*   })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');*/
/* */
/*   ga('create', 'UA-90563832-6', 'auto');*/
/*   ga('send', 'pageview');*/
/* */
/* </script>*/
/*     </head>*/
/*     <body>*/
/*         <!-- PRELOADER -->*/
/*         <div style="display: none;" class="page-loader">*/
/*             <div style="display: none;" class="loader">Loading...</div>*/
/*         </div>*/
/*         <!-- /PRELOADER -->*/
/* */
/*         <!-- WRAPPER -->*/
/*         <div class="wrapper">*/
/* */
/*             <div id="selector-idiomas" style="position: fixed; right: 0; left: 0; z-index: 999999;">*/
/*                 <div class="container">*/
/*                     <div style="float: right; font-size: 11px; margin-top: 5px;">*/
/*                         <a class="selector-idiomas-item" style="margin-right: 5px" href="{{ path(app.request.attributes.get('_route'), app.request.attributes.get('_route_params')|merge({'_locale': 'es'})) }}">ES </a>*/
/*                         <span>|</span>*/
/*                         <a class="selector-idiomas-item" style="margin-left: 5px" href="{{ path(app.request.attributes.get('_route'), app.request.attributes.get('_route_params')|merge({'_locale': 'en'})) }}"> EN </a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             {{ render(controller(*/
/*             'FrontendBundle:Frontend:menu'*/
/*             )) }}*/
/*             {% block content %}*/
/*             {% endblock %}*/
/*             {{ render(controller(*/
/*             'FrontendBundle:Frontend:footer'*/
/*             )) }}*/
/*         </div>*/
/* */
/*         <!-- SCROLLTOP -->*/
/*         <div style="display: none;" class="scroll-up">*/
/*             <a href="#totop"><i class="fa fa-angle-double-up"></i></a>*/
/*         </div>*/
/*                 */
/* */
/*         <!-- Javascript files -->*/
/*         <script src="{{ asset('bundles/frontend/content/jquery-2.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/bootstrap.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery_003.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery_006.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery_007.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/owl.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery_002.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/imagesloaded.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/isotope.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/packery-mode.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/appear.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery_005.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/wow.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jqBootstrapValidation.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/jquery_004.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/js.js')}}"></script><script src="{{ asset('bundles/frontend/content/main.js')}}"></script>*/
/*         <script src="{{ asset('bundles/frontend/content/gmaps.js')}}"></script>*/
/*         {#<script src="{{ asset('bundles/frontend/content/contact.js')}}"></script>#}*/
/*         <script src="{{ asset('bundles/frontend/content/custom.js')}}"></script>*/
/* */
/*         <script src="{{ asset('bundles/frontend/fancybox/jquery.fancybox.pack.js?v=2.1.5')}}"></script>*/
/*         <script type="text/javascript">*/
/*             $(document).ready(function() {*/
/*                 $(".fancybox").fancybox();*/
/*             });*/
/*         </script>*/
/*         {% block javascripts %}*/
/*         {% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
/* */
