<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\Distributor;
use Momon\BackendBundle\Form\DistributorType;

/**
 * Distributor controller.
 *
 * @Route("/distributor")
 */
class DistributorController extends Controller
{

    /**
     * INDEX ACTION - Distributor.
     *
     * @Route("/", name="backend_distributor_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aDistributors = $em->getRepository( 'BackendBundle:Distributor' )->findAll();

        return array(
            'aDistributors' => $aDistributors,
        );
    }
    /**
     * CREATE ACTION - Distributor.
     *
     * @Route("/create", name="backend_distributor_create")
     * @Template()
     */
    public function createAction( Request $oRequest )
    {
        $oDistributor = new Distributor();
        $oForm = $this->createForm( new DistributorType(), $oDistributor );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );

            if( $oForm->isValid() ){
                $em = $this->getDoctrine()->getManager();
                $em->persist( $oDistributor );
                $em->flush();

                $this->addFlash(
                    'success',
                    'Creado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_distributor_view', array( 'nDistributorId' => $oDistributor->getId() ) ) );            }
        }

        return array(
            'oDistributor' => $oDistributor,
            'oForm'   => $oForm->createView(),
        );
    }
    /**
     * VIEW ACTION - Distributor.
     *
     * @Route("/view/{nDistributorId}", name="backend_distributor_view")
     * @Template()
     */
    public function viewAction( $nDistributorId )
    {
        $em = $this->getDoctrine()->getManager();

        $oDistributor = $em->getRepository( 'BackendBundle:Distributor' )->find( $nDistributorId );

        if ( !$oDistributor ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Distributor' );
        }

        return array(
            'oDistributor' => $oDistributor,
        );
    }

    /**
     * EDIT ACTION - Distributor.
     *
     * @Route("/edit/{nDistributorId}", name="backend_distributor_edit")
     * @Template()
     */
    public function editAction( $nDistributorId, Request $oRequest )
    {
        $em = $this->getDoctrine()->getManager();

        $oDistributor = $em->getRepository( 'BackendBundle:Distributor' )->find( $nDistributorId );

        if ( !$oDistributor ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Distributor' );
        }

        $oForm = $this->createForm( new DistributorType(), $oDistributor );

        if( $oRequest->isMethod( 'POST' ) ){
            $oForm->handleRequest( $oRequest );
            if( $oForm->isValid() ){
                $em->flush();

                $this->addFlash(
                'success',
                'Editado correctamente!'
                );

                return $this->redirect( $this->generateUrl( 'backend_distributor_view', array( 'nDistributorId' => $nDistributorId ) ) );
            }
        }

        return array(
            'oDistributor' => $oDistributor,
            'oForm'         => $oForm->createView(),
        );
    }
    /**
     * DELETE ACTION - Distributor.
     *
     * @Route("/delete/{nDistributorId}", name="backend_distributor_delete")
     */
    public function deleteAction( $nDistributorId )
    {
        $em = $this->getDoctrine()->getManager();

        $oDistributor = $em->getRepository( 'BackendBundle:Distributor' )->find( $nDistributorId );

        if ( !$oDistributor ) {
            throw $this->createNotFoundException( 'No se ha podido encontrar el/la Distributor' );
        }

        $em->remove( $oDistributor );
        $em->flush();

        $this->addFlash(
        'success',
        'Eliminado correctamente!'
        );

        return $this->redirect( $this->generateUrl( 'backend_distributor_index' ) );
    }}
