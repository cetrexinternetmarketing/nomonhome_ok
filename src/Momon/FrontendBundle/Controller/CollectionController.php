<?php

namespace Momon\FrontendBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CollectionController extends Controller
{
	/**
	 * @Route("/collections", name="frontend_collection_list")
	 * @Template()
	 */
	public function listAction()
	{
		$oManager = $this->getDoctrine()->getManager();
		$aCollections = $oManager->getRepository('BackendBundle:Collection')->findByOrder();
		$oSection = $oManager->getRepository( 'BackendBundle:Section' )->findOneByName( 'collections' );

		$oNovedadesCollection = $oManager->getRepository('BackendBundle:Collection')->find(7);
		$oNovedadesCollectionModels = $oManager->getRepository('BackendBundle:Model')->findByIsNew();

		$aCollections = new ArrayCollection($aCollections);
		$aCollections->removeElement($oNovedadesCollection);

		return array(
			'oSection' => $oSection,
			'aCollection' => $aCollections,
			'oNovedadesCollectionModels' => $oNovedadesCollectionModels,
			'oNovedadesCollection' => $oNovedadesCollection
		);
	}
}
