<?php

/* FrontendBundle:Emails:contact.html.twig */
class __TwigTemplate_a8f90114b8441f5b2c50101240df6cab98e7cacb86aa5714667d45c12b46e83f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Se ha recibido un nuevo mensaje de contacto.
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["sName"]) ? $context["sName"] : null), "html", null, true);
        echo " ( ";
        echo twig_escape_filter($this->env, (isset($context["sEmail"]) ? $context["sEmail"] : null), "html", null, true);
        echo " ) dice:
";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["sText"]) ? $context["sText"] : null), "html", null, true);
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Emails:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 2,  19 => 1,);
    }
}
/* Se ha recibido un nuevo mensaje de contacto.*/
/* {{ sName }} ( {{ sEmail }} ) dice:*/
/* {{ sText }}*/
