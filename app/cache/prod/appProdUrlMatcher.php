<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/backend')) {
            // es_en__RG__backend_index
            if (rtrim($pathinfo, '/') === '/backend') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_es_en__RG__backend_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'es_en__RG__backend_index');
                }

                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BackendController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_index',);
            }
            not_es_en__RG__backend_index:

            // es__RG__backend_index
            if (rtrim($pathinfo, '/') === '/backend') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_es__RG__backend_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'es__RG__backend_index');
                }

                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BackendController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_index',);
            }
            not_es__RG__backend_index:

            // en__RG__backend_index
            if (rtrim($pathinfo, '/') === '/backend') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_en__RG__backend_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'en__RG__backend_index');
                }

                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BackendController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_index',);
            }
            not_en__RG__backend_index:

            if (0 === strpos($pathinfo, '/backend/banner')) {
                // es_en__RG__backend_banner_index
                if (rtrim($pathinfo, '/') === '/backend/banner') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_es_en__RG__backend_banner_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es_en__RG__backend_banner_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_banner_index',);
                }
                not_es_en__RG__backend_banner_index:

                // es__RG__backend_banner_index
                if (rtrim($pathinfo, '/') === '/backend/banner') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_es__RG__backend_banner_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es__RG__backend_banner_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_banner_index',);
                }
                not_es__RG__backend_banner_index:

                // en__RG__backend_banner_index
                if (rtrim($pathinfo, '/') === '/backend/banner') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_en__RG__backend_banner_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'en__RG__backend_banner_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_banner_index',);
                }
                not_en__RG__backend_banner_index:

                if (0 === strpos($pathinfo, '/backend/banner/create')) {
                    // es_en__RG__backend_banner_create
                    if ($pathinfo === '/backend/banner/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_banner_create',);
                    }

                    // es__RG__backend_banner_create
                    if ($pathinfo === '/backend/banner/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_banner_create',);
                    }

                    // en__RG__backend_banner_create
                    if ($pathinfo === '/backend/banner/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_banner_create',);
                    }

                }

                if (0 === strpos($pathinfo, '/backend/banner/view')) {
                    // es_en__RG__backend_banner_view
                    if (preg_match('#^/backend/banner/view/(?P<nBannerId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_banner_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::viewAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_banner_view
                    if (preg_match('#^/backend/banner/view/(?P<nBannerId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_banner_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::viewAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_banner_view
                    if (preg_match('#^/backend/banner/view/(?P<nBannerId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_banner_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::viewAction',  '_locale' => 'en',));
                    }

                }

                if (0 === strpos($pathinfo, '/backend/banner/edit')) {
                    // es_en__RG__backend_banner_edit
                    if (preg_match('#^/backend/banner/edit/(?P<nBannerId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_banner_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_banner_edit
                    if (preg_match('#^/backend/banner/edit/(?P<nBannerId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_banner_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::editAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_banner_edit
                    if (preg_match('#^/backend/banner/edit/(?P<nBannerId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_banner_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::editAction',  '_locale' => 'en',));
                    }

                }

                if (0 === strpos($pathinfo, '/backend/banner/delete')) {
                    // es_en__RG__backend_banner_delete
                    if (preg_match('#^/backend/banner/delete/(?P<nBannerId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_banner_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_banner_delete
                    if (preg_match('#^/backend/banner/delete/(?P<nBannerId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_banner_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::deleteAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_banner_delete
                    if (preg_match('#^/backend/banner/delete/(?P<nBannerId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_banner_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\BannerController::deleteAction',  '_locale' => 'en',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/backend/c')) {
                if (0 === strpos($pathinfo, '/backend/categoryproject')) {
                    // es_en__RG__backend_categoryproject_index
                    if (rtrim($pathinfo, '/') === '/backend/categoryproject') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_es_en__RG__backend_categoryproject_index;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'es_en__RG__backend_categoryproject_index');
                        }

                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_categoryproject_index',);
                    }
                    not_es_en__RG__backend_categoryproject_index:

                    // es__RG__backend_categoryproject_index
                    if (rtrim($pathinfo, '/') === '/backend/categoryproject') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_es__RG__backend_categoryproject_index;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'es__RG__backend_categoryproject_index');
                        }

                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_categoryproject_index',);
                    }
                    not_es__RG__backend_categoryproject_index:

                    // en__RG__backend_categoryproject_index
                    if (rtrim($pathinfo, '/') === '/backend/categoryproject') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_en__RG__backend_categoryproject_index;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'en__RG__backend_categoryproject_index');
                        }

                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_categoryproject_index',);
                    }
                    not_en__RG__backend_categoryproject_index:

                    if (0 === strpos($pathinfo, '/backend/categoryproject/create')) {
                        // es_en__RG__backend_categoryproject_create
                        if ($pathinfo === '/backend/categoryproject/create') {
                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_categoryproject_create',);
                        }

                        // es__RG__backend_categoryproject_create
                        if ($pathinfo === '/backend/categoryproject/create') {
                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_categoryproject_create',);
                        }

                        // en__RG__backend_categoryproject_create
                        if ($pathinfo === '/backend/categoryproject/create') {
                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_categoryproject_create',);
                        }

                    }

                    if (0 === strpos($pathinfo, '/backend/categoryproject/view')) {
                        // es_en__RG__backend_categoryproject_view
                        if (preg_match('#^/backend/categoryproject/view/(?P<nCategoryProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_categoryproject_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::viewAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                        }

                        // es__RG__backend_categoryproject_view
                        if (preg_match('#^/backend/categoryproject/view/(?P<nCategoryProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_categoryproject_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::viewAction',  '_locale' => 'es',));
                        }

                        // en__RG__backend_categoryproject_view
                        if (preg_match('#^/backend/categoryproject/view/(?P<nCategoryProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_categoryproject_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::viewAction',  '_locale' => 'en',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/backend/categoryproject/edit')) {
                        // es_en__RG__backend_categoryproject_edit
                        if (preg_match('#^/backend/categoryproject/edit/(?P<nCategoryProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_categoryproject_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                        }

                        // es__RG__backend_categoryproject_edit
                        if (preg_match('#^/backend/categoryproject/edit/(?P<nCategoryProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_categoryproject_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::editAction',  '_locale' => 'es',));
                        }

                        // en__RG__backend_categoryproject_edit
                        if (preg_match('#^/backend/categoryproject/edit/(?P<nCategoryProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_categoryproject_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::editAction',  '_locale' => 'en',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/backend/categoryproject/delete')) {
                        // es_en__RG__backend_categoryproject_delete
                        if (preg_match('#^/backend/categoryproject/delete/(?P<nCategoryProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_categoryproject_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                        }

                        // es__RG__backend_categoryproject_delete
                        if (preg_match('#^/backend/categoryproject/delete/(?P<nCategoryProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_categoryproject_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::deleteAction',  '_locale' => 'es',));
                        }

                        // en__RG__backend_categoryproject_delete
                        if (preg_match('#^/backend/categoryproject/delete/(?P<nCategoryProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_categoryproject_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CategoryProjectController::deleteAction',  '_locale' => 'en',));
                        }

                    }

                }

                if (0 === strpos($pathinfo, '/backend/coll')) {
                    if (0 === strpos($pathinfo, '/backend/collaborator')) {
                        // es_en__RG__backend_collaborator_index
                        if (rtrim($pathinfo, '/') === '/backend/collaborator') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_es_en__RG__backend_collaborator_index;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'es_en__RG__backend_collaborator_index');
                            }

                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_collaborator_index',);
                        }
                        not_es_en__RG__backend_collaborator_index:

                        // es__RG__backend_collaborator_index
                        if (rtrim($pathinfo, '/') === '/backend/collaborator') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_es__RG__backend_collaborator_index;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'es__RG__backend_collaborator_index');
                            }

                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_collaborator_index',);
                        }
                        not_es__RG__backend_collaborator_index:

                        // en__RG__backend_collaborator_index
                        if (rtrim($pathinfo, '/') === '/backend/collaborator') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_en__RG__backend_collaborator_index;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'en__RG__backend_collaborator_index');
                            }

                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_collaborator_index',);
                        }
                        not_en__RG__backend_collaborator_index:

                        if (0 === strpos($pathinfo, '/backend/collaborator/create')) {
                            // es_en__RG__backend_collaborator_create
                            if ($pathinfo === '/backend/collaborator/create') {
                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_collaborator_create',);
                            }

                            // es__RG__backend_collaborator_create
                            if ($pathinfo === '/backend/collaborator/create') {
                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_collaborator_create',);
                            }

                            // en__RG__backend_collaborator_create
                            if ($pathinfo === '/backend/collaborator/create') {
                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_collaborator_create',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/collaborator/view')) {
                            // es_en__RG__backend_collaborator_view
                            if (preg_match('#^/backend/collaborator/view/(?P<nCollaboratorId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_collaborator_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::viewAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                            }

                            // es__RG__backend_collaborator_view
                            if (preg_match('#^/backend/collaborator/view/(?P<nCollaboratorId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_collaborator_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::viewAction',  '_locale' => 'es',));
                            }

                            // en__RG__backend_collaborator_view
                            if (preg_match('#^/backend/collaborator/view/(?P<nCollaboratorId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_collaborator_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::viewAction',  '_locale' => 'en',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/collaborator/edit')) {
                            // es_en__RG__backend_collaborator_edit
                            if (preg_match('#^/backend/collaborator/edit/(?P<nCollaboratorId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_collaborator_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                            }

                            // es__RG__backend_collaborator_edit
                            if (preg_match('#^/backend/collaborator/edit/(?P<nCollaboratorId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_collaborator_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::editAction',  '_locale' => 'es',));
                            }

                            // en__RG__backend_collaborator_edit
                            if (preg_match('#^/backend/collaborator/edit/(?P<nCollaboratorId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_collaborator_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::editAction',  '_locale' => 'en',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/collaborator/delete')) {
                            // es_en__RG__backend_collaborator_delete
                            if (preg_match('#^/backend/collaborator/delete/(?P<nCollaboratorId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_collaborator_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                            }

                            // es__RG__backend_collaborator_delete
                            if (preg_match('#^/backend/collaborator/delete/(?P<nCollaboratorId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_collaborator_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::deleteAction',  '_locale' => 'es',));
                            }

                            // en__RG__backend_collaborator_delete
                            if (preg_match('#^/backend/collaborator/delete/(?P<nCollaboratorId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_collaborator_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollaboratorController::deleteAction',  '_locale' => 'en',));
                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/backend/collection')) {
                        // es_en__RG__backend_collection_index
                        if (rtrim($pathinfo, '/') === '/backend/collection') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_es_en__RG__backend_collection_index;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'es_en__RG__backend_collection_index');
                            }

                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_collection_index',);
                        }
                        not_es_en__RG__backend_collection_index:

                        // es__RG__backend_collection_index
                        if (rtrim($pathinfo, '/') === '/backend/collection') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_es__RG__backend_collection_index;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'es__RG__backend_collection_index');
                            }

                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_collection_index',);
                        }
                        not_es__RG__backend_collection_index:

                        // en__RG__backend_collection_index
                        if (rtrim($pathinfo, '/') === '/backend/collection') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_en__RG__backend_collection_index;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'en__RG__backend_collection_index');
                            }

                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_collection_index',);
                        }
                        not_en__RG__backend_collection_index:

                        if (0 === strpos($pathinfo, '/backend/collection/create')) {
                            // es_en__RG__backend_collection_create
                            if ($pathinfo === '/backend/collection/create') {
                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_collection_create',);
                            }

                            // es__RG__backend_collection_create
                            if ($pathinfo === '/backend/collection/create') {
                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_collection_create',);
                            }

                            // en__RG__backend_collection_create
                            if ($pathinfo === '/backend/collection/create') {
                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_collection_create',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/collection/view')) {
                            // es_en__RG__backend_collection_view
                            if (preg_match('#^/backend/collection/view/(?P<nCollectionId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_collection_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::viewAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                            }

                            // es__RG__backend_collection_view
                            if (preg_match('#^/backend/collection/view/(?P<nCollectionId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_collection_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::viewAction',  '_locale' => 'es',));
                            }

                            // en__RG__backend_collection_view
                            if (preg_match('#^/backend/collection/view/(?P<nCollectionId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_collection_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::viewAction',  '_locale' => 'en',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/collection/edit')) {
                            // es_en__RG__backend_collection_edit
                            if (preg_match('#^/backend/collection/edit/(?P<nCollectionId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_collection_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                            }

                            // es__RG__backend_collection_edit
                            if (preg_match('#^/backend/collection/edit/(?P<nCollectionId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_collection_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::editAction',  '_locale' => 'es',));
                            }

                            // en__RG__backend_collection_edit
                            if (preg_match('#^/backend/collection/edit/(?P<nCollectionId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_collection_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::editAction',  '_locale' => 'en',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/collection/delete')) {
                            // es_en__RG__backend_collection_delete
                            if (preg_match('#^/backend/collection/delete/(?P<nCollectionId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_collection_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                            }

                            // es__RG__backend_collection_delete
                            if (preg_match('#^/backend/collection/delete/(?P<nCollectionId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_collection_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::deleteAction',  '_locale' => 'es',));
                            }

                            // en__RG__backend_collection_delete
                            if (preg_match('#^/backend/collection/delete/(?P<nCollectionId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_collection_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionController::deleteAction',  '_locale' => 'en',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/collectionhome')) {
                            // es_en__RG__backend_collectionhome_index
                            if (rtrim($pathinfo, '/') === '/backend/collectionhome') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_es_en__RG__backend_collectionhome_index;
                                }

                                if (substr($pathinfo, -1) !== '/') {
                                    return $this->redirect($pathinfo.'/', 'es_en__RG__backend_collectionhome_index');
                                }

                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_collectionhome_index',);
                            }
                            not_es_en__RG__backend_collectionhome_index:

                            // es__RG__backend_collectionhome_index
                            if (rtrim($pathinfo, '/') === '/backend/collectionhome') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_es__RG__backend_collectionhome_index;
                                }

                                if (substr($pathinfo, -1) !== '/') {
                                    return $this->redirect($pathinfo.'/', 'es__RG__backend_collectionhome_index');
                                }

                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_collectionhome_index',);
                            }
                            not_es__RG__backend_collectionhome_index:

                            // en__RG__backend_collectionhome_index
                            if (rtrim($pathinfo, '/') === '/backend/collectionhome') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_en__RG__backend_collectionhome_index;
                                }

                                if (substr($pathinfo, -1) !== '/') {
                                    return $this->redirect($pathinfo.'/', 'en__RG__backend_collectionhome_index');
                                }

                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_collectionhome_index',);
                            }
                            not_en__RG__backend_collectionhome_index:

                            if (0 === strpos($pathinfo, '/backend/collectionhome/create')) {
                                // es_en__RG__backend_collectionhome_create
                                if ($pathinfo === '/backend/collectionhome/create') {
                                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_collectionhome_create',);
                                }

                                // es__RG__backend_collectionhome_create
                                if ($pathinfo === '/backend/collectionhome/create') {
                                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_collectionhome_create',);
                                }

                                // en__RG__backend_collectionhome_create
                                if ($pathinfo === '/backend/collectionhome/create') {
                                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_collectionhome_create',);
                                }

                            }

                            if (0 === strpos($pathinfo, '/backend/collectionhome/view')) {
                                // es_en__RG__backend_collectionhome_view
                                if (preg_match('#^/backend/collectionhome/view/(?P<nCollectionHomeId>[^/]++)$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_collectionhome_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::viewAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                                }

                                // es__RG__backend_collectionhome_view
                                if (preg_match('#^/backend/collectionhome/view/(?P<nCollectionHomeId>[^/]++)$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_collectionhome_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::viewAction',  '_locale' => 'es',));
                                }

                                // en__RG__backend_collectionhome_view
                                if (preg_match('#^/backend/collectionhome/view/(?P<nCollectionHomeId>[^/]++)$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_collectionhome_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::viewAction',  '_locale' => 'en',));
                                }

                            }

                            if (0 === strpos($pathinfo, '/backend/collectionhome/edit')) {
                                // es_en__RG__backend_collectionhome_edit
                                if (preg_match('#^/backend/collectionhome/edit/(?P<nCollectionHomeId>[^/]++)$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_collectionhome_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                                }

                                // es__RG__backend_collectionhome_edit
                                if (preg_match('#^/backend/collectionhome/edit/(?P<nCollectionHomeId>[^/]++)$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_collectionhome_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::editAction',  '_locale' => 'es',));
                                }

                                // en__RG__backend_collectionhome_edit
                                if (preg_match('#^/backend/collectionhome/edit/(?P<nCollectionHomeId>[^/]++)$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_collectionhome_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::editAction',  '_locale' => 'en',));
                                }

                            }

                            if (0 === strpos($pathinfo, '/backend/collectionhome/delete')) {
                                // es_en__RG__backend_collectionhome_delete
                                if (preg_match('#^/backend/collectionhome/delete/(?P<nCollectionHomeId>[^/]++)$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_collectionhome_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                                }

                                // es__RG__backend_collectionhome_delete
                                if (preg_match('#^/backend/collectionhome/delete/(?P<nCollectionHomeId>[^/]++)$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_collectionhome_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::deleteAction',  '_locale' => 'es',));
                                }

                                // en__RG__backend_collectionhome_delete
                                if (preg_match('#^/backend/collectionhome/delete/(?P<nCollectionHomeId>[^/]++)$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_collectionhome_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\CollectionHomeController::deleteAction',  '_locale' => 'en',));
                                }

                            }

                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/backend/distributor')) {
                // es_en__RG__backend_distributor_index
                if (rtrim($pathinfo, '/') === '/backend/distributor') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_es_en__RG__backend_distributor_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es_en__RG__backend_distributor_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_distributor_index',);
                }
                not_es_en__RG__backend_distributor_index:

                // es__RG__backend_distributor_index
                if (rtrim($pathinfo, '/') === '/backend/distributor') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_es__RG__backend_distributor_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es__RG__backend_distributor_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_distributor_index',);
                }
                not_es__RG__backend_distributor_index:

                // en__RG__backend_distributor_index
                if (rtrim($pathinfo, '/') === '/backend/distributor') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_en__RG__backend_distributor_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'en__RG__backend_distributor_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_distributor_index',);
                }
                not_en__RG__backend_distributor_index:

                if (0 === strpos($pathinfo, '/backend/distributor/create')) {
                    // es_en__RG__backend_distributor_create
                    if ($pathinfo === '/backend/distributor/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_distributor_create',);
                    }

                    // es__RG__backend_distributor_create
                    if ($pathinfo === '/backend/distributor/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_distributor_create',);
                    }

                    // en__RG__backend_distributor_create
                    if ($pathinfo === '/backend/distributor/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_distributor_create',);
                    }

                }

                if (0 === strpos($pathinfo, '/backend/distributor/view')) {
                    // es_en__RG__backend_distributor_view
                    if (preg_match('#^/backend/distributor/view/(?P<nDistributorId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_distributor_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::viewAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_distributor_view
                    if (preg_match('#^/backend/distributor/view/(?P<nDistributorId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_distributor_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::viewAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_distributor_view
                    if (preg_match('#^/backend/distributor/view/(?P<nDistributorId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_distributor_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::viewAction',  '_locale' => 'en',));
                    }

                }

                if (0 === strpos($pathinfo, '/backend/distributor/edit')) {
                    // es_en__RG__backend_distributor_edit
                    if (preg_match('#^/backend/distributor/edit/(?P<nDistributorId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_distributor_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_distributor_edit
                    if (preg_match('#^/backend/distributor/edit/(?P<nDistributorId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_distributor_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::editAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_distributor_edit
                    if (preg_match('#^/backend/distributor/edit/(?P<nDistributorId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_distributor_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::editAction',  '_locale' => 'en',));
                    }

                }

                if (0 === strpos($pathinfo, '/backend/distributor/delete')) {
                    // es_en__RG__backend_distributor_delete
                    if (preg_match('#^/backend/distributor/delete/(?P<nDistributorId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_distributor_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_distributor_delete
                    if (preg_match('#^/backend/distributor/delete/(?P<nDistributorId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_distributor_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::deleteAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_distributor_delete
                    if (preg_match('#^/backend/distributor/delete/(?P<nDistributorId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_distributor_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\DistributorController::deleteAction',  '_locale' => 'en',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/backend/homemodel')) {
                // es_en__RG__backend_homemodel_index
                if (rtrim($pathinfo, '/') === '/backend/homemodel') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_es_en__RG__backend_homemodel_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es_en__RG__backend_homemodel_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_homemodel_index',);
                }
                not_es_en__RG__backend_homemodel_index:

                // es__RG__backend_homemodel_index
                if (rtrim($pathinfo, '/') === '/backend/homemodel') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_es__RG__backend_homemodel_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es__RG__backend_homemodel_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_homemodel_index',);
                }
                not_es__RG__backend_homemodel_index:

                // en__RG__backend_homemodel_index
                if (rtrim($pathinfo, '/') === '/backend/homemodel') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_en__RG__backend_homemodel_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'en__RG__backend_homemodel_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_homemodel_index',);
                }
                not_en__RG__backend_homemodel_index:

                if (0 === strpos($pathinfo, '/backend/homemodel/create')) {
                    // es_en__RG__backend_homemodel_create
                    if ($pathinfo === '/backend/homemodel/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_homemodel_create',);
                    }

                    // es__RG__backend_homemodel_create
                    if ($pathinfo === '/backend/homemodel/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_homemodel_create',);
                    }

                    // en__RG__backend_homemodel_create
                    if ($pathinfo === '/backend/homemodel/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_homemodel_create',);
                    }

                }

                if (0 === strpos($pathinfo, '/backend/homemodel/view')) {
                    // es_en__RG__backend_homemodel_view
                    if (preg_match('#^/backend/homemodel/view/(?P<nHomeModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_homemodel_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::viewAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_homemodel_view
                    if (preg_match('#^/backend/homemodel/view/(?P<nHomeModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_homemodel_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::viewAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_homemodel_view
                    if (preg_match('#^/backend/homemodel/view/(?P<nHomeModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_homemodel_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::viewAction',  '_locale' => 'en',));
                    }

                }

                if (0 === strpos($pathinfo, '/backend/homemodel/edit')) {
                    // es_en__RG__backend_homemodel_edit
                    if (preg_match('#^/backend/homemodel/edit/(?P<nHomeModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_homemodel_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_homemodel_edit
                    if (preg_match('#^/backend/homemodel/edit/(?P<nHomeModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_homemodel_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::editAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_homemodel_edit
                    if (preg_match('#^/backend/homemodel/edit/(?P<nHomeModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_homemodel_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::editAction',  '_locale' => 'en',));
                    }

                }

                if (0 === strpos($pathinfo, '/backend/homemodel/delete')) {
                    // es_en__RG__backend_homemodel_delete
                    if (preg_match('#^/backend/homemodel/delete/(?P<nHomeModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_homemodel_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_homemodel_delete
                    if (preg_match('#^/backend/homemodel/delete/(?P<nHomeModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_homemodel_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::deleteAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_homemodel_delete
                    if (preg_match('#^/backend/homemodel/delete/(?P<nHomeModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_homemodel_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\HomeModelController::deleteAction',  '_locale' => 'en',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/backend/model')) {
                // es_en__RG__backend_model_index
                if (rtrim($pathinfo, '/') === '/backend/model') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_es_en__RG__backend_model_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es_en__RG__backend_model_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_model_index',);
                }
                not_es_en__RG__backend_model_index:

                // es__RG__backend_model_index
                if (rtrim($pathinfo, '/') === '/backend/model') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_es__RG__backend_model_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es__RG__backend_model_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_model_index',);
                }
                not_es__RG__backend_model_index:

                // en__RG__backend_model_index
                if (rtrim($pathinfo, '/') === '/backend/model') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_en__RG__backend_model_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'en__RG__backend_model_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_model_index',);
                }
                not_en__RG__backend_model_index:

                if (0 === strpos($pathinfo, '/backend/model/create')) {
                    // es_en__RG__backend_model_create
                    if ($pathinfo === '/backend/model/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_model_create',);
                    }

                    // es__RG__backend_model_create
                    if ($pathinfo === '/backend/model/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_model_create',);
                    }

                    // en__RG__backend_model_create
                    if ($pathinfo === '/backend/model/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_model_create',);
                    }

                }

                if (0 === strpos($pathinfo, '/backend/model/view')) {
                    // es_en__RG__backend_model_view
                    if (preg_match('#^/backend/model/view/(?P<nModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_model_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::viewAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_model_view
                    if (preg_match('#^/backend/model/view/(?P<nModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_model_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::viewAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_model_view
                    if (preg_match('#^/backend/model/view/(?P<nModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_model_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::viewAction',  '_locale' => 'en',));
                    }

                }

                if (0 === strpos($pathinfo, '/backend/model/edit')) {
                    // es_en__RG__backend_model_edit
                    if (preg_match('#^/backend/model/edit/(?P<nModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_model_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_model_edit
                    if (preg_match('#^/backend/model/edit/(?P<nModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_model_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::editAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_model_edit
                    if (preg_match('#^/backend/model/edit/(?P<nModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_model_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::editAction',  '_locale' => 'en',));
                    }

                }

                if (0 === strpos($pathinfo, '/backend/model/delete')) {
                    // es_en__RG__backend_model_delete
                    if (preg_match('#^/backend/model/delete/(?P<nModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_model_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_model_delete
                    if (preg_match('#^/backend/model/delete/(?P<nModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_model_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::deleteAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_model_delete
                    if (preg_match('#^/backend/model/delete/(?P<nModelId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_model_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ModelController::deleteAction',  '_locale' => 'en',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/backend/p')) {
                if (0 === strpos($pathinfo, '/backend/pa')) {
                    if (0 === strpos($pathinfo, '/backend/page')) {
                        // es_en__RG__backend_page_index
                        if (rtrim($pathinfo, '/') === '/backend/page') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_es_en__RG__backend_page_index;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'es_en__RG__backend_page_index');
                            }

                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_page_index',);
                        }
                        not_es_en__RG__backend_page_index:

                        // es__RG__backend_page_index
                        if (rtrim($pathinfo, '/') === '/backend/page') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_es__RG__backend_page_index;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'es__RG__backend_page_index');
                            }

                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_page_index',);
                        }
                        not_es__RG__backend_page_index:

                        // en__RG__backend_page_index
                        if (rtrim($pathinfo, '/') === '/backend/page') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_en__RG__backend_page_index;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'en__RG__backend_page_index');
                            }

                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_page_index',);
                        }
                        not_en__RG__backend_page_index:

                        if (0 === strpos($pathinfo, '/backend/page/create')) {
                            // es_en__RG__backend_page_create
                            if ($pathinfo === '/backend/page/create') {
                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_page_create',);
                            }

                            // es__RG__backend_page_create
                            if ($pathinfo === '/backend/page/create') {
                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_page_create',);
                            }

                            // en__RG__backend_page_create
                            if ($pathinfo === '/backend/page/create') {
                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_page_create',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/page/view')) {
                            // es_en__RG__backend_page_view
                            if (preg_match('#^/backend/page/view/(?P<nPageId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_page_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::viewAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                            }

                            // es__RG__backend_page_view
                            if (preg_match('#^/backend/page/view/(?P<nPageId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_page_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::viewAction',  '_locale' => 'es',));
                            }

                            // en__RG__backend_page_view
                            if (preg_match('#^/backend/page/view/(?P<nPageId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_page_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::viewAction',  '_locale' => 'en',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/page/edit')) {
                            // es_en__RG__backend_page_edit
                            if (preg_match('#^/backend/page/edit/(?P<nPageId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_page_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                            }

                            // es__RG__backend_page_edit
                            if (preg_match('#^/backend/page/edit/(?P<nPageId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_page_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::editAction',  '_locale' => 'es',));
                            }

                            // en__RG__backend_page_edit
                            if (preg_match('#^/backend/page/edit/(?P<nPageId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_page_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::editAction',  '_locale' => 'en',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/page/delete')) {
                            // es_en__RG__backend_page_delete
                            if (preg_match('#^/backend/page/delete/(?P<nPageId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_page_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                            }

                            // es__RG__backend_page_delete
                            if (preg_match('#^/backend/page/delete/(?P<nPageId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_page_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::deleteAction',  '_locale' => 'es',));
                            }

                            // en__RG__backend_page_delete
                            if (preg_match('#^/backend/page/delete/(?P<nPageId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_page_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\PageController::deleteAction',  '_locale' => 'en',));
                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/backend/parallax')) {
                        // es_en__RG__backend_parallax_index
                        if (rtrim($pathinfo, '/') === '/backend/parallax') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_es_en__RG__backend_parallax_index;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'es_en__RG__backend_parallax_index');
                            }

                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_parallax_index',);
                        }
                        not_es_en__RG__backend_parallax_index:

                        // es__RG__backend_parallax_index
                        if (rtrim($pathinfo, '/') === '/backend/parallax') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_es__RG__backend_parallax_index;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'es__RG__backend_parallax_index');
                            }

                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_parallax_index',);
                        }
                        not_es__RG__backend_parallax_index:

                        // en__RG__backend_parallax_index
                        if (rtrim($pathinfo, '/') === '/backend/parallax') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_en__RG__backend_parallax_index;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'en__RG__backend_parallax_index');
                            }

                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_parallax_index',);
                        }
                        not_en__RG__backend_parallax_index:

                        if (0 === strpos($pathinfo, '/backend/parallax/create')) {
                            // es_en__RG__backend_parallax_create
                            if ($pathinfo === '/backend/parallax/create') {
                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_parallax_create',);
                            }

                            // es__RG__backend_parallax_create
                            if ($pathinfo === '/backend/parallax/create') {
                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_parallax_create',);
                            }

                            // en__RG__backend_parallax_create
                            if ($pathinfo === '/backend/parallax/create') {
                                return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_parallax_create',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/parallax/view')) {
                            // es_en__RG__backend_parallax_view
                            if (preg_match('#^/backend/parallax/view/(?P<nParallaxId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_parallax_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::viewAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                            }

                            // es__RG__backend_parallax_view
                            if (preg_match('#^/backend/parallax/view/(?P<nParallaxId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_parallax_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::viewAction',  '_locale' => 'es',));
                            }

                            // en__RG__backend_parallax_view
                            if (preg_match('#^/backend/parallax/view/(?P<nParallaxId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_parallax_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::viewAction',  '_locale' => 'en',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/parallax/edit')) {
                            // es_en__RG__backend_parallax_edit
                            if (preg_match('#^/backend/parallax/edit/(?P<nParallaxId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_parallax_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                            }

                            // es__RG__backend_parallax_edit
                            if (preg_match('#^/backend/parallax/edit/(?P<nParallaxId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_parallax_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::editAction',  '_locale' => 'es',));
                            }

                            // en__RG__backend_parallax_edit
                            if (preg_match('#^/backend/parallax/edit/(?P<nParallaxId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_parallax_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::editAction',  '_locale' => 'en',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/backend/parallax/delete')) {
                            // es_en__RG__backend_parallax_delete
                            if (preg_match('#^/backend/parallax/delete/(?P<nParallaxId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_parallax_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                            }

                            // es__RG__backend_parallax_delete
                            if (preg_match('#^/backend/parallax/delete/(?P<nParallaxId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_parallax_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::deleteAction',  '_locale' => 'es',));
                            }

                            // en__RG__backend_parallax_delete
                            if (preg_match('#^/backend/parallax/delete/(?P<nParallaxId>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_parallax_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ParallaxController::deleteAction',  '_locale' => 'en',));
                            }

                        }

                    }

                }

                if (0 === strpos($pathinfo, '/backend/project')) {
                    // es_en__RG__backend_project_index
                    if (rtrim($pathinfo, '/') === '/backend/project') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_es_en__RG__backend_project_index;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'es_en__RG__backend_project_index');
                        }

                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_project_index',);
                    }
                    not_es_en__RG__backend_project_index:

                    // es__RG__backend_project_index
                    if (rtrim($pathinfo, '/') === '/backend/project') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_es__RG__backend_project_index;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'es__RG__backend_project_index');
                        }

                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_project_index',);
                    }
                    not_es__RG__backend_project_index:

                    // en__RG__backend_project_index
                    if (rtrim($pathinfo, '/') === '/backend/project') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_en__RG__backend_project_index;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'en__RG__backend_project_index');
                        }

                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_project_index',);
                    }
                    not_en__RG__backend_project_index:

                    if (0 === strpos($pathinfo, '/backend/project/create')) {
                        // es_en__RG__backend_project_create
                        if ($pathinfo === '/backend/project/create') {
                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_project_create',);
                        }

                        // es__RG__backend_project_create
                        if ($pathinfo === '/backend/project/create') {
                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_project_create',);
                        }

                        // en__RG__backend_project_create
                        if ($pathinfo === '/backend/project/create') {
                            return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_project_create',);
                        }

                    }

                    if (0 === strpos($pathinfo, '/backend/project/view')) {
                        // es_en__RG__backend_project_view
                        if (preg_match('#^/backend/project/view/(?P<nProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_project_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::viewAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                        }

                        // es__RG__backend_project_view
                        if (preg_match('#^/backend/project/view/(?P<nProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_project_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::viewAction',  '_locale' => 'es',));
                        }

                        // en__RG__backend_project_view
                        if (preg_match('#^/backend/project/view/(?P<nProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_project_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::viewAction',  '_locale' => 'en',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/backend/project/edit')) {
                        // es_en__RG__backend_project_edit
                        if (preg_match('#^/backend/project/edit/(?P<nProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_project_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                        }

                        // es__RG__backend_project_edit
                        if (preg_match('#^/backend/project/edit/(?P<nProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_project_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::editAction',  '_locale' => 'es',));
                        }

                        // en__RG__backend_project_edit
                        if (preg_match('#^/backend/project/edit/(?P<nProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_project_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::editAction',  '_locale' => 'en',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/backend/project/delete')) {
                        // es_en__RG__backend_project_delete
                        if (preg_match('#^/backend/project/delete/(?P<nProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_project_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                        }

                        // es__RG__backend_project_delete
                        if (preg_match('#^/backend/project/delete/(?P<nProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_project_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::deleteAction',  '_locale' => 'es',));
                        }

                        // en__RG__backend_project_delete
                        if (preg_match('#^/backend/project/delete/(?P<nProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_project_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\ProjectController::deleteAction',  '_locale' => 'en',));
                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/backend/section')) {
                // es_en__RG__backend_section_index
                if (rtrim($pathinfo, '/') === '/backend/section') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_es_en__RG__backend_section_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es_en__RG__backend_section_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::indexAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_section_index',);
                }
                not_es_en__RG__backend_section_index:

                // es__RG__backend_section_index
                if (rtrim($pathinfo, '/') === '/backend/section') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_es__RG__backend_section_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es__RG__backend_section_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_section_index',);
                }
                not_es__RG__backend_section_index:

                // en__RG__backend_section_index
                if (rtrim($pathinfo, '/') === '/backend/section') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_en__RG__backend_section_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'en__RG__backend_section_index');
                    }

                    return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_section_index',);
                }
                not_en__RG__backend_section_index:

                if (0 === strpos($pathinfo, '/backend/section/create')) {
                    // es_en__RG__backend_section_create
                    if ($pathinfo === '/backend/section/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__backend_section_create',);
                    }

                    // es__RG__backend_section_create
                    if ($pathinfo === '/backend/section/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__backend_section_create',);
                    }

                    // en__RG__backend_section_create
                    if ($pathinfo === '/backend/section/create') {
                        return array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__backend_section_create',);
                    }

                }

                if (0 === strpos($pathinfo, '/backend/section/view')) {
                    // es_en__RG__backend_section_view
                    if (preg_match('#^/backend/section/view/(?P<nSectionId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_section_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::viewAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_section_view
                    if (preg_match('#^/backend/section/view/(?P<nSectionId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_section_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::viewAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_section_view
                    if (preg_match('#^/backend/section/view/(?P<nSectionId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_section_view')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::viewAction',  '_locale' => 'en',));
                    }

                }

                if (0 === strpos($pathinfo, '/backend/section/edit')) {
                    // es_en__RG__backend_section_edit
                    if (preg_match('#^/backend/section/edit/(?P<nSectionId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_section_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_section_edit
                    if (preg_match('#^/backend/section/edit/(?P<nSectionId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_section_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::editAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_section_edit
                    if (preg_match('#^/backend/section/edit/(?P<nSectionId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_section_edit')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::editAction',  '_locale' => 'en',));
                    }

                }

                if (0 === strpos($pathinfo, '/backend/section/delete')) {
                    // es_en__RG__backend_section_delete
                    if (preg_match('#^/backend/section/delete/(?P<nSectionId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__backend_section_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__backend_section_delete
                    if (preg_match('#^/backend/section/delete/(?P<nSectionId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__backend_section_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::deleteAction',  '_locale' => 'es',));
                    }

                    // en__RG__backend_section_delete
                    if (preg_match('#^/backend/section/delete/(?P<nSectionId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__backend_section_delete')), array (  '_controller' => 'Momon\\BackendBundle\\Controller\\SectionController::deleteAction',  '_locale' => 'en',));
                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/col')) {
            // es__RG__frontend_collaborator_list
            if ($pathinfo === '/colaboradores') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\CollaboratorController::listAction',  '_locale' => 'es',  '_route' => 'es__RG__frontend_collaborator_list',);
            }

            // en__RG__frontend_collaborator_list
            if ($pathinfo === '/collaborators') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\CollaboratorController::listAction',  '_locale' => 'en',  '_route' => 'en__RG__frontend_collaborator_list',);
            }

            // es__RG__frontend_collaborator_view
            if (0 === strpos($pathinfo, '/colaborador') && preg_match('#^/colaborador/(?P<sSlug>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__frontend_collaborator_view')), array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\CollaboratorController::viewAction',  '_locale' => 'es',));
            }

            // en__RG__frontend_collaborator_view
            if (0 === strpos($pathinfo, '/collaborator') && preg_match('#^/collaborator/(?P<sSlug>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__frontend_collaborator_view')), array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\CollaboratorController::viewAction',  '_locale' => 'en',));
            }

            // es__RG__frontend_collection_list
            if ($pathinfo === '/colecciones') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\CollectionController::listAction',  '_locale' => 'es',  '_route' => 'es__RG__frontend_collection_list',);
            }

            // en__RG__frontend_collection_list
            if ($pathinfo === '/collections') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\CollectionController::listAction',  '_locale' => 'en',  '_route' => 'en__RG__frontend_collection_list',);
            }

        }

        if (0 === strpos($pathinfo, '/homecollections')) {
            // es_en__RG__frontend_homecollection_list
            if ($pathinfo === '/homecollections') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\CollectionHomeController::listAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__frontend_homecollection_list',);
            }

            // es__RG__frontend_homecollection_list
            if ($pathinfo === '/homecollections') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\CollectionHomeController::listAction',  '_locale' => 'es',  '_route' => 'es__RG__frontend_homecollection_list',);
            }

            // en__RG__frontend_homecollection_list
            if ($pathinfo === '/homecollections') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\CollectionHomeController::listAction',  '_locale' => 'en',  '_route' => 'en__RG__frontend_homecollection_list',);
            }

        }

        if (0 === strpos($pathinfo, '/distribu')) {
            // es__RG__frontend_distributor_list
            if ($pathinfo === '/distribuidores') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\DistributorController::listAction',  '_locale' => 'es',  '_route' => 'es__RG__frontend_distributor_list',);
            }

            // en__RG__frontend_distributor_list
            if ($pathinfo === '/distributors') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\DistributorController::listAction',  '_locale' => 'en',  '_route' => 'en__RG__frontend_distributor_list',);
            }

        }

        // es__RG__frontend_frontend_index
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'es__RG__frontend_frontend_index');
            }

            return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\FrontendController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__frontend_frontend_index',);
        }

        // en__RG__frontend_frontend_index
        if ($pathinfo === '/en') {
            return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\FrontendController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__frontend_frontend_index',);
        }

        if (0 === strpos($pathinfo, '/contact')) {
            // es__RG__frontend_frontend_contact
            if ($pathinfo === '/contacto') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\FrontendController::contactAction',  '_locale' => 'es',  '_route' => 'es__RG__frontend_frontend_contact',);
            }

            // en__RG__frontend_frontend_contact
            if ($pathinfo === '/contact') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\FrontendController::contactAction',  '_locale' => 'en',  '_route' => 'en__RG__frontend_frontend_contact',);
            }

            if (0 === strpos($pathinfo, '/contact/send-email')) {
                // es_en__RG__frontend_frontend_send_email
                if ($pathinfo === '/contact/send-email') {
                    return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\FrontendController::sendEmailAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__frontend_frontend_send_email',);
                }

                // es__RG__frontend_frontend_send_email
                if ($pathinfo === '/contact/send-email') {
                    return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\FrontendController::sendEmailAction',  '_locale' => 'es',  '_route' => 'es__RG__frontend_frontend_send_email',);
                }

                // en__RG__frontend_frontend_send_email
                if ($pathinfo === '/contact/send-email') {
                    return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\FrontendController::sendEmailAction',  '_locale' => 'en',  '_route' => 'en__RG__frontend_frontend_send_email',);
                }

            }

        }

        // es__RG__frontend_home_index
        if ($pathinfo === '/nomonhome') {
            return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\HomeController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__frontend_home_index',);
        }

        // en__RG__frontend_home_index
        if ($pathinfo === '/en/nomonhome') {
            return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\HomeController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__frontend_home_index',);
        }

        // es__RG__frontend_homemodel_view
        if (0 === strpos($pathinfo, '/modelo-home') && preg_match('#^/modelo\\-home/(?P<sHomeModel>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__frontend_homemodel_view')), array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\HomeModelController::viewAction',  '_locale' => 'es',));
        }

        // en__RG__frontend_homemodel_view
        if (0 === strpos($pathinfo, '/home-model') && preg_match('#^/home\\-model/(?P<sHomeModel>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__frontend_homemodel_view')), array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\HomeModelController::viewAction',  '_locale' => 'en',));
        }

        if (0 === strpos($pathinfo, '/model')) {
            // es__RG__frontend_model_view
            if (0 === strpos($pathinfo, '/modelo') && preg_match('#^/modelo/(?P<sModelSlug>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__frontend_model_view')), array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\ModelController::viewAction',  '_locale' => 'es',));
            }

            // en__RG__frontend_model_view
            if (preg_match('#^/model/(?P<sModelSlug>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__frontend_model_view')), array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\ModelController::viewAction',  '_locale' => 'en',));
            }

        }

        if (0 === strpos($pathinfo, '/nomon-culture/e')) {
            // es__RG__frontend_page_view
            if (0 === strpos($pathinfo, '/nomon-culture/es') && preg_match('#^/nomon\\-culture/es/(?P<sSlug>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__frontend_page_view')), array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\PageController::viewAction',  '_locale' => 'es',));
            }

            // en__RG__frontend_page_view
            if (0 === strpos($pathinfo, '/nomon-culture/en') && preg_match('#^/nomon\\-culture/en/(?P<sSlug>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__frontend_page_view')), array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\PageController::viewAction',  '_locale' => 'en',));
            }

        }

        if (0 === strpos($pathinfo, '/pro')) {
            // es__RG__frontend_project_view
            if (0 === strpos($pathinfo, '/proyecto') && preg_match('#^/proyecto/(?P<nProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__frontend_project_view')), array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\ProjectController::viewAction',  '_locale' => 'es',));
            }

            // en__RG__frontend_project_view
            if (0 === strpos($pathinfo, '/project') && preg_match('#^/project/(?P<nProjectId>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__frontend_project_view')), array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\ProjectController::viewAction',  '_locale' => 'en',));
            }

        }

        if (0 === strpos($pathinfo, '/list')) {
            // es__RG__frontend_project_list
            if ($pathinfo === '/lista-proyectos') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\ProjectController::listAction',  '_locale' => 'es',  '_route' => 'es__RG__frontend_project_list',);
            }

            // en__RG__frontend_project_list
            if ($pathinfo === '/list-projects') {
                return array (  '_controller' => 'Momon\\FrontendBundle\\Controller\\ProjectController::listAction',  '_locale' => 'en',  '_route' => 'en__RG__frontend_project_list',);
            }

        }

        if (0 === strpos($pathinfo, '/mediabundle')) {
            if (0 === strpos($pathinfo, '/mediabundle/folder')) {
                if (0 === strpos($pathinfo, '/mediabundle/folder/upload')) {
                    // es_en__RG__media_document_upload
                    if (preg_match('#^/mediabundle/folder/upload/(?P<nFolderId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__media_document_upload')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\DocumentController::uploadAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__media_document_upload
                    if (preg_match('#^/mediabundle/folder/upload/(?P<nFolderId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__media_document_upload')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\DocumentController::uploadAction',  '_locale' => 'es',));
                    }

                    // en__RG__media_document_upload
                    if (preg_match('#^/mediabundle/folder/upload/(?P<nFolderId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__media_document_upload')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\DocumentController::uploadAction',  '_locale' => 'en',));
                    }

                }

                if (0 === strpos($pathinfo, '/mediabundle/folder/sort')) {
                    // es_en__RG__media_document_sort
                    if ($pathinfo === '/mediabundle/folder/sort') {
                        return array (  '_controller' => 'Netik\\MediaBundle\\Controller\\DocumentController::sortAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__media_document_sort',);
                    }

                    // es__RG__media_document_sort
                    if ($pathinfo === '/mediabundle/folder/sort') {
                        return array (  '_controller' => 'Netik\\MediaBundle\\Controller\\DocumentController::sortAction',  '_locale' => 'es',  '_route' => 'es__RG__media_document_sort',);
                    }

                    // en__RG__media_document_sort
                    if ($pathinfo === '/mediabundle/folder/sort') {
                        return array (  '_controller' => 'Netik\\MediaBundle\\Controller\\DocumentController::sortAction',  '_locale' => 'en',  '_route' => 'en__RG__media_document_sort',);
                    }

                }

                if (0 === strpos($pathinfo, '/mediabundle/folder/delete')) {
                    // es_en__RG__media_document_delete
                    if (preg_match('#^/mediabundle/folder/delete/(?P<nDocumentId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__media_document_delete')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\DocumentController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__media_document_delete
                    if (preg_match('#^/mediabundle/folder/delete/(?P<nDocumentId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__media_document_delete')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\DocumentController::deleteAction',  '_locale' => 'es',));
                    }

                    // en__RG__media_document_delete
                    if (preg_match('#^/mediabundle/folder/delete/(?P<nDocumentId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__media_document_delete')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\DocumentController::deleteAction',  '_locale' => 'en',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/mediabundle/gallery/create')) {
                // es_en__RG__media_gallery_create
                if ($pathinfo === '/mediabundle/gallery/create') {
                    return array (  '_controller' => 'Netik\\MediaBundle\\Controller\\GalleryController::createAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__media_gallery_create',);
                }

                // es__RG__media_gallery_create
                if ($pathinfo === '/mediabundle/gallery/create') {
                    return array (  '_controller' => 'Netik\\MediaBundle\\Controller\\GalleryController::createAction',  '_locale' => 'es',  '_route' => 'es__RG__media_gallery_create',);
                }

                // en__RG__media_gallery_create
                if ($pathinfo === '/mediabundle/gallery/create') {
                    return array (  '_controller' => 'Netik\\MediaBundle\\Controller\\GalleryController::createAction',  '_locale' => 'en',  '_route' => 'en__RG__media_gallery_create',);
                }

            }

            if (0 === strpos($pathinfo, '/mediabundle/image')) {
                if (0 === strpos($pathinfo, '/mediabundle/image/upload')) {
                    // es_en__RG__media_image_upload
                    if (preg_match('#^/mediabundle/image/upload/(?P<nGalleryId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__media_image_upload')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\ImageController::uploadAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__media_image_upload
                    if (preg_match('#^/mediabundle/image/upload/(?P<nGalleryId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__media_image_upload')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\ImageController::uploadAction',  '_locale' => 'es',));
                    }

                    // en__RG__media_image_upload
                    if (preg_match('#^/mediabundle/image/upload/(?P<nGalleryId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__media_image_upload')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\ImageController::uploadAction',  '_locale' => 'en',));
                    }

                }

                if (0 === strpos($pathinfo, '/mediabundle/image/sort')) {
                    // es_en__RG__media_image_sort
                    if ($pathinfo === '/mediabundle/image/sort') {
                        return array (  '_controller' => 'Netik\\MediaBundle\\Controller\\ImageController::sortAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__media_image_sort',);
                    }

                    // es__RG__media_image_sort
                    if ($pathinfo === '/mediabundle/image/sort') {
                        return array (  '_controller' => 'Netik\\MediaBundle\\Controller\\ImageController::sortAction',  '_locale' => 'es',  '_route' => 'es__RG__media_image_sort',);
                    }

                    // en__RG__media_image_sort
                    if ($pathinfo === '/mediabundle/image/sort') {
                        return array (  '_controller' => 'Netik\\MediaBundle\\Controller\\ImageController::sortAction',  '_locale' => 'en',  '_route' => 'en__RG__media_image_sort',);
                    }

                }

                if (0 === strpos($pathinfo, '/mediabundle/image/delete')) {
                    // es_en__RG__media_image_delete
                    if (preg_match('#^/mediabundle/image/delete/(?P<nImageId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__media_image_delete')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\ImageController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }

                    // es__RG__media_image_delete
                    if (preg_match('#^/mediabundle/image/delete/(?P<nImageId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__media_image_delete')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\ImageController::deleteAction',  '_locale' => 'es',));
                    }

                    // en__RG__media_image_delete
                    if (preg_match('#^/mediabundle/image/delete/(?P<nImageId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__media_image_delete')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\ImageController::deleteAction',  '_locale' => 'en',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/mediabundle/simple/delete')) {
                // es_en__RG__media_simple_delete
                if (preg_match('#^/mediabundle/simple/delete/(?P<nSimpleId>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__media_simple_delete')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\SimpleFileController::deleteAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                }

                // es__RG__media_simple_delete
                if (preg_match('#^/mediabundle/simple/delete/(?P<nSimpleId>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__media_simple_delete')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\SimpleFileController::deleteAction',  '_locale' => 'es',));
                }

                // en__RG__media_simple_delete
                if (preg_match('#^/mediabundle/simple/delete/(?P<nSimpleId>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__media_simple_delete')), array (  '_controller' => 'Netik\\MediaBundle\\Controller\\SimpleFileController::deleteAction',  '_locale' => 'en',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // es_en__RG__fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_es_en__RG__fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__fos_user_security_login',);
                }
                not_es_en__RG__fos_user_security_login:

                // es__RG__fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_es__RG__fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_security_login',);
                }
                not_es__RG__fos_user_security_login:

                // en__RG__fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_en__RG__fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_security_login',);
                }
                not_en__RG__fos_user_security_login:

                if (0 === strpos($pathinfo, '/login_check')) {
                    // es_en__RG__fos_user_security_check
                    if ($pathinfo === '/login_check') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_es_en__RG__fos_user_security_check;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__fos_user_security_check',);
                    }
                    not_es_en__RG__fos_user_security_check:

                    // es__RG__fos_user_security_check
                    if ($pathinfo === '/login_check') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_es__RG__fos_user_security_check;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_security_check',);
                    }
                    not_es__RG__fos_user_security_check:

                    // en__RG__fos_user_security_check
                    if ($pathinfo === '/login_check') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_en__RG__fos_user_security_check;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_security_check',);
                    }
                    not_en__RG__fos_user_security_check:

                }

            }

            if (0 === strpos($pathinfo, '/logout')) {
                // es_en__RG__fos_user_security_logout
                if ($pathinfo === '/logout') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_es_en__RG__fos_user_security_logout;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__fos_user_security_logout',);
                }
                not_es_en__RG__fos_user_security_logout:

                // es__RG__fos_user_security_logout
                if ($pathinfo === '/logout') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_es__RG__fos_user_security_logout;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_security_logout',);
                }
                not_es__RG__fos_user_security_logout:

                // en__RG__fos_user_security_logout
                if ($pathinfo === '/logout') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_en__RG__fos_user_security_logout;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_security_logout',);
                }
                not_en__RG__fos_user_security_logout:

            }

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // es_en__RG__fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_es_en__RG__fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'es_en__RG__fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__fos_user_profile_show',);
            }
            not_es_en__RG__fos_user_profile_show:

            // es__RG__fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_es__RG__fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'es__RG__fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_profile_show',);
            }
            not_es__RG__fos_user_profile_show:

            // en__RG__fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_en__RG__fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'en__RG__fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_profile_show',);
            }
            not_en__RG__fos_user_profile_show:

            if (0 === strpos($pathinfo, '/profile/edit')) {
                // es_en__RG__fos_user_profile_edit
                if ($pathinfo === '/profile/edit') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_es_en__RG__fos_user_profile_edit;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__fos_user_profile_edit',);
                }
                not_es_en__RG__fos_user_profile_edit:

                // es__RG__fos_user_profile_edit
                if ($pathinfo === '/profile/edit') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_es__RG__fos_user_profile_edit;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_profile_edit',);
                }
                not_es__RG__fos_user_profile_edit:

                // en__RG__fos_user_profile_edit
                if ($pathinfo === '/profile/edit') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_en__RG__fos_user_profile_edit;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_profile_edit',);
                }
                not_en__RG__fos_user_profile_edit:

            }

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // es_en__RG__fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_es_en__RG__fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es_en__RG__fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__fos_user_registration_register',);
                }
                not_es_en__RG__fos_user_registration_register:

                // es__RG__fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_es__RG__fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es__RG__fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_registration_register',);
                }
                not_es__RG__fos_user_registration_register:

                // en__RG__fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_en__RG__fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'en__RG__fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_registration_register',);
                }
                not_en__RG__fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    if (0 === strpos($pathinfo, '/register/check-email')) {
                        // es_en__RG__fos_user_registration_check_email
                        if ($pathinfo === '/register/check-email') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_es_en__RG__fos_user_registration_check_email;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__fos_user_registration_check_email',);
                        }
                        not_es_en__RG__fos_user_registration_check_email:

                        // es__RG__fos_user_registration_check_email
                        if ($pathinfo === '/register/check-email') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_es__RG__fos_user_registration_check_email;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_registration_check_email',);
                        }
                        not_es__RG__fos_user_registration_check_email:

                        // en__RG__fos_user_registration_check_email
                        if ($pathinfo === '/register/check-email') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_en__RG__fos_user_registration_check_email;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_registration_check_email',);
                        }
                        not_en__RG__fos_user_registration_check_email:

                    }

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // es_en__RG__fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_es_en__RG__fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                        }
                        not_es_en__RG__fos_user_registration_confirm:

                        // es__RG__fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_es__RG__fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',  '_locale' => 'es',));
                        }
                        not_es__RG__fos_user_registration_confirm:

                        // en__RG__fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_en__RG__fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',  '_locale' => 'en',));
                        }
                        not_en__RG__fos_user_registration_confirm:

                        if (0 === strpos($pathinfo, '/register/confirmed')) {
                            // es_en__RG__fos_user_registration_confirmed
                            if ($pathinfo === '/register/confirmed') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_es_en__RG__fos_user_registration_confirmed;
                                }

                                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__fos_user_registration_confirmed',);
                            }
                            not_es_en__RG__fos_user_registration_confirmed:

                            // es__RG__fos_user_registration_confirmed
                            if ($pathinfo === '/register/confirmed') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_es__RG__fos_user_registration_confirmed;
                                }

                                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_registration_confirmed',);
                            }
                            not_es__RG__fos_user_registration_confirmed:

                            // en__RG__fos_user_registration_confirmed
                            if ($pathinfo === '/register/confirmed') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_en__RG__fos_user_registration_confirmed;
                                }

                                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_registration_confirmed',);
                            }
                            not_en__RG__fos_user_registration_confirmed:

                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                if (0 === strpos($pathinfo, '/resetting/request')) {
                    // es_en__RG__fos_user_resetting_request
                    if ($pathinfo === '/resetting/request') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_es_en__RG__fos_user_resetting_request;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__fos_user_resetting_request',);
                    }
                    not_es_en__RG__fos_user_resetting_request:

                    // es__RG__fos_user_resetting_request
                    if ($pathinfo === '/resetting/request') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_es__RG__fos_user_resetting_request;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_resetting_request',);
                    }
                    not_es__RG__fos_user_resetting_request:

                    // en__RG__fos_user_resetting_request
                    if ($pathinfo === '/resetting/request') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_en__RG__fos_user_resetting_request;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_resetting_request',);
                    }
                    not_en__RG__fos_user_resetting_request:

                }

                if (0 === strpos($pathinfo, '/resetting/send-email')) {
                    // es_en__RG__fos_user_resetting_send_email
                    if ($pathinfo === '/resetting/send-email') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_es_en__RG__fos_user_resetting_send_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__fos_user_resetting_send_email',);
                    }
                    not_es_en__RG__fos_user_resetting_send_email:

                    // es__RG__fos_user_resetting_send_email
                    if ($pathinfo === '/resetting/send-email') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_es__RG__fos_user_resetting_send_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_resetting_send_email',);
                    }
                    not_es__RG__fos_user_resetting_send_email:

                    // en__RG__fos_user_resetting_send_email
                    if ($pathinfo === '/resetting/send-email') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_en__RG__fos_user_resetting_send_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_resetting_send_email',);
                    }
                    not_en__RG__fos_user_resetting_send_email:

                }

                if (0 === strpos($pathinfo, '/resetting/check-email')) {
                    // es_en__RG__fos_user_resetting_check_email
                    if ($pathinfo === '/resetting/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_es_en__RG__fos_user_resetting_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__fos_user_resetting_check_email',);
                    }
                    not_es_en__RG__fos_user_resetting_check_email:

                    // es__RG__fos_user_resetting_check_email
                    if ($pathinfo === '/resetting/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_es__RG__fos_user_resetting_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_resetting_check_email',);
                    }
                    not_es__RG__fos_user_resetting_check_email:

                    // en__RG__fos_user_resetting_check_email
                    if ($pathinfo === '/resetting/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_en__RG__fos_user_resetting_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_resetting_check_email',);
                    }
                    not_en__RG__fos_user_resetting_check_email:

                }

                if (0 === strpos($pathinfo, '/resetting/reset')) {
                    // es_en__RG__fos_user_resetting_reset
                    if (preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_es_en__RG__fos_user_resetting_reset;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es_en__RG__fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),));
                    }
                    not_es_en__RG__fos_user_resetting_reset:

                    // es__RG__fos_user_resetting_reset
                    if (preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_es__RG__fos_user_resetting_reset;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',  '_locale' => 'es',));
                    }
                    not_es__RG__fos_user_resetting_reset:

                    // en__RG__fos_user_resetting_reset
                    if (preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_en__RG__fos_user_resetting_reset;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',  '_locale' => 'en',));
                    }
                    not_en__RG__fos_user_resetting_reset:

                }

            }

        }

        if (0 === strpos($pathinfo, '/profile/change-password')) {
            // es_en__RG__fos_user_change_password
            if ($pathinfo === '/profile/change-password') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_es_en__RG__fos_user_change_password;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_locales' =>   array (    0 => 'es',    1 => 'en',  ),  '_route' => 'es_en__RG__fos_user_change_password',);
            }
            not_es_en__RG__fos_user_change_password:

            // es__RG__fos_user_change_password
            if ($pathinfo === '/profile/change-password') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_es__RG__fos_user_change_password;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_change_password',);
            }
            not_es__RG__fos_user_change_password:

            // en__RG__fos_user_change_password
            if ($pathinfo === '/profile/change-password') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_en__RG__fos_user_change_password;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_change_password',);
            }
            not_en__RG__fos_user_change_password:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
