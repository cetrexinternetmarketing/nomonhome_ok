<?php

/* FrontendBundle:HomeModel:view.html.twig */
class __TwigTemplate_f0f9e05b783960cde7c3886abb4d3cca388e7b22151fc16118059657d4809f5c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle:Home:base.html.twig", "FrontendBundle:HomeModel:view.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle:Home:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oHomeModel"]) ? $context["oHomeModel"] : null), "name", array()), "html", null, true);
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<!-- HERO -->
<section style=\"background-image: url('";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oHomeModel"]) ? $context["oHomeModel"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "');\" class=\"module module-parallax bg-dark-30\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oHomeModel"]) ? $context["oHomeModel"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

    <!-- HERO TEXT -->
    <div class=\"container\">

        <div class=\"row\">
            <div class=\"col-sm-12 text-center\">
                <h1 class=\"mh-line-size-3 font-alt m-b-20\">";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.basic.collection"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oHomeModel"]) ? $context["oHomeModel"] : null), "name", array()), "html", null, true);
        echo "</h1>
                <h5 class=\"mh-line-size-4 font-alt\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oHomeModel"]) ? $context["oHomeModel"] : null), "name", array()), "html", null, true);
        echo "</h5>
            </div>
        </div>

    </div>
    <!-- /HERO TEXT -->

</section>
<!-- /HERO -->

<!-- PRODUCT -->
<section class=\"module\">

    <div class=\"container\">

        <div class=\"row\">

            <!-- PRODUCT IMAGES -->
            <div class=\"col-sm-6 m-b-sm-40 text-center\">

                ";
        // line 33
        if ($this->getAttribute((isset($context["oImagePrincipal"]) ? $context["oImagePrincipal"] : null), "webpath", array(), "any", true, true)) {
            // line 34
            echo "                    ";
            // line 35
            echo "                    <a class=\"fancybox\" id=\"fancy-image-principal\" rel=\"gallery1\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["oImagePrincipal"]) ? $context["oImagePrincipal"] : null), "webpath", array())), "html", null, true);
            echo "\">
                        <img src=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute((isset($context["oImagePrincipal"]) ? $context["oImagePrincipal"] : null), "webpath", array())), "html", null, true);
            echo "\" style=\"\" id=\"image-principal\" alt=\"\">
                    </a>
                    ";
            // line 39
            echo "                ";
        }
        // line 40
        echo "
                ";
        // line 44
        echo "
                <ul class=\"product-gallery\">
                    ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aImages"]) ? $context["aImages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oImage"]) {
            // line 47
            echo "                        <li>
                            ";
            // line 49
            echo "                            ";
            // line 50
            echo "                            ";
            // line 51
            echo "                            ";
            // line 52
            echo "                            <div class=\"col-sm-12 image-gallery\" style=\"height: 68px; background-size: 110px; background-position: center;  background-repeat: no-repeat; background-image: url('";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["oImage"], "webpath", array())), "html", null, true);
            echo "');\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["oImage"], "webpath", array())), "html", null, true);
            echo "\" class=\"fancybox\" rel=\"gallery1\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($context["oImage"], "webpath", array())), "html", null, true);
            echo "\">
                            </div>
                            ";
            // line 55
            echo "                        </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oImage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "                </ul>

            </div>
            <!-- PRODUCT IMAGES -->

            <!-- PRODUCT DESC -->
            <div class=\"col-sm-6 m-b-sm-40\">

                <!-- TITLE -->
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                        <h1 class=\"product-title font-alt\">";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oHomeModel"]) ? $context["oHomeModel"] : null), "name", array()), "html", null, true);
        echo "</h1>
                    </div>
                </div>

                <!-- DESCRIPTION -->
                <div class=\"row m-b-20\">
                    <div class=\"col-sm-12\">
                        <div class=\"description\">
                            <p>
                                ";
        // line 77
        echo $this->getAttribute((isset($context["oHomeModel"]) ? $context["oHomeModel"] : null), "description", array());
        echo "
                            </p>
                        </div>
                    </div>
                </div>

                <div class=\"row m-b-20\">
                    <div class=\"col-sm-4 mb-sm-20\">
                        ";
        // line 85
        if ( !(null === $this->getAttribute($this->getAttribute((isset($context["oHomeModel"]) ? $context["oHomeModel"] : null), "model3d", array()), "webpath", array()))) {
            // line 86
            echo "                        <a target=\"_blank\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oHomeModel"]) ? $context["oHomeModel"] : null), "model3d", array()), "webpath", array())), "html", null, true);
            echo "\">
                            <img src=\"";
            // line 87
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/icon_2d3d.png"), "html", null, true);
            echo "\">
                            ";
            // line 88
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.download"), "html", null, true);
            echo " 3D
                        </a>
                        ";
        }
        // line 91
        echo "                    </div>

                    <div class=\"col-sm-4 mb-sm-20\">
                        ";
        // line 94
        if ( !(null === $this->getAttribute($this->getAttribute((isset($context["oHomeModel"]) ? $context["oHomeModel"] : null), "datasheet", array()), "webpath", array()))) {
            // line 95
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oHomeModel"]) ? $context["oHomeModel"] : null), "datasheet", array()), "webpath", array())), "html", null, true);
            echo "\" target=\"_blank\" class=\"btn btn-lg btn-block btn-round btn-b\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.model.datasheet"), "html", null, true);
            echo "</a>
                        ";
        }
        // line 97
        echo "                    </div>
                </div>

            </div>
            <!-- PRODUCT DESC -->

        </div>

    </div>

</section>
<!-- /PRODUCT -->

<hr class=\"divider\"><!-- DIVIDER -->

    ";
        // line 112
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FrontendBundle:HomeModel:otherModel"));
        // line 114
        echo "

";
    }

    // line 119
    public function block_javascripts($context, array $blocks = array())
    {
        // line 120
        echo "    <script>
        \$( document ).ready(function() {
            \$( \".image-gallery\" ).click(function() {
                \$(\"#image-principal\").attr('src', \$(this).attr('src') );
                \$(\"#fancy-image-principal\").attr('href', \$(this).attr('src') );
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:HomeModel:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 120,  230 => 119,  224 => 114,  222 => 112,  205 => 97,  197 => 95,  195 => 94,  190 => 91,  184 => 88,  180 => 87,  175 => 86,  173 => 85,  162 => 77,  150 => 68,  137 => 57,  130 => 55,  120 => 52,  118 => 51,  116 => 50,  114 => 49,  111 => 47,  107 => 46,  103 => 44,  100 => 40,  97 => 39,  92 => 36,  87 => 35,  85 => 34,  83 => 33,  60 => 13,  54 => 12,  42 => 5,  39 => 4,  36 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle:Home:base.html.twig' %}*/
/* {% block title %}{{ oHomeModel.name }}{% endblock %}*/
/* {% block content %}*/
/* <!-- HERO -->*/
/* <section style="background-image: url('{{ asset( oHomeModel.banner.webpath ) }}');" class="module module-parallax bg-dark-30" data-background="{{ asset( oHomeModel.banner.webpath ) }}">*/
/* */
/*     <!-- HERO TEXT -->*/
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/*             <div class="col-sm-12 text-center">*/
/*                 <h1 class="mh-line-size-3 font-alt m-b-20">{{ 'nomon.basic.collection' | trans }} {{ oHomeModel.name }}</h1>*/
/*                 <h5 class="mh-line-size-4 font-alt">{{ oHomeModel.name }}</h5>*/
/*             </div>*/
/*         </div>*/
/* */
/*     </div>*/
/*     <!-- /HERO TEXT -->*/
/* */
/* </section>*/
/* <!-- /HERO -->*/
/* */
/* <!-- PRODUCT -->*/
/* <section class="module">*/
/* */
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/* */
/*             <!-- PRODUCT IMAGES -->*/
/*             <div class="col-sm-6 m-b-sm-40 text-center">*/
/* */
/*                 {% if oImagePrincipal.webpath is defined  %}*/
/*                     {#<a href="{{ asset(oImagePrincipal.webpath) }}" class="gallery">#}*/
/*                     <a class="fancybox" id="fancy-image-principal" rel="gallery1" href="{{ asset(oImagePrincipal.webpath) }}">*/
/*                         <img src="{{ asset(oImagePrincipal.webpath) }}" style="" id="image-principal" alt="">*/
/*                     </a>*/
/*                     {#</a>#}*/
/*                 {% endif %}*/
/* */
/*                 {#<a href="{{ asset(oImagePrincipal.webpath) }}" class="gallery">*/
/*                 <img src="{{ asset(oImagePrincipal.webpath) }}" id="image-principal" alt="">*/
/*                 </a>#}*/
/* */
/*                 <ul class="product-gallery">*/
/*                     {% for oImage in aImages %}*/
/*                         <li>*/
/*                             {#<a href="{{  asset(oImage.webpath )  }}" class="gallery">#}*/
/*                             {##}*/
/*                             {#</a>#}*/
/*                             {#<a class="fancybox" rel="gallery1" href="{{ asset(oImage.webpath) }}">#}*/
/*                             <div class="col-sm-12 image-gallery" style="height: 68px; background-size: 110px; background-position: center;  background-repeat: no-repeat; background-image: url('{{ asset( oImage.webpath ) }}');" src="{{ asset( oImage.webpath ) }}" class="fancybox" rel="gallery1" href="{{ asset(oImage.webpath) }}">*/
/*                             </div>*/
/*                             {#</a>#}*/
/*                         </li>*/
/*                     {% endfor %}*/
/*                 </ul>*/
/* */
/*             </div>*/
/*             <!-- PRODUCT IMAGES -->*/
/* */
/*             <!-- PRODUCT DESC -->*/
/*             <div class="col-sm-6 m-b-sm-40">*/
/* */
/*                 <!-- TITLE -->*/
/*                 <div class="row">*/
/*                     <div class="col-sm-12">*/
/*                         <h1 class="product-title font-alt">{{ oHomeModel.name }}</h1>*/
/*                     </div>*/
/*                 </div>*/
/* */
/*                 <!-- DESCRIPTION -->*/
/*                 <div class="row m-b-20">*/
/*                     <div class="col-sm-12">*/
/*                         <div class="description">*/
/*                             <p>*/
/*                                 {{ oHomeModel.description | raw }}*/
/*                             </p>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/* */
/*                 <div class="row m-b-20">*/
/*                     <div class="col-sm-4 mb-sm-20">*/
/*                         {% if oHomeModel.model3d.webpath is not null %}*/
/*                         <a target="_blank" href="{{ asset( oHomeModel.model3d.webpath ) }}">*/
/*                             <img src="{{ asset('bundles/frontend/images/icon_2d3d.png') }}">*/
/*                             {{ 'nomon.model.download' | trans }} 3D*/
/*                         </a>*/
/*                         {% endif %}*/
/*                     </div>*/
/* */
/*                     <div class="col-sm-4 mb-sm-20">*/
/*                         {% if oHomeModel.datasheet.webpath is not null %}*/
/*                             <a href="{{ asset( oHomeModel.datasheet.webpath ) }}" target="_blank" class="btn btn-lg btn-block btn-round btn-b">{{ 'nomon.model.datasheet' | trans }}</a>*/
/*                         {% endif %}*/
/*                     </div>*/
/*                 </div>*/
/* */
/*             </div>*/
/*             <!-- PRODUCT DESC -->*/
/* */
/*         </div>*/
/* */
/*     </div>*/
/* */
/* </section>*/
/* <!-- /PRODUCT -->*/
/* */
/* <hr class="divider"><!-- DIVIDER -->*/
/* */
/*     {{ render(controller(*/
/*     'FrontendBundle:HomeModel:otherModel'*/
/*     )) }}*/
/* */
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*     <script>*/
/*         $( document ).ready(function() {*/
/*             $( ".image-gallery" ).click(function() {*/
/*                 $("#image-principal").attr('src', $(this).attr('src') );*/
/*                 $("#fancy-image-principal").attr('href', $(this).attr('src') );*/
/*             });*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
