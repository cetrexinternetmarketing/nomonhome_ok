<?php

/* FrontendBundle:Frontend:menu.html.twig */
class __TwigTemplate_5262067151f2b7f57e0241e9bfa1383bfa9dc283ea72ec26c31fafd60447c48d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- NAVIGATION -->
<nav class=\"navbar navbar-custom navbar-fixed-top navbar-transparent\">

    <div class=\"container\">

        <div class=\"navbar-header\">

            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#custom-collapse\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>

            <!-- YOU LOGO HERE -->
            <a class=\"navbar-brand\" href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("frontend_frontend_index");
        echo "\">
                <!-- IMAGE OR SIMPLE TEXT -->
                <img class=\"dark-logo\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/nomon_home_white.png"), "html", null, true);
        echo "\" alt=\"\" width=\"95\">
                <img class=\"light-logo\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/frontend/images/nomon_home_white.png"), "html", null, true);
        echo "\" alt=\"\" width=\"95\">
            </a>
        </div>

        <div class=\"collapse navbar-collapse\" id=\"custom-collapse\">

            <ul class=\"nav navbar-nav navbar-right\">
                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">NOMON CULTURE</a>
                    <ul class=\"dropdown-menu\" role=\"menu\">

                        <li class=\"dropdown\">
                            <a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("frontend_page_view", array("sSlug" => "saber-hacer"));
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.menu.saberhacer"), "html", null, true);
        echo "</a>
                        </li>

                        ";
        // line 39
        echo "                       ";
        // line 44
        echo "
                        <li class=\"dropdown\">
                            <a href=\"";
        // line 46
        echo $this->env->getExtension('routing')->getPath("frontend_page_view", array("sSlug" => "customer-service"));
        echo "\">CUSTOMER SERVICE</a>
                        </li>
                    </ul>
                </li>

                <li class=\"dropdown\">
                    <a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("frontend_collection_list");
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.menu.collections"), "html", null, true);
        echo "</a>
                    <ul class=\"dropdown-menu\" role=\"menu\">
                        ";
        // line 54
        if ((twig_length_filter($this->env, (isset($context["aMenuNovedadesModels"]) ? $context["aMenuNovedadesModels"] : null)) > 0)) {
            // line 55
            echo "                        <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oMenuNovedadesCollection"]) ? $context["oMenuNovedadesCollection"] : null), "name", array()), "html", null, true);
            echo "</a>
                            <ul class=\"dropdown-menu\">
                                ";
            // line 58
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["aMenuNovedadesModels"]) ? $context["aMenuNovedadesModels"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["oModel"]) {
                // line 59
                echo "                                    <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_model_view", array("sModelSlug" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["oModel"], "name", array()), "html", null, true);
                echo "</a></li>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oModel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 61
            echo "                            </ul>
                        </li>
                        ";
        }
        // line 64
        echo "                        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aCollection"]) ? $context["aCollection"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oCollection"]) {
            // line 65
            echo "                        <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
            // line 66
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCollection"], "name", array()), "html", null, true);
            echo "</a>
                            <ul class=\"dropdown-menu\">
                                ";
            // line 68
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["oCollection"], "models", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["oModel"]) {
                // line 69
                echo "                                    <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_model_view", array("sModelSlug" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["oModel"], "name", array()), "html", null, true);
                echo "</a></li>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oModel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 71
            echo "                            </ul>
                        </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oCollection'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "                    </ul>
                </li>

                ";
        // line 80
        echo "
                <li class=\"dropdown\">
                    <a href=\"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("frontend_distributor_list");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.menu.distributors"), "html", null, true);
        echo "</a>
                </li>

                ";
        // line 88
        echo "
                ";
        // line 92
        echo "
                <li>
                <a href=\"#\" style=\"font-weight:bold\">NOMON CLOCKS</a>
                </li>

                <li><a href=\"";
        // line 97
        echo $this->env->getExtension('routing')->getPath("frontend_frontend_contact");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.menu.contact"), "html", null, true);
        echo "</a></li>

            </ul>
        </div>

    </div>

</nav>
<!-- /NAVIGATION -->";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Frontend:menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 97,  176 => 92,  173 => 88,  165 => 82,  161 => 80,  156 => 74,  148 => 71,  137 => 69,  133 => 68,  128 => 66,  125 => 65,  120 => 64,  115 => 61,  104 => 59,  100 => 58,  95 => 56,  92 => 55,  90 => 54,  83 => 52,  74 => 46,  70 => 44,  68 => 39,  60 => 31,  45 => 19,  41 => 18,  36 => 16,  19 => 1,);
    }
}
/* <!-- NAVIGATION -->*/
/* <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent">*/
/* */
/*     <div class="container">*/
/* */
/*         <div class="navbar-header">*/
/* */
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">*/
/*                 <span class="sr-only">Toggle navigation</span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/* */
/*             <!-- YOU LOGO HERE -->*/
/*             <a class="navbar-brand" href="{{ path( 'frontend_frontend_index' ) }}">*/
/*                 <!-- IMAGE OR SIMPLE TEXT -->*/
/*                 <img class="dark-logo" src="{{ asset('bundles/frontend/images/nomon_home_white.png')}}" alt="" width="95">*/
/*                 <img class="light-logo" src="{{ asset('bundles/frontend/images/nomon_home_white.png')}}" alt="" width="95">*/
/*             </a>*/
/*         </div>*/
/* */
/*         <div class="collapse navbar-collapse" id="custom-collapse">*/
/* */
/*             <ul class="nav navbar-nav navbar-right">*/
/*                 <li class="dropdown">*/
/*                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">NOMON CULTURE</a>*/
/*                     <ul class="dropdown-menu" role="menu">*/
/* */
/*                         <li class="dropdown">*/
/*                             <a href="{{ path('frontend_page_view', { 'sSlug' : 'saber-hacer' }) }}">{{ 'nomon.menu.saberhacer' | trans }}</a>*/
/*                         </li>*/
/* */
/*                         {#*/
/*                         <li class="dropdown">*/
/* */
/*                         </li>*/
/*                         #}*/
/*                        {#*/
/*                         <li class="dropdown">*/
/*                             <a href="{{ path('frontend_page_view', { 'sName' : 'nomonfriends' }) }}">NOMON & FRIENDS</a>*/
/*                         </li>*/
/*                         #}*/
/* */
/*                         <li class="dropdown">*/
/*                             <a href="{{ path('frontend_page_view', { 'sSlug' : 'customer-service' }) }}">CUSTOMER SERVICE</a>*/
/*                         </li>*/
/*                     </ul>*/
/*                 </li>*/
/* */
/*                 <li class="dropdown">*/
/*                     <a href="{{ path("frontend_collection_list") }}" class="dropdown-toggle" data-toggle="dropdown">{{ 'nomon.menu.collections' | trans }}</a>*/
/*                     <ul class="dropdown-menu" role="menu">*/
/*                         {% if aMenuNovedadesModels | length > 0 %}*/
/*                         <li class="dropdown">*/
/*                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ oMenuNovedadesCollection.name }}</a>*/
/*                             <ul class="dropdown-menu">*/
/*                                 {% for oModel in aMenuNovedadesModels %}*/
/*                                     <li><a href="{{ path('frontend_model_view',{ 'sModelSlug' : oModel.slug }) }}">{{ oModel.name }}</a></li>*/
/*                                 {% endfor %}*/
/*                             </ul>*/
/*                         </li>*/
/*                         {% endif %}*/
/*                         {% for oCollection in aCollection %}*/
/*                         <li class="dropdown">*/
/*                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ oCollection.name }}</a>*/
/*                             <ul class="dropdown-menu">*/
/*                                 {% for oModel in oCollection.models %}*/
/*                                     <li><a href="{{ path('frontend_model_view',{ 'sModelSlug' : oModel.slug }) }}">{{ oModel.name }}</a></li>*/
/*                                 {% endfor %}*/
/*                             </ul>*/
/*                         </li>*/
/*                         {% endfor %}*/
/*                     </ul>*/
/*                 </li>*/
/* */
/*                 {#<li class="dropdown">*/
/*                     <a href="{{ path('frontend_project_list') }}">{{ 'nomon.menu.projects' | trans }}</a>*/
/*                 </li>#}*/
/* */
/*                 <li class="dropdown">*/
/*                     <a href="{{ path('frontend_distributor_list') }}">{{ 'nomon.menu.distributors' | trans }}</a>*/
/*                 </li>*/
/* */
/*                 {#<li class="dropdown">*/
/*                     <a href="{{ path('frontend_collaborator_list') }}">{{ 'nomon.menu.collaborations' | trans }}</a>*/
/*                 </li>#}*/
/* */
/*                 {#<li>*/
/*                     <a href="{{ path('frontend_home_index') }}" style="font-weight:bold">NOMON "HOME"</a>*/
/*                 </li>#}*/
/* */
/*                 <li>*/
/*                 <a href="#" style="font-weight:bold">NOMON CLOCKS</a>*/
/*                 </li>*/
/* */
/*                 <li><a href="{{ path('frontend_frontend_contact') }}">{{ 'nomon.menu.contact' | trans }}</a></li>*/
/* */
/*             </ul>*/
/*         </div>*/
/* */
/*     </div>*/
/* */
/* </nav>*/
/* <!-- /NAVIGATION -->*/
