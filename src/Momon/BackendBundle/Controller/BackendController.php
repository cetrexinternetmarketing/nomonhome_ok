<?php

namespace Momon\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Momon\BackendBundle\Entity\Collection;
use Momon\BackendBundle\Form\CollectionType;

/**
 *
 */
class BackendController extends Controller
{

	/**
	 * INDEX ACTION - Collection.
	 *
	 * @Route("/", name="backend_index")
	 * @Method("GET")
	 * @Template()
	 */
	public function indexAction()
	{
        $em = $this->getDoctrine()->getManager();
        $aCollections = $em->getRepository( 'BackendBundle:Collection' )->findAll();
        $aModels = $em->getRepository( 'BackendBundle:Model' )->findAll();
        $aProjects = $em->getRepository( 'BackendBundle:Project' )->findAll();
        $aCategories = $em->getRepository( 'BackendBundle:CategoryProject' )->findAll();
        $aDistributors = $em->getRepository( 'BackendBundle:Distributor' )->findAll();
        $aCollaborators = $em->getRepository( 'BackendBundle:Collaborator' )->findAll();
        $aHomeModels = $em->getRepository( 'BackendBundle:HomeModel' )->findAll();
        $aPages = $em->getRepository( 'BackendBundle:Page' )->findAll();
        $aSections = $em->getRepository( 'BackendBundle:Section' )->findAll();
        $aParallax = $em->getRepository( 'BackendBundle:Parallax' )->findAll();       
        $aBanners = $em->getRepository( 'BackendBundle:Banner' )->findAll();
        
		return array(
            'aCollections' => $aCollections,
            'aModels' => $aModels,
            'aProjects' => $aProjects,
            'aCategories' => $aCategories,
            'aDistributors' => $aDistributors,
            'aCollaborators' => $aCollaborators,
            'aHomeModels' => $aHomeModels,
            'aPages' => $aPages,
            'aSections' => $aSections,
            'aParallax' => $aParallax,
            'aBanners' => $aBanners
        );
	}
}