<?php

/* FrontendBundle:CollectionHome:list.html.twig */
class __TwigTemplate_d5d796ce9ab36d26afdc1d175fe23022293494f4c3cf194cabae86892913268d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle:Home:base.html.twig", "FrontendBundle:CollectionHome:list.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle:Home:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "NOMON CLOCKS | ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.title.collections"), "html", null, true);
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<!-- HERO -->
<section style=\"background-image: url('";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oSection"]) ? $context["oSection"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "');\" class=\"module module-parallax bg-dark-30\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oSection"]) ? $context["oSection"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

    <!-- HERO TEXT -->
    <div class=\"container\">

        <div class=\"row\">
            <div class=\"col-sm-12 text-center\">
                <h1 class=\"mh-line-size-3 font-alt m-b-20\">";
        // line 12
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["oSection"]) ? $context["oSection"] : null), "title", array())), "html", null, true);
        echo "</h1>
                <h5 class=\"mh-line-size-4 font-alt\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oSection"]) ? $context["oSection"] : null), "slogan", array()), "html", null, true);
        echo "</h5>
            </div>
        </div>

    </div>
    <!-- /HERO TEXT -->

</section>
<!-- /HERO -->

";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aCollection"]) ? $context["aCollection"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oCollection"]) {
            // line 24
            echo "    <!-- ABOUT -->
    <section class=\"module\">


        <div class=\"container\">

            <div class=\"row\">
                <div class=\"col-sm-6 col-sm-offset-3\">
                    <h2 class=\"module-title font-alt\" id=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCollection"], "name", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCollection"], "name", array()), "html", null, true);
            echo "</h2>
                </div>
            </div>
            <!-- /MODULE TITLE -->

            <div class=\"row multi-columns-row\">

                ";
            // line 39
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["oCollection"], "models", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["oModel"]) {
                // line 40
                echo "                <!-- POST -->
                <div class=\"col-sm-6 col-md-4 col-lg-4 m-b-60\">
                    <div class=\"post\">
                        <div class=\"post-media\">
                            <a href=\"";
                // line 44
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_homemodel_view", array("sHomeModel" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
                echo "\">
                                <img src=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute($context["oModel"], "banner", array()), "webpath", array())), "html", null, true);
                echo "\" alt=\"\">
                            </a>
                        </div>
                        <div class=\"post-header\">
                            <h5 class=\"post-title font-alt\">
                                <a href=\"";
                // line 50
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_homemodel_view", array("sHomeModel" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["oModel"], "name", array()), "html", null, true);
                echo "</a>
                            </h5>
                        </div>
                        <div class=\"post-meta font-alt\">

                        </div>
                    </div>
                </div>
                <!-- /POST -->
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oModel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 60
            echo "
            </div>
        </div>


    </section>
    <!-- BLOG 3 COLUMN -->

    <hr class=\"divider\"><!-- DIVIDER -->
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oCollection'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 70
        if ( !(null === (isset($context["oNovedadesCollection"]) ? $context["oNovedadesCollection"] : null))) {
            // line 71
            echo "    <!-- ABOUT -->
    <section class=\"module\">


        <div class=\"container\">

            <div class=\"row\">
                <div class=\"col-sm-6 col-sm-offset-3\">
                    <h2 class=\"module-title font-alt\" id=\"";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oNovedadesCollection"]) ? $context["oNovedadesCollection"] : null), "name", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oNovedadesCollection"]) ? $context["oNovedadesCollection"] : null), "name", array()), "html", null, true);
            echo "</h2>
                </div>
            </div>
            <!-- /MODULE TITLE -->

            <div class=\"row multi-columns-row\">

                ";
            // line 86
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["oNovedadesCollectionModels"]) ? $context["oNovedadesCollectionModels"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["oModel"]) {
                // line 87
                echo "                    <!-- POST -->
                    <div class=\"col-sm-6 col-md-4 col-lg-4 m-b-60\">
                        <div class=\"post\">
                            <div class=\"post-media\">
                                <a href=\"";
                // line 91
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_homemodel_view", array("sHomeModel" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
                echo "\">
                                    <img src=\"";
                // line 92
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute($context["oModel"], "banner", array()), "webpath", array())), "html", null, true);
                echo "\" alt=\"\">
                                </a>
                            </div>
                            <div class=\"post-header\">
                                <h5 class=\"post-title font-alt\">
                                    <a href=\"";
                // line 97
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_homemodel_view", array("sHomeModel" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["oModel"], "name", array()), "html", null, true);
                echo "</a>
                                </h5>
                            </div>
                            <div class=\"post-meta font-alt\">

                            </div>
                        </div>
                    </div>
                    <!-- /POST -->
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oModel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 107
            echo "
            </div>
        </div>


    </section>
    <!-- BLOG 3 COLUMN -->

    <hr class=\"divider\"><!-- DIVIDER -->
";
        }
        // line 117
        echo "
";
        // line 118
        if (array_key_exists("oModel", $context)) {
            // line 119
            echo "    ";
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FrontendBundle:HomeModel:otherModel", array("nCollectionId" => $this->getAttribute($this->getAttribute(            // line 120
(isset($context["oModel"]) ? $context["oModel"] : null), "collection", array()), "id", array()))));
            // line 121
            echo "
";
        } else {
            // line 123
            echo "    ";
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("FrontendBundle:HomeModel:otherModel"));
            // line 125
            echo "
";
        }
        // line 127
        echo "
";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:CollectionHome:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  248 => 127,  244 => 125,  241 => 123,  237 => 121,  235 => 120,  233 => 119,  231 => 118,  228 => 117,  216 => 107,  198 => 97,  190 => 92,  186 => 91,  180 => 87,  176 => 86,  164 => 79,  154 => 71,  152 => 70,  137 => 60,  119 => 50,  111 => 45,  107 => 44,  101 => 40,  97 => 39,  85 => 32,  75 => 24,  71 => 23,  58 => 13,  54 => 12,  42 => 5,  39 => 4,  36 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle:Home:base.html.twig' %}*/
/* {% block title %}NOMON CLOCKS | {{ 'nomon.title.collections' | trans }}{% endblock %}*/
/* {% block content %}*/
/* <!-- HERO -->*/
/* <section style="background-image: url('{{ asset(oSection.banner.webpath) }}');" class="module module-parallax bg-dark-30" data-background="{{ asset(oSection.banner.webpath) }}">*/
/* */
/*     <!-- HERO TEXT -->*/
/*     <div class="container">*/
/* */
/*         <div class="row">*/
/*             <div class="col-sm-12 text-center">*/
/*                 <h1 class="mh-line-size-3 font-alt m-b-20">{{ oSection.title | upper }}</h1>*/
/*                 <h5 class="mh-line-size-4 font-alt">{{ oSection.slogan }}</h5>*/
/*             </div>*/
/*         </div>*/
/* */
/*     </div>*/
/*     <!-- /HERO TEXT -->*/
/* */
/* </section>*/
/* <!-- /HERO -->*/
/* */
/* {% for oCollection in aCollection %}*/
/*     <!-- ABOUT -->*/
/*     <section class="module">*/
/* */
/* */
/*         <div class="container">*/
/* */
/*             <div class="row">*/
/*                 <div class="col-sm-6 col-sm-offset-3">*/
/*                     <h2 class="module-title font-alt" id="{{ oCollection.name }}">{{ oCollection.name }}</h2>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- /MODULE TITLE -->*/
/* */
/*             <div class="row multi-columns-row">*/
/* */
/*                 {% for oModel in oCollection.models %}*/
/*                 <!-- POST -->*/
/*                 <div class="col-sm-6 col-md-4 col-lg-4 m-b-60">*/
/*                     <div class="post">*/
/*                         <div class="post-media">*/
/*                             <a href="{{ path('frontend_homemodel_view',{ 'sHomeModel' : oModel.slug }) }}">*/
/*                                 <img src="{{ asset( oModel.banner.webpath ) }}" alt="">*/
/*                             </a>*/
/*                         </div>*/
/*                         <div class="post-header">*/
/*                             <h5 class="post-title font-alt">*/
/*                                 <a href="{{ path('frontend_homemodel_view',{ 'sHomeModel' : oModel.slug }) }}">{{ oModel.name }}</a>*/
/*                             </h5>*/
/*                         </div>*/
/*                         <div class="post-meta font-alt">*/
/* */
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <!-- /POST -->*/
/*                 {% endfor %}*/
/* */
/*             </div>*/
/*         </div>*/
/* */
/* */
/*     </section>*/
/*     <!-- BLOG 3 COLUMN -->*/
/* */
/*     <hr class="divider"><!-- DIVIDER -->*/
/* {% endfor %}*/
/* {% if oNovedadesCollection is not null%}*/
/*     <!-- ABOUT -->*/
/*     <section class="module">*/
/* */
/* */
/*         <div class="container">*/
/* */
/*             <div class="row">*/
/*                 <div class="col-sm-6 col-sm-offset-3">*/
/*                     <h2 class="module-title font-alt" id="{{ oNovedadesCollection.name }}">{{ oNovedadesCollection.name }}</h2>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- /MODULE TITLE -->*/
/* */
/*             <div class="row multi-columns-row">*/
/* */
/*                 {% for oModel in oNovedadesCollectionModels %}*/
/*                     <!-- POST -->*/
/*                     <div class="col-sm-6 col-md-4 col-lg-4 m-b-60">*/
/*                         <div class="post">*/
/*                             <div class="post-media">*/
/*                                 <a href="{{ path('frontend_homemodel_view',{ 'sHomeModel' : oModel.slug }) }}">*/
/*                                     <img src="{{ asset( oModel.banner.webpath ) }}" alt="">*/
/*                                 </a>*/
/*                             </div>*/
/*                             <div class="post-header">*/
/*                                 <h5 class="post-title font-alt">*/
/*                                     <a href="{{ path('frontend_homemodel_view',{ 'sHomeModel' : oModel.slug }) }}">{{ oModel.name }}</a>*/
/*                                 </h5>*/
/*                             </div>*/
/*                             <div class="post-meta font-alt">*/
/* */
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <!-- /POST -->*/
/*                 {% endfor %}*/
/* */
/*             </div>*/
/*         </div>*/
/* */
/* */
/*     </section>*/
/*     <!-- BLOG 3 COLUMN -->*/
/* */
/*     <hr class="divider"><!-- DIVIDER -->*/
/* {% endif %}*/
/* */
/* {% if oModel is defined %}*/
/*     {{ render(controller(*/
/*     'FrontendBundle:HomeModel:otherModel', {'nCollectionId': oModel.collection.id}*/
/*     )) }}*/
/* {% else %}*/
/*     {{ render(controller(*/
/*     'FrontendBundle:HomeModel:otherModel'*/
/*     )) }}*/
/* {% endif %}*/
/* */
/* {% endblock %}*/
