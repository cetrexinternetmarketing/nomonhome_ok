<?php

/* FrontendBundle:IndexSection:models_index.html.twig */
class __TwigTemplate_8f8daebad8cc827e413499ea65d20fe82efafa46812b766e4decc416f3117f50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- ABOUT -->
<section class=\"module\">

    <div class=\"container\">

        <!-- MODULE TITLE -->
        <div class=\"row\">
            <div class=\"col-sm-6 col-sm-offset-3\">
                <h2 class=\"module-title font-alt\">";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.home.title"), "html", null, true);
        echo "</h2>
                <div class=\"module-subtitle font-serif\">
                    ";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.home.text"), "html", null, true);
        echo "
                </div>
            </div>
        </div>

        <hr class=\"divider\"><!-- DIVIDER -->
        <section class=\"module\">
            <!-- MODULE TITLE -->
            <div class=\"row\">
                <div class=\"col-sm-6 col-sm-offset-3\">
                    <h2 class=\"module-title font-alt\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.menu.collections"), "html", null, true);
        echo "</h2>
                </div>
            </div>
            <!-- /MODULE TITLE -->

            <div class=\"row multi-columns-row\">

                ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aCollections"]) ? $context["aCollections"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["oCollection"]) {
            // line 29
            echo "                    <!-- POST -->
                    <div class=\"col-sm-6 col-md-4 col-lg-4 m-b-60\">
                        <div class=\"post\">
                            <div class=\"post-media\">
                                <img src=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute($context["oCollection"], "image", array()), "webpath", array())), "html", null, true);
            echo "\" alt=\"\">
                            </div>
                            <div class=\"post-header\">
                                <h5 class=\"post-title font-alt\">
                                    ";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["oCollection"], "name", array()), "html", null, true);
            echo "
                                </h5>
                            </div>
                            <div class=\"post-meta font-alt\">
                                ";
            // line 41
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["oCollection"], "models", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["oModel"]) {
                // line 42
                echo "                                    <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_model_view", array("sModelSlug" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["oModel"], "name", array()), "html", null, true);
                echo "</a> ";
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo "/";
                }
                // line 43
                echo "                                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oModel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "                            </div>
                        </div>
                    </div>
                    <!-- /POST -->
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oCollection'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "                ";
        if ( !(null === (isset($context["oNovedadesCollection"]) ? $context["oNovedadesCollection"] : null))) {
            // line 50
            echo "                    <div class=\"col-sm-6 col-md-4 col-lg-4 m-b-60\">
                        <div class=\"post\">
                            <div class=\"post-media\">
                                <img src=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oNovedadesCollection"]) ? $context["oNovedadesCollection"] : null), "image", array()), "webpath", array())), "html", null, true);
            echo "\" alt=\"\">
                            </div>
                            <div class=\"post-header\">
                                <h5 class=\"post-title font-alt\">
                                    ";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["oNovedadesCollection"]) ? $context["oNovedadesCollection"] : null), "name", array()), "html", null, true);
            echo "
                                </h5>
                            </div>
                            <div class=\"post-meta font-alt\">
                                ";
            // line 61
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["oNovedadesCollectionModels"]) ? $context["oNovedadesCollectionModels"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["oModel"]) {
                // line 62
                echo "                                    <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("frontend_model_view", array("sModelSlug" => $this->getAttribute($context["oModel"], "slug", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["oModel"], "name", array()), "html", null, true);
                echo "</a> ";
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo "/";
                }
                // line 63
                echo "                                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oModel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "                            </div>
                        </div>
                    </div>
                ";
        }
        // line 68
        echo "
            </div>
        </section>
        <!-- BLOG 3 COLUMN -->
    </div>


</section>
<!-- /ABOUT -->";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:IndexSection:models_index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 68,  193 => 64,  179 => 63,  170 => 62,  153 => 61,  146 => 57,  139 => 53,  134 => 50,  131 => 49,  121 => 44,  107 => 43,  98 => 42,  81 => 41,  74 => 37,  67 => 33,  61 => 29,  57 => 28,  47 => 21,  34 => 11,  29 => 9,  19 => 1,);
    }
}
/* <!-- ABOUT -->*/
/* <section class="module">*/
/* */
/*     <div class="container">*/
/* */
/*         <!-- MODULE TITLE -->*/
/*         <div class="row">*/
/*             <div class="col-sm-6 col-sm-offset-3">*/
/*                 <h2 class="module-title font-alt">{{ 'nomon.home.title' | raw | trans }}</h2>*/
/*                 <div class="module-subtitle font-serif">*/
/*                     {{ 'nomon.home.text' | raw | trans }}*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <hr class="divider"><!-- DIVIDER -->*/
/*         <section class="module">*/
/*             <!-- MODULE TITLE -->*/
/*             <div class="row">*/
/*                 <div class="col-sm-6 col-sm-offset-3">*/
/*                     <h2 class="module-title font-alt">{{ 'nomon.menu.collections' | trans }}</h2>*/
/*                 </div>*/
/*             </div>*/
/*             <!-- /MODULE TITLE -->*/
/* */
/*             <div class="row multi-columns-row">*/
/* */
/*                 {% for oCollection in aCollections %}*/
/*                     <!-- POST -->*/
/*                     <div class="col-sm-6 col-md-4 col-lg-4 m-b-60">*/
/*                         <div class="post">*/
/*                             <div class="post-media">*/
/*                                 <img src="{{ asset( oCollection.image.webpath )}}" alt="">*/
/*                             </div>*/
/*                             <div class="post-header">*/
/*                                 <h5 class="post-title font-alt">*/
/*                                     {{ oCollection.name }}*/
/*                                 </h5>*/
/*                             </div>*/
/*                             <div class="post-meta font-alt">*/
/*                                 {% for oModel in oCollection.models %}*/
/*                                     <a href="{{ path('frontend_model_view', { 'sModelSlug' : oModel.slug }) }}">{{ oModel.name  }}</a> {% if not loop.last %}/{% endif %}*/
/*                                 {% endfor %}*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <!-- /POST -->*/
/*                 {% endfor %}*/
/*                 {% if oNovedadesCollection is not null%}*/
/*                     <div class="col-sm-6 col-md-4 col-lg-4 m-b-60">*/
/*                         <div class="post">*/
/*                             <div class="post-media">*/
/*                                 <img src="{{ asset( oNovedadesCollection.image.webpath )}}" alt="">*/
/*                             </div>*/
/*                             <div class="post-header">*/
/*                                 <h5 class="post-title font-alt">*/
/*                                     {{ oNovedadesCollection.name }}*/
/*                                 </h5>*/
/*                             </div>*/
/*                             <div class="post-meta font-alt">*/
/*                                 {% for oModel in oNovedadesCollectionModels %}*/
/*                                     <a href="{{ path('frontend_model_view', { 'sModelSlug' : oModel.slug }) }}">{{ oModel.name  }}</a> {% if not loop.last %}/{% endif %}*/
/*                                 {% endfor %}*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 {% endif %}*/
/* */
/*             </div>*/
/*         </section>*/
/*         <!-- BLOG 3 COLUMN -->*/
/*     </div>*/
/* */
/* */
/* </section>*/
/* <!-- /ABOUT -->*/
