<?php

/* FrontendBundle:Frontend:index.html.twig */
class __TwigTemplate_cd32a19e88cadce6b052a88227aedbc1e53ee6d7f214123867b3021d80c3c483 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontendBundle:Frontend:base.html.twig", "FrontendBundle:Frontend:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontendBundle:Frontend:base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "NOMON CLOCKS | ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nomon.title.index"), "html", null, true);
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
    <!-- SLIDER -->
    ";
        // line 6
        if ( !(null === $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "url", array()))) {
            // line 7
            echo "        ";
            echo $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "url", array());
            echo "
    ";
        }
        // line 9
        echo "    <section style=\"background-image: url('";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "'); background-position: 50% 0px; height: 951px;\" id=\"hero\" class=\"module-hero module-parallax module-full-height bg-film\" data-background=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "banner", array()), "webpath", array())), "html", null, true);
        echo "\">

        <!-- HERO TEXT -->
        <div class=\"hero-caption\">
            <div class=\"hero-text\">
                <h5 class=\"mh-line-size-4 font-alt m-b-50\">";
        // line 14
        echo $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "title1", array());
        echo "</h5>
                <h1 class=\"mh-line-size-1 font-alt m-b-60\">";
        // line 15
        echo $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "title2", array());
        echo "</h1>
                <h5 class=\"mh-line-size-5 font-alt m-b-50\">";
        // line 16
        echo $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "title3", array());
        echo "</h5>
            </div>
        </div>
        <!-- /HERO TEXT -->

    </section>
    ";
        // line 22
        if ( !(null === $this->getAttribute((isset($context["oBanner"]) ? $context["oBanner"] : null), "url", array()))) {
            // line 23
            echo "        </a>
    ";
        }
        // line 25
        echo "    <!-- /SLIDER -->

    ";
        // line 27
        echo twig_include($this->env, $context, "FrontendBundle:IndexSection:models_index.html.twig");
        echo "

    ";
        // line 29
        echo twig_include($this->env, $context, "FrontendBundle:IndexSection:divider_index.html.twig");
        echo "

    ";
        // line 32
        echo "
    ";
        // line 33
        echo twig_include($this->env, $context, "FrontendBundle:IndexSection:divider_index.html.twig");
        echo "

    ";
        // line 36
        echo "
    ";
        // line 38
        echo "
    ";
        // line 39
        echo twig_include($this->env, $context, "FrontendBundle:IndexSection:culture_index.html.twig");
        echo "

    <hr class=\"divider\"><!-- DIVIDER -->

";
    }

    public function getTemplateName()
    {
        return "FrontendBundle:Frontend:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 39,  110 => 38,  107 => 36,  102 => 33,  99 => 32,  94 => 29,  89 => 27,  85 => 25,  81 => 23,  79 => 22,  70 => 16,  66 => 15,  62 => 14,  51 => 9,  45 => 7,  43 => 6,  39 => 4,  36 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'FrontendBundle:Frontend:base.html.twig' %}*/
/* {% block title %}NOMON CLOCKS | {{ 'nomon.title.index' | trans }}{% endblock %}*/
/* {% block content %}*/
/* */
/*     <!-- SLIDER -->*/
/*     {% if oBanner.url is not null %}*/
/*         {{ oBanner.url | raw }}*/
/*     {% endif %}*/
/*     <section style="background-image: url('{{ asset( oBanner.banner.webpath) }}'); background-position: 50% 0px; height: 951px;" id="hero" class="module-hero module-parallax module-full-height bg-film" data-background="{{ asset( oBanner.banner.webpath) }}">*/
/* */
/*         <!-- HERO TEXT -->*/
/*         <div class="hero-caption">*/
/*             <div class="hero-text">*/
/*                 <h5 class="mh-line-size-4 font-alt m-b-50">{{ oBanner.title1 | raw }}</h5>*/
/*                 <h1 class="mh-line-size-1 font-alt m-b-60">{{ oBanner.title2 | raw }}</h1>*/
/*                 <h5 class="mh-line-size-5 font-alt m-b-50">{{ oBanner.title3 | raw }}</h5>*/
/*             </div>*/
/*         </div>*/
/*         <!-- /HERO TEXT -->*/
/* */
/*     </section>*/
/*     {% if oBanner.url is not null %}*/
/*         </a>*/
/*     {% endif %}*/
/*     <!-- /SLIDER -->*/
/* */
/*     {{ include('FrontendBundle:IndexSection:models_index.html.twig') }}*/
/* */
/*     {{ include('FrontendBundle:IndexSection:divider_index.html.twig') }}*/
/* */
/*     {#{ include('FrontendBundle:IndexSection:projects_index.html.twig') }#}*/
/* */
/*     {{ include('FrontendBundle:IndexSection:divider_index.html.twig') }}*/
/* */
/*     {#{ include('FrontendBundle:IndexSection:collaborators_index.html.twig') }#}*/
/* */
/*     {#{ include('FrontendBundle:IndexSection:home_index.html.twig') }#}*/
/* */
/*     {{ include('FrontendBundle:IndexSection:culture_index.html.twig') }}*/
/* */
/*     <hr class="divider"><!-- DIVIDER -->*/
/* */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
