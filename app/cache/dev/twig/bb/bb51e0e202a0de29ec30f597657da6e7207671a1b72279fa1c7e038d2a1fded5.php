<?php

/* FrontendBundle:IndexSection:divider_index.html.twig */
class __TwigTemplate_53838f2c93064492f8c49fe6edcefa0a6f1ba69354145687627214fa5a184f88 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b9f49c39fb1a4253c51ff709a8f47a51d46024232d198675938d49d76b33b9aa = $this->env->getExtension("native_profiler");
        $__internal_b9f49c39fb1a4253c51ff709a8f47a51d46024232d198675938d49d76b33b9aa->enter($__internal_b9f49c39fb1a4253c51ff709a8f47a51d46024232d198675938d49d76b33b9aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontendBundle:IndexSection:divider_index.html.twig"));

        // line 1
        echo "<hr class=\"divider\"><!-- DIVIDER -->";
        
        $__internal_b9f49c39fb1a4253c51ff709a8f47a51d46024232d198675938d49d76b33b9aa->leave($__internal_b9f49c39fb1a4253c51ff709a8f47a51d46024232d198675938d49d76b33b9aa_prof);

    }

    public function getTemplateName()
    {
        return "FrontendBundle:IndexSection:divider_index.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <hr class="divider"><!-- DIVIDER -->*/
